import React, { PureComponent } from 'react';
import E from 'wangeditor';
import { message } from 'antd';

export default class Editor extends PureComponent {
  componentDidMount() {
    const { initContent } = this.props;
    const elem = this.editorElem;
    this.editor = new E(elem);
    // 配置服务器端地址
    this.editor.customConfig.uploadImgServer = '/b/editor-upload-image';
    // 配置文件名
    this.editor.customConfig.uploadFileName = 'file[]';
    // 限制一次最多上传9张图片
    this.editor.customConfig.uploadImgMaxLength = 9;
    // 将 timeout 时间改为 1分钟
    this.editor.customConfig.uploadImgTimeout = 60000;
    // 将图片大小限制为 2M
    this.editor.customConfig.uploadImgMaxSize = 1048576;
    // 上传提示
    this.editor.customConfig.customAlert = (info) => {
      // info 是需要提示的内容
      message.info(info);
    };
    const self = this;
    // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
    this.editor.customConfig.onchange = (html) => {
      self.props.onChange(html);
    };
    this.editor.create();
    setInterval(() => {
      self.props.onChange(this.editor.txt.html());
    }, 1000);
    this.editor.txt.html(initContent);
  }
  componentWillUnmount() {
    this.editor.txt.clear();
  }
  // A标签兼容操作
  openLink = (e) => {
    if (e.target.nodeName === 'A' && e.target.dataset.href) {
      window.open(e.target.dataset.href);
    }
  };

  render() {
    return (
      <div>
        <div ref={(n) => { this.editorElem = n; }} style={{ textAlign: 'left' }} />
      </div>
    );
  }
}

