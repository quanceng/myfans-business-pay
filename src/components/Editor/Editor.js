import React, { PureComponent } from 'react';
import E from 'wangeditor';
import { message } from 'antd';

export default class Editor extends PureComponent {
  componentDidMount() {
    const { initContent } = this.props;
    const elem2 = this.editorElem;
    const elem1 = this.editElem;
    this.editor = new E(elem1, elem2);
    // 配置编辑器显示设置
    this.editor.customConfig.zIndex = 1;
    // 配置服务器端地址
    this.editor.customConfig.uploadImgServer = '/b/editor-upload-image';
    // 配置文件名
    this.editor.customConfig.uploadFileName = 'file[]';
    // 限制一次最多上传9张图片
    this.editor.customConfig.uploadImgMaxLength = 9;
    // 将 timeout 时间改为 1分钟
    this.editor.customConfig.uploadImgTimeout = 60000;
    // 将图片大小限制为 2M
    this.editor.customConfig.uploadImgMaxSize = 1048576;
    // 上传提示
    this.editor.customConfig.customAlert = (info) => {
      // info 是需要提示的内容
      message.info(info);
    };
    const self = this;
    // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
    this.editor.customConfig.onchange = (html) => {
      self.props.onChange(html);
    };
    this.editor.create();
    this.editor.txt.html(initContent);
  }
  handle135 = () => {
    const url = `http://www.135editor.com/beautify_editor?callback=${window.location.href}`;
    window.open(url, 'new_window', 'dialogHeight:750px; dialogWidth:1000px;help:No;resizable:No;status:No;scroll:Yes');
    if (window.addEventListener) {
      window.addEventListener('message', this.onMessage, false);
    } else if (window.attachEvent) {
      window.attachEvent('onmessage', this.onMessage);
    }
  };
  onMessage = (e) => {
    this.editor.txt.append(e.data);
    setTimeout(() => {
      this.props.onChange(this.editor.txt.html());
    }, 2000);
  };
  // A标签兼容操作
  openLink = (e) => {
    if (e.target.nodeName === 'A' && e.target.dataset.href) {
      window.open(e.target.dataset.href);
    }
  };

  componentWillUnmount() {
    this.editor.txt.clear();
  }

  render() {
    return (
      <div style={{ position: 'relative' }}>
        <div ref={(n) => { this.editElem = n; }} style={{ textAlign: 'left', border: '1px solid #ccc', borderBottom: 0 }}>
          <div className="w-e-menu" style={{ zIndex: 2 }} onClick={this.handle135} title="前去135编辑器排版">
            135
          </div>
        </div>
        <div onClick={(e) => { this.openLink(e); }} ref={(n) => { this.editorElem = n; }} style={{ textAlign: 'left', border: '1px solid #ccc', height: 400 }} />
      </div>
    );
  }
}

