import React, { PureComponent } from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';

const { RangePicker } = DatePicker;
export default class DatePickers extends PureComponent {
  // 选中开始时间
  onDateChange = (e) => {
    if (!e.length) {
      this.props.changeDate();
    } else {
      this.props.changeDate(e[0].format('YYYY-MM-DD'), e[1].format('YYYY-MM-DD'));
    }
  };
  render() {
    const { width, endDate, startDate } = this.props;
    const dateFormat = 'YYYY-MM-DD';
    if (width && startDate) {
      return (
        <div
          style={{ width: 260 }}
        >
          <RangePicker
            onChange={this.onDateChange}
            value={[moment(startDate, dateFormat), moment(endDate, dateFormat)]}
          />
        </div>
      );
    }
    if (width) {
      return (
        <div
          style={{ width: 260 }}
        >
          <RangePicker
            onChange={this.onDateChange}
          />
        </div>
      );
    }
    return (
      <div>
        <RangePicker
          onChange={this.onDateChange}
        />
      </div>
    );
  }
}
