import React, { PureComponent } from 'react';
import { Modal, Form, Select, Input, message } from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;
@Form.create()
class GagUser extends PureComponent {
  state = {
    reason: '',
    gagTime: '21600',
  };
  // 选择禁言时间
  handleGagTime = (value) => {
    this.setState({ gagTime: value });
  }
  // 保存禁言原因
  handleReason = (e) => {
    this.setState({ reason: e.target.value });
  };
  // 提交禁言信息
  handleGapModal = () => {
    const { reason, gagTime } = this.state;
    if (!reason) {
      message.error('请输入禁言原因以告知用户');
      return;
    }
    this.props.handleGapModal(reason, gagTime);
  }
  handleCancel = () => {
    this.setState({
      reason: '',
      gagTime: '21600',
    });
    this.props.handleCancel();
  }
  render() {
    const { visible, isAudit } = this.props;
    const { reason, gagTime } = this.state;
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
    };
    return (
      <Modal
        title={isAudit ? '拒绝审核' : '禁言处理'}
        visible={visible}
        onOk={this.handleGapModal}
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem
            label={isAudit ? '拒绝原因' : '禁言原因'}
          >
            <TextArea
              placeholder="请描述具体的处理原因及警告以发送给用户"
              rows="6"
              onChange={this.handleReason}
              value={reason}
            />
          </FormItem>
          {isAudit ? '' :
          <FormItem
            {...formItemLayout}
            label="禁言时间"
          >
            <Select
              style={{ width: 120 }}
              onChange={this.handleGagTime}
              value={gagTime}
            >
              <Option value="21600">6小时</Option>
              <Option value="86400">1天</Option>
              <Option value="604800">7天</Option>
              <Option value="2592000">30天</Option>
              <Option value="9999999999">永久</Option>
            </Select>
          </FormItem>}
        </Form>
      </Modal>
    );
  }
}

export default GagUser;
