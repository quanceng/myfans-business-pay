import React, { Component } from 'react';
import styles from './Preview.less';

export default class Preview extends Component {
  render() {
    return (
      <div className={styles.mask}>
        <div className={styles.mobile_preview} id="mobileDiv" style={{ display: 'block' }}>
          <div className={styles.mobile_preview_hd}>
            <strong className={styles.nickname}>{this.props.initData.forum_info.name}</strong>
          </div>
          <div className={styles.mobile_preview_bd}>
            <h4>{this.props.title}</h4>
            <div className={styles.article_content} dangerouslySetInnerHTML={{ __html: this.props.content }} />
          </div>
          <div className={styles.mobile_preview_ft} />
          <div className={styles.moblie_preview_weapp} js_preview_weapp_wrapper style={{ display: 'none' }}>
            <div className={styles.weapp_hd}>
              <i className={styles.icon_back_weapp} js_preview_weapp_close>返回</i>
              <p className={styles.weapp_title} js_preview_weapp_title />
            </div>
            <div className={styles.preview_extra}>
              电脑端暂不支持预览小程序
            </div>
          </div>
          <a className={styles.mobile_preview_closed} btn btn_default onClick={this.props.closePreview} id="viewClose">退出预览</a>
        </div>
      </div>
    );
  }
}

