import React, { PureComponent } from 'react';
import { Button } from 'antd';

export default class SMSButton extends PureComponent {
  state = {
    smsStatus: 'init',
    time: 60,
  };
  // 发送验证码
  sendSMScode = () => {
    const data = { phone: this.props.phone, send_type: 2 };
    this.props.dispatch({
      type: 'common/send',
      payload: data,
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          smsStatus: 'pending',
        });
        this.startTimer();
      }
    });
  };
  // 倒计时
  startTimer = () => {
    this.timer = setInterval(() => {
      const time = this.state.time - 1;
      this.setState({ time });
      if (time <= 0) {
        clearInterval(this.timer);
        this.setState({ smsStatus: 'init', time: 60 });
      }
    }, 1000);
  }
  render() {
    const { smsStatus, time } = this.state;
    if (smsStatus === 'init') {
      return (
        <Button type="primary" onClick={() => this.sendSMScode()}>获取手机验证码</Button>
      );
    }
    return (
      <Button disabled>{time}s后重发</Button>
    );
  }
}

