import React, { PureComponent } from 'react';
import { Upload, Icon, message, Button } from 'antd';

export default class UploadCover extends PureComponent {
  state = {
    fileList: this.props.filename ? [{
      uid: -1,
      name: this.props.filename.substring(0, 22) === 'https://cdn.myfans.cc/' ? this.props.filename.substring(22) : this.props.filename,
      status: 'done',
      thumbUrl: this.props.filename.substring(0, 22) === 'https://cdn.myfans.cc/' ? this.props.filename : `https://cdn.myfans.cc/${this.props.filename}`,
    }] : [],
  };

  // 上传图片
  handleChange = (res) => {
    // 上传显示
    this.setState({
      fileList: res.fileList.slice(-1),
    });

    // 上传状态判断
    switch (res.file.status) {
      case 'error':
        message.error('上传图片失败');
        break;
      case 'done':
        message.success('上传图片成功');
        if (this.props.changeImg) {
          this.props.changeImg(res.file.response.data.filename);
        }
        break;
      default:
        break;
    }
  }

  // 删除图片
  handleRemove = (e) => {
    if (this.props.disabled) {
      return false;
    }
    if (this.props.removeImg) {
      this.props.removeImg(e);
    }
  }

  render() {
    const { fileList } = this.state;
    const uploadProps = {
      name: 'image',
      action: '/b/upload-image',
      accept: 'image/png,image/jpg,image/jpeg,imge/bmp,image/gif',
      listType: this.props.card ? 'picture-card' : 'picture',
      onRemove: this.handleRemove,
      beforeUpload(file) {
        const isPic = file.type === 'image/jpeg' || file.type === 'image/png';
        const limitedSize = file.size < 1048576;
        if (!isPic) {
          message.error('非法的图片格式，请重新选择');
          return false;
        }
        if (!limitedSize) {
          message.error('图片体积过大，请重新选择');
          return false;
        }
        return isPic && limitedSize;
      },
      onChange: this.handleChange,
    };
    return (
      <Upload {...uploadProps} fileList={fileList}>
        {this.props.card ?
          <div style={{ marginTop: 10 }}>
            <Icon type="upload" />
            <div>点击上传</div>
          </div> :
          <Button disabled={this.props.disabled ? true : false} type="ghost">
            <Icon type="upload" /> 点击上传
          </Button>
        }
        &nbsp;&nbsp;&nbsp;<span style={{ color: '#999' }}>{this.props.msg}</span>
      </Upload>);
  }
}

