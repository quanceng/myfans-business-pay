import React, { PureComponent } from 'react';
import { Radio, Button, DatePicker, Row, Col, Alert } from 'antd';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const { RangePicker } = DatePicker;

export default class Blank extends PureComponent {
  state = {
    radioValue: 'week',
    startedAt: null,
    endedAt: null,
  }

  // 快速选择
  handleRadioChange = (e) => {
    this.setState({ radioValue: e.target.value });
    this.props.handleSearch({ date_type: e.target.value });
  }

  // 选择时间范围
  handleDateRange = (e) => {
    this.setState({
      radioValue: null,
      startedAt: e[0].format('YYYY-MM-DD'),
      endedAt: e[1].format('YYYY-MM-DD'),
    });
  }

  // 筛选
  handleClick = () => {
    this.props.handleSearch({
      start_time: this.state.startedAt,
      end_time: this.state.endedAt,
    });
  }

  render() {
    const { radioValue } = this.state;

    // 快速选择
    const fastSelect = (
      <RadioGroup value={radioValue} onChange={this.handleRadioChange}>
        <RadioButton value="yesterday">昨日</RadioButton>
        <RadioButton value="week">最近7天</RadioButton>
        <RadioButton value="month">最近30天</RadioButton>
      </RadioGroup>
    );

    // 时间区间选择ho
    const rangeSelect = (
      <RangePicker onChange={this.handleDateRange} />
    );

    return (
      <div>
        <Row className="mt-10" gutter={20} type="flex">
          <Col>{fastSelect}</Col>
          <Col>{rangeSelect}</Col>
          <Col><Button type="primary" onClick={this.handleClick}>筛选</Button></Col>
        </Row>
        <Alert className="mt-20" message="数据每日凌晨更新" type="info" showIcon />
      </div>
    );
  }
}
