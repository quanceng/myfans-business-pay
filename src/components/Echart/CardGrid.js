import React, { PureComponent } from 'react';
import { Row, Col, Card, Icon, Tooltip } from 'antd';
import lodash from 'lodash';
import styles from './Echart.less';

export default class Blank extends PureComponent {
  render() {
    const { cardGridData } = this.props;

    return (
      <Card bordered={false} bodyStyle={{ padding: 0 }}>
        {
          lodash.map(cardGridData, item => (
            <Card.Grid key={item.key} style={{ width: item.width || '33.33%' }}>
              <Card.Meta
                description={(
                  <Row type="flex" justify="space-between">
                    <Col>{item.title}</Col>
                    <Col>
                      {
                        item.tip && (
                          <Tooltip title={item.tip}>
                            <Icon type="question-circle-o" />
                          </Tooltip>
                        )
                      }
                    </Col>
                  </Row>
                )}
              />
              <div className={styles.cardGridData}>{item.data}</div>
            </Card.Grid>
          ))
        }
      </Card>
    );
  }
}
