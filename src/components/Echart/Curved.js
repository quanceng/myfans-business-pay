import React, { PureComponent } from 'react';
import lodash from 'lodash';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';

export default class Curved extends PureComponent {
  componentDidMount() {
    this.initChart();
  }

  componentWillUpdate() {
    this.initChart();
  }

  initChart = () => {
    const { chartData, fields, title } = this.props;
    const series = [];
    const legendData = fields ? lodash.map(fields, item => item) : false;
    lodash.forEach(chartData.seriesData, (item) => {
      const data = lodash.size(item.data) === 0 ? [0] : item.data;
      series.push({ ...item, data, type: 'line', smooth: true });
    });

    const myEchart = echarts.init(this.node);
    myEchart.setOption({
      title: title && {
        text: title,
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#6a7985',
          },
        },
      },
      legend: {
        data: legendData,
      },
      grid: {
        left: '0',
        right: '5%',
        bottom: '0',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: false,
          data: chartData.xAxisData,
        },
      ],
      yAxis: [{ type: 'value' }],
      series,
    });
  }

  handleNode = (n) => {
    this.node = n;
  }

  render() {
    const height = this.props.height || '400px';

    return (
      <div ref={this.handleNode} style={{ height }} />
    );
  }
}
