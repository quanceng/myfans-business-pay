import request from '../utils/request';

// 消息模板
export async function getMsgTemplate() {
  return request('/message/0');
}

// 消息模板列表
export async function getMsgTemplateGo() {
  return request('/message');
}

// 保存模板
export async function setMsgTemplate(data) {
  return request('/message', {
    method: 'POST',
    body: data,
  });
}
