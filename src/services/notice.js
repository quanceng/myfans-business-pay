import request from '../utils/request';

// 获取公告列表
export async function noticeList() {
  return request('/notice');
}
// 获取公告详情
export async function noticeDetail(params) {
  return request(`/notice/${params}`);
}

// 新建和编辑公告
export async function editNotice(params) {
  return request('/notice', {
    method: 'POST',
    body: params,
  });
}
// 删除公告
export async function removeNotice(params) {
  return request(`/notice/${params}`, {
    method: 'DELETE',
  });
}
