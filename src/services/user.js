import { stringify } from 'qs';
import request from '../utils/request';
// 禁言
export async function updateGag(params) {
  return request('/gag', {
    method: 'POST',
    body: params,
  });
}
// 取消禁言
export async function cancelGag(params) {
  return request(`/gag/${params}`, {
    method: 'DELETE',
  });
}
// 用户拉黑
export async function updateBlack(params) {
  return request('/black', {
    method: 'POST',
    body: params,
  });
}
// 取消拉黑
export async function cancleBlack(params) {
  return request(`/black/${params}`, {
    method: 'DELETE',
  });
}
// 获取用户列表
export async function userList(params) {
  return request(`/user?${stringify(params)}`);
}
// 获取身份列表
export async function identityList() {
  return request('/identity');
}
// 获取管理员列表
export async function adminList(params) {
  return request(`/ring?${stringify(params)}`);
}
// 获取黑名单列表
export async function blackList(params) {
  return request(`/black?${stringify(params)}`);
}
// 成为管理员
export async function updateAdmin(params) {
  return request('/ring', {
    method: 'POST',
    body: params,
  });
}
// 取消管理员
export async function cancleAdmin(params) {
  return request(`/ring/${params}`, {
    method: 'DELETE',
  });
}
// 新建身份
export async function createIdt(params) {
  return request('/identity', {
    method: 'POST',
    body: params,
  });
}
// 编辑身份
export async function editIdt(params) {
  return request('/identity', {
    method: 'POST',
    body: params,
  });
}
// 删除身份
export async function deleteIdt(params) {
  return request(`/identity/${params}`, {
    method: 'DELETE',
  });
}
// 用户加入或者移除身份
export async function updateUserIdt(params) {
  return request(`/user/${params.identityId}`, {
    method: 'PUT',
    body: params.data,
  });
}
