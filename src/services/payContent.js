import { stringify } from 'qs';
import request from '../utils/request';

// 获取付费入圈信息
export async function getPayIn() {
  return request('/forum-pay');
}


// 获取付费入圈用户信息
export async function payInUser(params) {
  return request(`/forum-pay-list?${stringify(params)}`);
}
// 获取入圈链接
export async function getLink() {
  return request('/forum-pay-share');
}

// 更新付费入圈设置
export async function updatePayIn(data) {
  return request('/forum-pay', {
    method: 'POST',
    body: data,
  });
}
// 购买入圈码
export async function buyCode(data) {
  return request('/activation-code', {
    method: 'POST',
    body: data,
  });
}
// 入圈码列表
export async function buyList(params) {
  return request(`/activation-code?${stringify(params)}`);
}
