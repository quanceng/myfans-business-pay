import { stringify } from 'qs';
import request from '../utils/request';
// 获取帖子列表
export async function postList(params) {
  return request(`/post?${stringify(params)}`);
}
// 获取审核帖子列表
export async function auditList(params) {
  return request(`/post-review?${stringify(params)}`);
}
// 获取微帖设置
export async function postSet() {
  return request('/post-config');
}
// 设置微帖评论链接等级
export async function updateConfig(params) {
  return request('/post-config', {
    method: 'POST',
    body: params,
  });
}
// 通过审核
export async function passPost(params) {
  return request('/post-review', {
    method: 'POST',
    body: params,
  });
}
// 拒绝审核
export async function refusePost(params) {
  return request(`/post-review/${params.threadId}`, {
    method: 'DELETE',
    body: params.data,
  });
}
// 删除帖子
export async function removePost(params) {
  return request(`/post/${params}`, {
    method: 'DELETE',
  });
}
// 加精
export async function updateEssence(params) {
  return request('/essence', {
    method: 'POST',
    body: params,
  });
}
// 取消加精
export async function cancleEssence(params) {
  return request(`/essence/${params}`, {
    method: 'DELETE',
  });
}
// 置顶
export async function updateTop(params) {
  return request('/top', {
    method: 'POST',
    body: params,
  });
}
// 取消置顶
export async function cancleTop(params) {
  return request(`/top/${params}`, {
    method: 'DELETE',
  });
}
// 修改话题
export async function updateTopics(params) {
  return request(`/post/${params.topic_id}`, {
    method: 'PUT',
    body: {
      thread_id: params.threads_id,
    },
  });
}
// 新建微帖
export async function postCreate(params) {
  return request('/post', {
    method: 'POST',
    body: params,
  });
}
// 编辑微帖
export async function postEdit(params) {
  return request('/post', {
    method: 'POST',
    body: params,
  });
}
// 微帖详情
export async function postDetail(params) {
  return request(`/post/${params}`);
}
// 审核微帖详情
export async function auditDetail(params) {
  return request(`/post-review/${params}`);
}
// 发表评论
export async function postComments(params) {
  return request('/comment', {
    method: 'POST',
    body: params,
  });
}
// 评论列表
export async function commentsList(params) {
  return request(`/comment?${stringify(params)}`);
}
// 删除评论
export async function removeComments(params) {
  return request(`/comment/${params}`, {
    method: 'DELETE',
  });
}
