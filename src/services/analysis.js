import { stringify } from 'qs';
import request from '../utils/request';

export async function getStatsBasic(params) {
  return request(`/stats/basic?${stringify(params)}`);
}

export async function getForumBasic(params) {
  return request(`/stats/forum?${stringify(params)}`);
}

export async function getUserBasic(params) {
  return request(`/stats/user?${stringify(params)}`);
}
