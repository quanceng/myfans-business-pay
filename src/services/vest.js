import { stringify } from 'qs';
import request from '../utils/request';
// 获取马甲列表
export async function vestList(params) {
  return request(`/vest?${stringify(params)}`);
}
// 添加马甲
export async function addVest(params) {
  return request('/vest', {
    method: 'POST',
    body: params,
  });
}
// 编辑马甲
export async function editVest(params) {
  return request(`/vest/${params.vestId}`, {
    method: 'PUT',
    body: params.data,
  });
}
// 马甲详情
export async function vestDetail(params) {
  return request(`/vest/${params}`);
}
