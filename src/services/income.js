import { stringify } from 'qs';
import request from '../utils/request';
// 社区收入列表
export async function getWalletList(params) {
  return request(`/co-wallet?${stringify(params)}`);
}
// 获取钱包信息
export async function getMoney() {
  return request('/co-wallet/1');
}
// 提现记录
export async function getRecord(params) {
  return request(`/forum-withdraw?${stringify(params)}`);
}
// 提现账户获取
export async function getUser() {
  return request('/co-ali-trade');
}
// 更新提现账号
export async function updataAccount(params) {
  return request('/co-ali-trade', {
    method: 'POST',
    body: params,
  });
}
// 提现
export async function withdrawal(params) {
  return request('/forum-withdraw', {
    method: 'POST',
    body: params,
  });
}
