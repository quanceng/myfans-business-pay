import { stringify } from 'qs';
import request from '../utils/request';

// 获取签到人数
export async function signNum(params) {
  return request(`/sign-user-count?${stringify(params)}`);
}
// 获取签到统计
export async function signList(params) {
  return request(`/sign-user-data?${stringify(params)}`);
}
// 获取签到详细
export async function signDay(params) {
  return request(`/sign-user-info?${stringify(params)}`);
}
