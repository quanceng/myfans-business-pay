import request from '../utils/request';

export async function forum() {
  return request('/forum-index');
}
