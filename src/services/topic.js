import request from '../utils/request';
// 获取话题列表
export async function topicList() {
  return request('/topic');
}
// 获取话题设置列表
export async function GetTopicList() {
  return request('/topic-config');
}
// 更新话题设置
export async function setTopicDisplay(params) {
  return request('/topic-config', {
    method: 'POST',
    body: params,
  });
}
// 删除话题
export async function removeTopic(params) {
  return request(`/topic/${params}`, {
    method: 'DELETE',
  });
}
// 添加和编辑话题
export async function editTopic(params) {
  return request('/topic', {
    method: 'POST',
    body: params,
  });
}

// 话题排序
export async function sortTopic(params) {
  return request('/topic-sort', {
    method: 'POST',
    body: params,
  });
}
