import request from '../utils/request';

// 登录
export async function login(params) {
  return request('/login', {
    method: 'POST',
    body: params,
  });
}

// 登出
export async function logout() {
  return request('/logout');
}

// 获取圈子列表
export async function getForumList() {
  return request('/forum-list');
}

// 选择圈子
export async function getForum(params) {
  return request('/forum-list', {
    method: 'POST',
    body: params,
  });
}
