import request from '../utils/request';

// 获取粉丝圈信息
export async function getForumInfo() {
  return request('/forum');
}

// 更新粉丝圈信息
export async function updateForumInfo(params) {
  return request(`/forum/${params.forumId}`, {
    method: 'PUT',
    body: params.data,
  });
}

// 获取公众号推广
export async function getWechat() {
  return request('/extension');
}

// 设置公众号推广
export async function updateWechat(params) {
  return request('/extension', {
    method: 'POST',
    body: params,
  });
}

// 获取用户行为规则列表
export async function getBehaviorRules() {
  return request('/user-behavior-rules');
}

// 设置用户行为规则列表
export async function updateBehaviorRules(params) {
  return request('/user-behavior-rules', {
    method: 'POST',
    body: params,
  });
}
// 设置用户经验值上限
export async function updateBehaviorLimit(params) {
  return request('/user-behavior-daily', {
    method: 'POST',
    body: params,
  });
}

// 获取等级列表
export async function getLevel() {
  return request('/level');
}

// 更新等级列表
export async function updateLevel(params) {
  return request('/level', {
    method: 'POST',
    body: params,
  });
}

// 获取首页信息
export async function getCover() {
  return request('/cover');
}

// 更新首页信息
export async function updateCover(params) {
  return request('/cover', {
    method: 'POST',
    body: params,
  });
}

// 获取敏感词
export async function getSensitive() {
  return request('/sensitive');
}

// 更新敏感词
export async function updateSensitive(params) {
  return request('/sensitive', {
    method: 'POST',
    body: params,
  });
}

// 修改密码
export async function changePassword(params) {
  return request('/password', {
    method: 'POST',
    body: params,
  });
}
