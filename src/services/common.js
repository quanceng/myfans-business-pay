import request from '../utils/request';

// 获取模板信息
export async function getInitData() {
  return request('/base-data');
}
// 发送短信
export async function send(params) {
  return request('/sms', {
    method: 'POST',
    body: params,
  });
}
// 购买
export async function payFroum(params) {
  return request('/buy-professional', {
    method: 'POST',
    body: params,
  });
}
// 创建圈子
export async function createForum(params) {
  return request('/created-forum', {
    method: 'POST',
    body: params,
  });
}
