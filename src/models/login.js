import {
  login,
  logout,
  getForumList,
  getForum,
} from '../services/login';

export default {
  namespace: 'login',
  state: {
    forumList: [],
  },
  effects: {
    * login({ payload }, { call }) {
      const response = yield call(login, payload);
      return response;
    },
    * logout(_, { call }) {
      const response = yield call(logout);
      return response;
    },

    * getForumList(_, { call, put }) {
      const response = yield call(getForumList);
      if (response.code === 1) {
        yield put({
          type: 'saveForumList',
          payload: response.data,
        });
      }
      return response;
    },
    * chooseForum({ payload }, { call }) {
      const response = yield call(getForum, payload);
      return response;
    },
  },

  reducers: {
    saveForumList(state, action) {
      return {
        ...state,
        forumList: action.payload.data,
      };
    },
  },
};
