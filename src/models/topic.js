import { topicList, GetTopicList, setTopicDisplay, removeTopic, editTopic, sortTopic } from '../services/topic';

export default {
  namespace: 'topic',

  state: {
    allTopics: [],
    topics: [],
    updateTopics: [],
    topicDisplay: {
      force: 0,
      style: 0,
      user_force_topic: 0,
    },
  },

  effects: {
    // 获取话题列表
    *fetchTopic(_, { call, put }) {
      const response = yield call(topicList);
      if (response.code === 1) {
        yield put({
          type: 'saveTopic',
          payload: response.data,
        });
      }
    },
    // 删除话题
    *deleteTopic({ payload }, { call }) {
      const response = yield call(removeTopic, payload);
      if (response.code === 1) {
        return response;
      }
    },
    // 话题排序
    *sortTopic({ payload }, { call }) {
      const response = yield call(sortTopic, payload);
      if (response.code === 1) {
        return response;
      }
    },
    // 添加和编辑话题
    *fetchEditTopic({ payload }, { call }) {
      const response = yield call(editTopic, payload);
      return response;
    },

    // 获取话题设置
    *fetchTopicDisplay(_, { call, put }) {
      const response = yield call(GetTopicList);
      if (response.code === 1) {
        yield put({
          type: 'saveDisplay',
          payload: response.data,
        });
      }
    },
    // 修改话题设置
    *updateTopicDisplay({ payload }, { call, put }) {
      const response = yield call(setTopicDisplay, payload);
      if (response.code === 1) {
        yield put({ type: 'fetchTopicDisplay' });
      }
    },
  },

  reducers: {
    saveTopic(state, action) {
      const updateTopics = [];
      const topics = [];
      const allTopics = [];
      for (let i = 0; i < action.payload.length; i++) {
        const element = action.payload[i];
        if (!element.link && !element.is_admin && element.is_status) {
          updateTopics.push(element);
        }
        if (!element.link && element.is_status) {
          topics.push(element);
        }
        allTopics.push(element);
      }
      return {
        ...state,
        topics,
        updateTopics,
        allTopics: action.payload,
      };
    },
    saveDisplay(state, action) {
      return {
        ...state,
        topicDisplay: action.payload,
      };
    },
  },
};
