import { vestList, addVest, editVest, vestDetail } from '../services/vest';

export default {
  namespace: 'vest',

  state: {
    vests: [],
    vestPage: {},
    vestInfo: null,
  },

  effects: {
    // 获取马甲列表
    *fetchVest({ payload }, { call, put }) {
      const response = yield call(vestList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveVest',
          payload: response.data,
        });
      }
    },
    // 添加马甲
    *fetchAddVest({ payload }, { call }) {
      const response = yield call(addVest, payload);
      return response;
    },
    // 编辑马甲
    *fetchEditVest({ payload }, { call }) {
      const response = yield call(editVest, payload);
      return response;
    },
    // 马甲详情
    *vestDetail({ payload }, { call, put }) {
      const response = yield call(vestDetail, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveVestInfo',
          payload: response.data,
        });
      }
    },
  },

  reducers: {
    saveVest(state, action) {
      return {
        ...state,
        vests: action.payload.data,
        vestPage: {
          current: action.payload.current_page,
          total: action.payload.total,
          pageSize: action.payload.per_page,
        },
      };
    },
    saveVestInfo(state, action) {
      return {
        ...state,
        vestInfo: action.payload,
      };
    },
  },
};
