import { forum } from '../services/summary';

export default {
  namespace: 'summary',

  state: {
    forum: {
      date_list: [],
      forum_member_total: 0,
      member_list: [],
      notice_list: null,
      post_list: [],
      post_total: 0,
      visit_list: [],
      visit_total: 0,
      visit_yesterday: 0,
      notice_show: null,
    },
  },

  effects: {
    // 粉丝圈概况数据
    * forum(_, { call, put }) {
      const response = yield call(forum);
      if (response.code === 1) {
        yield put({
          type: 'saveForum',
          payload: response.data,
        });
        return response.data;
      }
    },
  },

  reducers: {
    saveForum(state, action) {
      return {
        ...state,
        forum: action.payload,
      };
    },
  },
};
