import { postList, removePost, updateEssence, cancleEssence, updateTop, cancleTop, updateTopics, postCreate, postDetail, postEdit, postComments, commentsList, removeComments, auditList, auditDetail, postSet, updateConfig, passPost, refusePost } from '../services/post';

export default {
  namespace: 'post',

  state: {
    posts: [],
    postPage: {},
    auditList: [],
    auditPage: {},
    postDetail: null,
    auditDetail: null,
    comments: [],
    commentsPage: {},
    postData: null,
    postConfig: {
      is_open_comment_level: 0,
      comment_level: 2,
      is_open_link_level: 0,
      link_level: 2,
      is_post_audit: 0,
      is_open_post_level: 0,
      post_level: 2,
      is_new_user_post: 0,
      new_user_post_time: 1,
    },
  },

  effects: {
    // 获取帖子列表
    *fetchPost({ payload }, { call, put, select }) {
      const postData = yield select(state => state.post.postData);
      let response;
      if (payload) {
        response = yield call(postList, payload);
        if (response.code === 1) {
          yield put({
            type: 'savePost',
            payload: response.data,
            postData: payload,
          });
        }
      } else {
        response = yield call(postList, postData);
        if (response.code === 1) {
          yield put({
            type: 'savePost',
            payload: response.data,
          });
        }
      }
    },
    // 获取审核帖子列表
    *auditList({ payload }, { call, put }) {
      const response = yield call(auditList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveAuditList',
          payload: response.data,
        });
      }
    },
    // 获取微帖设置
    *postSet(_, { call, put }) {
      const response = yield call(postSet);
      if (response.code === 1) {
        yield put({
          type: 'savePostSet',
          payload: response.data,
        });
      }
    },
    // 微帖设置
    *updateConfig({ payload }, { call }) {
      const response = yield call(updateConfig, payload);
      return response;
    },
    // 通过审核
    *passPost({ payload }, { call }) {
      const response = yield call(passPost, payload);
      return response;
    },
    // 拒绝审核
    *refusePost({ payload }, { call }) {
      const response = yield call(refusePost, payload);
      return response;
    },
    // 获取评论列表
    *fetchComments({ payload }, { call, put }) {
      const response = yield call(commentsList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveComments',
          payload: response.data,
        });
      }
    },
    // 删除评论
    *delComments({ payload }, { call }) {
      const response = yield call(removeComments, payload);
      return response;
    },
    // 获取帖子详情
    *fetchPostDetail({ payload }, { call, put }) {
      const response = yield call(postDetail, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },
    // 获取审核帖子详情
    *auditDetail({ payload }, { call, put }) {
      const response = yield call(auditDetail, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveAuditDetail',
          payload: response.data,
        });
      }
    },
    // 编辑帖子
    *editPost({ payload }, { call }) {
      const response = yield call(postEdit, payload);
      return response;
    },
    // 删除帖子
    *delPost({ payload }, { call }) {
      const response = yield call(removePost, payload);
      return response;
    },
    // 加精
    *fetchEssence({ payload }, { call }) {
      const response = yield call(updateEssence, payload);
      return response;
    },
    // 取消加精
    *cancleEssence({ payload }, { call }) {
      const response = yield call(cancleEssence, payload);
      return response;
    },
    // 置顶
    *fetchTop({ payload }, { call }) {
      const response = yield call(updateTop, payload);
      return response;
    },
    // 取消置顶
    *cancleTop({ payload }, { call }) {
      const response = yield call(cancleTop, payload);
      return response;
    },
    // 修改话题
    *uploadTopic({ payload }, { call }) {
      const response = yield call(updateTopics, payload);
      return response;
    },
    // 新建微帖
    *createPost({ payload }, { call }) {
      const response = yield call(postCreate, payload);
      return response;
    },
    // 新建评论
    *createComments({ payload }, { call }) {
      const response = yield call(postComments, payload);
      return response;
    },
  },

  reducers: {
    savePost(state, action) {
      const postPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        posts: action.payload.data,
        postPage,
        postData: action.postData ? action.postData : state.postData,
      };
    },
    saveAuditList(state, action) {
      const auditPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        auditList: action.payload.data,
        auditPage,
      };
    },
    saveDetail(state, action) {
      return {
        ...state,
        postDetail: action.payload,
      };
    },
    saveAuditDetail(state, action) {
      return {
        ...state,
        auditDetail: action.payload,
      };
    },
    savePostSet(state, action) {
      return {
        ...state,
        postConfig: action.payload,
      };
    },
    saveComments(state, action) {
      const commentsPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: action.payload.per_page,
      };
      return {
        ...state,
        comments: action.payload.data,
        commentsPage,
      };
    },
  },
};
