import { noticeList, noticeDetail, editNotice, removeNotice } from '../services/notice';

export default {
  namespace: 'notice',

  state: {
    noticeList: [],
    noticePage: {},
    noticeInfo: null,
  },

  effects: {
    // 删除公告
    *removeNotice({ payload }, { call }) {
      const response = yield call(removeNotice, payload);
      if (response.code === 1) {
        return response;
      }
    },
    // 新建和编辑公告
    *editNotice({ payload }, { call }) {
      const response = yield call(editNotice, payload);
      if (response.code === 1) {
        return response;
      }
    },

    // 公告详情
    *noticeDetail({ payload }, { call, put }) {
      const response = yield call(noticeDetail, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveNoticeInfo',
          payload: response.data,
        });
      }
    },
    // 获取公告列表
    *noticeList(_, { call, put }) {
      const response = yield call(noticeList);
      if (response.code === 1) {
        yield put({
          type: 'saveNoticeList',
          payload: response.data,
        });
      }
    },
  },

  reducers: {
    saveNoticeList(state, action) {
      const noticePage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        noticeList: action.payload.data,
        noticePage,
      };
    },
    saveNoticeInfo(state, action) {
      return {
        ...state,
        noticeInfo: action.payload,
      };
    },
  },
};
