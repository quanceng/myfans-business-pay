import { getMsgTemplate, getMsgTemplateGo, setMsgTemplate } from '../services/msgTemplate';

export default {
  namespace: 'msgTemplate',
  state: {
    msgTemplate: [],
    msgTemplateGo: null,
  },
  effects: {
    // 获取已有模板
    * msgTemplate(_, { call, put }) {
      const response = yield call(getMsgTemplate);
      if (response.code === 1) {
        yield put({
          type: 'setTemplate',
          payload: response.data,
        });
      }
    },
    // 消息模板列表
    * msgTemplateGo(_, { call, put }) {
      const response = yield call(getMsgTemplateGo);
      if (response.code === 1) {
        yield put({
          type: 'setTemplateGo',
          payload: response.data,
        });
      }
    },
    // 提交模板设置
    * saveMsgTemplate({ payload }, { call }) {
      const response = yield call(setMsgTemplate, payload);
      return response;
    },
  },
  reducers: {
    setTemplate(state, action) {
      return { ...state, msgTemplate: action.payload };
    },
    setTemplateGo(state, action) {
      return { ...state, msgTemplateGo: action.payload };
    },
  },
};
