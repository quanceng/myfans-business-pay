import {
  getInitData,
  send,
  payFroum,
  createForum,
} from '../services/common';

export default {
  namespace: 'common',

  state: {
    initData: {
      user_info: {},
      forum_info: {},
      pay_info: {},
    },
  },

  effects: {
    // 获取粉丝圈模板信息
    * getInitData(_, { call, put }) {
      const response = yield call(getInitData);
      if (response.code === 1) {
        yield put({
          type: 'saveInitData',
          payload: response.data,
        });
      }
      return response;
    },
    // 获取粉丝圈模板信息
    * send({ payload }, { call }) {
      const response = yield call(send, payload);
      return response;
    },
    // 购买
    * payFroum({ payload }, { call }) {
      const response = yield call(payFroum, payload);
      return response;
    },
    // 创建圈子
    * createForum({ payload }, { call }) {
      const response = yield call(createForum, payload);
      return response;
    },
  },

  reducers: {
    saveInitData(state, action) {
      return { ...state, initData: action.payload };
    },
  },
};
