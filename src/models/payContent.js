import { getPayIn, updatePayIn, getLink, payInUser, buyList, buyCode } from '../services/payContent';

export default {
  namespace: 'payContent',

  state: {
    payInUserList: [],
    payInUserPage: {},
    buyList: [],
    buyPage: {},
  },

  effects: {
    // 入圈码列表
    * buyList({ payload }, { call, put }) {
      const response = yield call(buyList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveBuyList',
          payload: response.data,
        });
      }
    },
    // 购买入圈码
    * buyCode({ payload }, { call }) {
      const response = yield call(buyCode, payload);
      return response;
    },
    // 获取付费入圈信息
    * getPayIn(_, { call }) {
      const response = yield call(getPayIn);
      return response;
    },
    // 更新付费入圈设置
    * updatePayIn({ payload }, { call }) {
      const response = yield call(updatePayIn, payload);
      return response;
    },
    // 获取付费入圈用户信息
    * payInUser({ payload }, { call, put }) {
      const response = yield call(payInUser, payload);
      if (response.code === 1) {
        yield put({
          type: 'savePayInUser',
          payload: response.data,
        });
      }
    },
    // 获取付费入圈链接
    * getLink(_, { call }) {
      const response = yield call(getLink);
      return response;
    },
  },

  reducers: {
    savePayInUser(state, action) {
      return {
        ...state,
        payInUserList: action.payload.data,
        payInUserPage: {
          current: action.payload.current_page,
          pageSize: action.payload.per_page,
          total: action.payload.total,
        },
      };
    },
    saveBuyList(state, action) {
      return { ...state, buyList: action.payload.data, buyPage: { current: action.payload.current_page, pageSize: 10, total: action.payload.total } };
    },
  },
};
