import {
  getStatsBasic,
  getForumBasic,
  getUserBasic,
} from '../services/analysis';

export default {
  namespace: 'analysis',

  state: {
    statsBasic: null,
    statsForum: null,
    statsUser: null,
  },

  effects: {
    * getStatsBasic({ payload }, { call, put }) {
      const response = yield call(getStatsBasic, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveStatsBasic',
          payload: response.data,
        });
      }
    },
    * getForumBasic({ payload }, { call, put }) {
      const response = yield call(getForumBasic, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveStatsForum',
          payload: response.data,
        });
      }
    },
    * getUserBasic({ payload }, { call, put }) {
      const response = yield call(getUserBasic, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveStatsUser',
          payload: response.data,
        });
      }
    },
  },

  reducers: {
    saveStatsBasic(state, action) {
      return { ...state, statsBasic: action.payload };
    },
    saveStatsForum(state, action) {
      return { ...state, statsForum: action.payload };
    },
    saveStatsUser(state, action) {
      return { ...state, statsUser: action.payload };
    },
  },
};
