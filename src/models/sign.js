import { signNum, signList, signDay } from '../services/sign';

export default {
  namespace: 'sign',

  state: {
    signNum: [],
    signList: [],
    signPage: {},
    signDayList: [],
    signDayPage: {},
  },

  effects: {
    // 获取签到人数
    *signNum({ payload }, { call, put }) {
      const response = yield call(signNum, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveSignNum',
          payload: response.data,
        });
      }
    },
    // 获取签到统计
    *signList({ payload }, { call, put }) {
      const response = yield call(signList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveSignList',
          payload: response.data,
        });
      }
    },
    // 获取签到详细
    *signDay({ payload }, { call, put }) {
      const response = yield call(signDay, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveSignDay',
          payload: response.data,
        });
      }
    },
  },

  reducers: {
    saveSignNum(state, action) {
      return {
        ...state,
        signNum: action.payload,
      };
    },
    saveSignDay(state, action) {
      const signDayPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        signDayList: action.payload.data,
        signDayPage,
      };
    },
    saveSignList(state, action) {
      const signPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        signList: action.payload.data,
        signPage,
      };
    },
  },
};
