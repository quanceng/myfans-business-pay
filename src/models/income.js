import { getMoney, getWalletList, getRecord, getUser, updataAccount, withdrawal } from '../services/income';

export default {
  namespace: 'income',

  state: {
    wallet: {},
    walletList: [],
    walletPage: {},
    recordList: [],
    recordPage: {},
    account: null,
  },

  effects: {
    // 获取钱包信息
    *getMoney(_, { call, put }) {
      const response = yield call(getMoney);
      if (response.code === 1) {
        yield put({
          type: 'saveMoney',
          payload: response.data,
        });
      }
    },
    // 提现账户获取
    *getUser(_, { call, put }) {
      const response = yield call(getUser);
      if (response.code === 1) {
        yield put({
          type: 'saveUser',
          payload: response.data,
        });
      }
    },
    // 社区收入列表
    *getWalletList({ payload }, { call, put }) {
      const response = yield call(getWalletList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveWalletList',
          payload: response.data,
        });
      }
    },
    // 提现记录
    *getRecord({ payload }, { call, put }) {
      const response = yield call(getRecord, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveRecord',
          payload: response.data,
        });
      }
    },
    // 更新提现账号
    *updataAccount({ payload }, { call }) {
      const response = yield call(updataAccount, payload);
      return response;
    },
    // 提现
    *withdrawal({ payload }, { call }) {
      const response = yield call(withdrawal, payload);
      return response;
    },
  },

  reducers: {
    saveMoney(state, action) {
      return {
        ...state,
        wallet: action.payload,
      };
    },
    saveUser(state, action) {
      return {
        ...state,
        account: action.payload,
      };
    },
    saveWalletList(state, action) {
      const walletPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        walletList: action.payload.data,
        walletPage,
      };
    },
    saveRecord(state, action) {
      const recordPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        recordList: action.payload.data,
        recordPage,
      };
    },
  },
};
