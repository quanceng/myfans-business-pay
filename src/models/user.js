import { updateGag, cancelGag, updateBlack, cancleBlack, userList, updateAdmin, cancleAdmin, adminList, blackList, identityList, createIdt, editIdt, deleteIdt, updateUserIdt } from '../services/user';

export default {
  namespace: 'user',

  state: {
    userList: [],
    userPage: {},
    adminList: [],
    adminPage: {},
    blackList: [],
    blackPage: {},
    idtUserList: [],
    idtPage: {},
  },

  effects: {
    // 禁言
    *fetchGag({ payload }, { call }) {
      const response = yield call(updateGag, payload);
      return response;
    },
    // 取消禁言
    *cancelGag({ payload }, { call }) {
      const response = yield call(cancelGag, payload);
      return response;
    },
    // 拉黑
    *fetchBlack({ payload }, { call }) {
      const response = yield call(updateBlack, payload);
      return response;
    },
    // 取消拉黑
    *cancleBlack({ payload }, { call }) {
      const response = yield call(cancleBlack, payload);
      return response;
    },
    // 新建身份
    *createIdt({ payload }, { call }) {
      const response = yield call(createIdt, payload);
      return response;
    },
    // 编辑身份
    *editIdt({ payload }, { call }) {
      const response = yield call(editIdt, payload);
      return response;
    },
    // 删除身份
    *deleteIdt({ payload }, { call }) {
      const response = yield call(deleteIdt, payload);
      return response;
    },
    // 获取用户列表
    *userList({ payload }, { call, put }) {
      const response = yield call(userList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveUserList',
          payload: response.data,
        });
      }
    },
    // 获取自定义身份用户列表
    *identityUserList({ payload }, { call, put }) {
      const response = yield call(userList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveIdtUserList',
          payload: response.data,
        });
      }
    },
    // 获取搜索身份用户列表
    *searchUserList({ payload }, { call }) {
      const response = yield call(userList, payload);
      return response;
    },
    // 获取身份列表
    *identityList(_, { call }) {
      const response = yield call(identityList);
      if (response.code === 1) {
        return response;
      }
    },
    // 获取黑名单列表
    *blackList({ payload }, { call, put }) {
      const response = yield call(blackList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveBlackList',
          payload: response.data,
        });
      }
    },
    // 获取管理员列表
    *adminList({ payload }, { call, put }) {
      const response = yield call(adminList, payload);
      if (response.code === 1) {
        yield put({
          type: 'saveAdminList',
          payload: response.data,
        });
      }
    },
    // 设置管理员
    *fetchAdmin({ payload }, { call }) {
      const response = yield call(updateAdmin, payload);
      return response;
    },
    // 取消管理员
    *cancleAdmin({ payload }, { call }) {
      const response = yield call(cancleAdmin, payload);
      return response;
    },
    // 用户加入或者移除身份
    *updateUserIdt({ payload }, { call }) {
      const response = yield call(updateUserIdt, payload);
      return response;
    },
  },

  reducers: {
    saveUserList(state, action) {
      const userPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: action.payload.per_page,
      };
      return {
        ...state,
        userList: action.payload.data,
        userPage,
      };
    },
    saveIdtUserList(state, action) {
      const idtPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        idtUserList: action.payload.data,
        idtPage,
      };
    },
    saveBlackList(state, action) {
      const blackPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        blackList: action.payload.data,
        blackPage,
      };
    },
    saveAdminList(state, action) {
      const adminPage = {
        current: action.payload.current_page,
        total: action.payload.total,
        pageSize: 10,
      };
      return {
        ...state,
        adminList: action.payload.data,
        adminPage,
      };
    },
  },
};
