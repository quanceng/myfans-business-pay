import {
  getForumInfo,
  updateForumInfo,
  changePassword,
  getWechat,
  updateWechat,
  getBehaviorRules,
  updateBehaviorRules,
  getLevel,
  updateLevel,
  getCover,
  updateCover,
  getSensitive,
  updateSensitive,
  updateBehaviorLimit,
} from '../services/forum';

export default {
  namespace: 'forum',

  state: {
    forumInfo: null,
    starInfo: null,
    wechat: null,
    sensitiveWords: { sensitive_words: '' },
    cover: null,
    backgroundArray: [
      { key: '3lkjlk4433kk1.jpg', custom: false },
      { key: '3lkjlk4433kk2.jpg', custom: false },
      { key: '3lkjlk4433kk3.jpg', custom: false },
      { key: '3lkjlk4433kk4.jpg', custom: false },
      { key: '3lkjlk4433kk5.jpg', custom: false },
    ],
    behaviorRules: {},
    behaviorLimit: 0,
    behaviorRulesEditor: {},
    level: [],
  },

  effects: {
    // 获取粉丝圈信息
    * getForumInfo(_, { call, put }) {
      const response = yield call(getForumInfo);
      if (response.code === 1) {
        yield put({
          type: 'saveForumInfo',
          payload: response.data,
        });
      }
    },
    // 更新粉丝圈信息
    * updateForumInfo({ payload }, { call }) {
      const response = yield call(updateForumInfo, payload);
      return response;
    },
    // 修改密码
    * changePassword({ payload }, { call }) {
      const response = yield call(changePassword, payload);
      return response;
    },
    // 获取公众号推广
    * getWechat(_, { call, put }) {
      const response = yield call(getWechat);
      if (response.code === 1) {
        yield put({
          type: 'saveWechat',
          payload: response.data,
        });
      }
    },
    // 更新公众号推广
    * updateWechat({ payload }, { call }) {
      const response = yield call(updateWechat, payload);
      return response;
    },
    // 获取经验值设置
    * getBehaviorRules(_, { call, put }) {
      const response = yield call(getBehaviorRules);
      if (response.code === 1) {
        yield put({
          type: 'saveBehaviorRules',
          payload: response.data,
        });
      }
    },
    // 更新经验值设置
    * updateBehaviorRules({ payload }, { call }) {
      const response = yield call(updateBehaviorRules, payload);
      return response;
    },
    // 更新经验值上限
    * updateBehaviorLimit({ payload }, { call }) {
      const response = yield call(updateBehaviorLimit, payload);
      return response;
    },
    // 获取等级列表
    * getLevel(_, { call, put }) {
      const response = yield call(getLevel);
      if (response.code === 1) {
        yield put({
          type: 'saveLevel',
          payload: response.data,
        });
      }
    },
    // 设置等级列表
    * updateLevel({ payload }, { call }) {
      const response = yield call(updateLevel, payload);
      return response;
    },
    // 获取首页信息
    * getCover(_, { call, put }) {
      const response = yield call(getCover);
      if (response.code === 1) {
        yield put({
          type: 'saveCover',
          payload: response.data,
        });
      }
    },
    // 更新首页信息
    * updateCover({ payload }, { call, put }) {
      const response = yield call(updateCover, payload);
      if (response.code === 1) {
        yield put({ type: 'getCover' });
      }

      return response;
    },
    // 获取敏感词
    * getSensitiveWords(_, { call, put }) {
      const response = yield call(getSensitive);
      yield put({
        type: 'saveSensitiveWords',
        payload: response.data,
      });
    },
    // 更新敏感词
    * updateSensitiveWords({ payload }, { call }) {
      const response = yield call(updateSensitive, payload);
      return response;
    },
  },

  reducers: {
    saveForumInfo(state, action) {
      return { ...state, forumInfo: action.payload };
    },
    saveStarInfo(state, action) {
      return { ...state, starInfo: action.payload };
    },
    saveWechat(state, action) {
      return { ...state, wechat: action.payload };
    },
    saveLevel(state, action) {
      return { ...state, level: action.payload };
    },
    saveBehaviorRules(state, action) {
      return { ...state, behaviorRules: action.payload.rules, behaviorLimit: action.payload.daily_limit };
    },
    saveSensitiveWords(state, action) {
      return { ...state, sensitiveWords: action.payload };
    },
    saveCover(state, action) {
      const newBackgroundArray = [...state.backgroundArray];
      if (!state.backgroundArray.some(item => item.key === action.payload.attach)) {
        newBackgroundArray.push({ key: action.payload.attach, custom: true });
      }
      return {
        ...state,
        backgroundArray: newBackgroundArray,
        cover: action.payload,
      };
    },
    addBackground(state, action) {
      const newBackgroundArray = [
        ...state.backgroundArray,
        { key: action.payload.attach, custom: true },
      ];
      return {
        ...state,
        backgroundArray: newBackgroundArray,
      };
    },
  },
};
