export default (app, dynamicWrapper) => {
  return {
    name: '社区管理',
    path: 'forum',
    children: [
      {
        name: '微帖管理',
        icon: 'file-text',
        path: 'post',
        children: [
          {
            path: '/',
            component: dynamicWrapper(
              app,
              ['post', 'topic', 'user'],
              () => import('../routes/Forum/Post/Post')
            ),
          },
          {
            name: '新建微帖',
            path: 'create',
            component: dynamicWrapper(
              app,
              ['post', 'topic'],
              () => import('../routes/Forum/Post/PostCreate')
            ),
          },
          {
            name: '编辑微帖',
            path: ':id/edit',
            component: dynamicWrapper(
              app,
              ['post', 'topic'],
              () => import('../routes/Forum/Post/PostEdit')
            ),
          },
          {
            name: '微帖详情',
            path: ':id',
            component: dynamicWrapper(
              app,
              ['post', 'user', 'vest'],
              () => import('../routes/Forum/Post/PostShow')
            ),
          },
        ],
      },
      {
        name: '话题管理',
        icon: 'tag-o',
        path: 'topic',
        component: dynamicWrapper(
          app,
          ['topic', 'user'],
          () => import('../routes/Forum/Topic/Topic')
        ),
      },
      {
        name: '用户管理',
        icon: 'team',
        path: 'user',
        component: dynamicWrapper(
          app,
          ['user', 'forum'],
          () => import('../routes/Forum/User/User')
        ),
      },
      {
        name: '社区收入',
        icon: 'pay-circle-o',
        path: 'income',
        children: [
          {
            path: '/',
            component: dynamicWrapper(
              app,
              ['income'],
              () => import('../routes/Forum/Income/Income')
            ),
          },
          {
            name: '提现记录',
            path: 'record',
            component: dynamicWrapper(
              app,
              ['income'],
              () => import('../routes/Forum/Income/Record')
            ),
          },
          {
            name: '提现申请',
            path: 'withdraw',
            component: dynamicWrapper(
              app,
              ['income'],
              () => import('../routes/Forum/Income/Withdraw')
            ),
          },
        ],
      },
      {
        name: '签到管理',
        icon: 'calendar',
        path: 'sign',
        component: dynamicWrapper(
          app,
          ['sign'],
          () => import('../routes/Forum/Sign/Sign')
        ),
      },
      {
        name: '公告管理',
        icon: 'notification',
        path: 'notice',
        children: [
          {
            path: '/',
            component: dynamicWrapper(
              app,
              ['notice'],
              () => import('../routes/Forum/Notice/Notice')
            ),
          },
          {
            name: '新建公告',
            path: 'create',
            component: dynamicWrapper(
              app,
              ['notice'],
              () => import('../routes/Forum/Notice/NoticeCreate')
            ),
          },
          {
            name: '编辑公告',
            path: ':id/edit',
            component: dynamicWrapper(
              app,
              ['notice'],
              () => import('../routes/Forum/Notice/NoticeEdit')
            ),
          },
        ],
      },
      {
        name: '马甲管理',
        icon: 'solution',
        path: 'vest',
        children: [
          {
            path: '/',
            component: dynamicWrapper(
              app,
              ['vest', 'post', 'topic'],
              () => import('../routes/Forum/Vest/Vest')
            ),
          },
          {
            name: '新建马甲',
            path: 'create',
            component: dynamicWrapper(
              app,
              ['vest'],
              () => import('../routes/Forum/Vest/VestCreate')
            ),
          },
          {
            name: '编辑马甲',
            path: ':id/edit',
            component: dynamicWrapper(
              app,
              ['vest'],
              () => import('../routes/Forum/Vest/VestEdit')
            ),
          },
        ],
      },
    ],
  };
};
