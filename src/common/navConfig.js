export default (app, dynamicWrapper) => {
  return {
    name: '设置',
    path: 'config',
    children: [
      {
        name: '基础设置',
        icon: 'setting',
        path: 'basic',
        component: dynamicWrapper(
          app,
          ['forum'],
          () => import('../routes/Config/Basic/Basic')
        ),
      },
      {
        name: '公众号推广',
        icon: 'wechat',
        path: 'wechat',
        component: dynamicWrapper(
          app,
          ['forum'],
          () => import('../routes/Config/Wechat')
        ),
      },
      {
        name: '经验值等级',
        icon: 'rocket',
        path: 'behavior',
        component: dynamicWrapper(
          app,
          ['forum'],
          () => import('../routes/Config/Behavior/Behavior')
        ),
      },
      {
        name: '首页设置',
        icon: 'mobile',
        path: 'cover',
        component: dynamicWrapper(
          app,
          ['forum'],
          () => import('../routes/Config/Cover/Cover')
        ),
      },
      {
        name: '敏感词设置',
        icon: 'frown-o',
        path: 'sensitive',
        component: dynamicWrapper(
          app,
          ['forum'],
          () => import('../routes/Config/Sensitive')
        ),
      },
    ],
  };
};
