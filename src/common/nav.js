import dynamic from 'dva/dynamic';
import navDashboard from './navDashboard';
import navForum from './navForum';
import navApplication from './navApplication';
import navAnalysis from './navAnalysis';
import navConfig from './navConfig';

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => dynamic({
  app,
  models: () => models.map(m => import(`../models/${m}.js`)),
  component,
});

// nav data
export const getNavData = app => [
  {
    component: dynamicWrapper(app, ['login'], () => import('../layouts/BasicLayout')),
    layout: 'BasicLayout',
    name: '首页', // for breadcrumb
    path: '/',
    children: [
      navDashboard(app, dynamicWrapper),
      navForum(app, dynamicWrapper),
      navApplication(app, dynamicWrapper),
      navAnalysis(app, dynamicWrapper),
      navConfig(app, dynamicWrapper),
    ],
  },
  {
    component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    path: '/account',
    layout: 'UserLayout',
    children: [
      {
        name: '帐户',
        path: 'account',
        children: [
          {
            name: '登录',
            path: 'login',
            component: dynamicWrapper(app, ['login'], () => import('../routes/Login/Login')),
          },
          {
            name: '圈子列表',
            path: 'list',
            component: dynamicWrapper(app, ['login'], () => import('../routes/Login/ForumList')),
          },
          // {
          //   name: '新建圈子',
          //   path: 'add',
          //   component: dynamicWrapper(app, ['login'], () => import('../routes/Login/AddList')),
          // },
        ],
      },
    ],
  },
];
