export default (app, dynamicWrapper) => {
  return {
    name: '数据分析',
    path: 'analysis',
    children: [
      {
        name: '基础数据',
        icon: 'line-chart',
        path: 'basic',
        component: dynamicWrapper(
          app,
          ['analysis'],
          () => import('../routes/Analysis/Basic')
        ),
      },
      {
        name: '粉丝圈数据',
        icon: 'bar-chart',
        path: 'forum',
        component: dynamicWrapper(
          app,
          ['analysis'],
          () => import('../routes/Analysis/Forum')
        ),
      },
      {
        name: '用户数据',
        icon: 'user',
        path: 'user',
        component: dynamicWrapper(
          app,
          ['analysis'],
          () => import('../routes/Analysis/User')
        ),
      },
    ],
  };
};
