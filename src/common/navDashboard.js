export default (app, dynamicWrapper) => {
  return {
    name: '概况',
    path: 'dashboard',
    children: [
      {
        name: '粉丝圈概况',
        icon: 'home',
        path: 'summary',
        component: dynamicWrapper(
          app,
          ['summary'],
          () => import('../routes/Dashboard/Summary')
        ),
      },
      {
        name: '续费管理',
        icon: 'pay-circle-o',
        path: 'pay',
        component: dynamicWrapper(
          app,
          [],
          () => import('../routes/Dashboard/Pay')
        ),
      },
    ],
  };
};
