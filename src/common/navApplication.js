export default (app, dynamicWrapper) => {
  return {
    name: '付费服务',
    path: 'application',
    children: [
      {
        name: '付费内容',
        icon: 'pay-circle-o',
        path: 'pay-content',
        children: [
          {
            name: '付费内容',
            path: '/',
            component: dynamicWrapper(
              app,
              ['payContent', 'user'],
              () => import('../routes/Application/PayContent/PayContent')
            ),
          },
          {
            name: '购买激活码',
            path: 'buy',
            component: dynamicWrapper(
              app,
              ['payContent'],
              () => import('../routes/Application/PayContent/BuyCode')
            ),
          },
        ],
      },
      // {
      //   name: '模板消息',
      //   icon: 'copy',
      //   path: 'msg-template',
      //   component: dynamicWrapper(
      //     app,
      //     ['msgTemplate'],
      //     () => import('../routes/Application/MsgTemplate/MsgTemplate')
      //   ),
      // },
    ],
  };
};
