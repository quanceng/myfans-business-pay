import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Row, Col, Menu, Icon, Avatar, Dropdown, Popover, Button, Spin } from 'antd';
import GlobalFooter from 'ant-design-pro/lib/GlobalFooter';
// import NoticeIcon from 'ant-design-pro/lib/NoticeIcon';
import DocumentTitle from 'react-document-title';
import { connect } from 'dva';
import { Link, Route, Redirect, Switch, routerRedux } from 'dva/router';
import NotFound from '../routes/Exception/404';
import styles from './BasicLayout.less';
import logo from '../assets/logo.png';

const { Header, Sider, Content } = Layout;

class BasicLayout extends React.PureComponent {
  static childContextTypes = {
    location: PropTypes.object,
    breadcrumbNameMap: PropTypes.object,
  }
  constructor(props) {
    super(props);
    // 把一级 Layout 的 children 作为菜单项
    this.menus = props.navData.reduce((arr, current) => arr.concat(current.children), []);
    this.state = {
      openKeys: [],
      qrCode: '',
      loading: true,
    };
  }
  componentDidMount() {
    this.getInitData();
    this.renderQrCode();
  }
  // 获取模板数据
  getInitData = () => {
    this.props.dispatch({
      type: 'common/getInitData',
    }).then(() => {
      this.setState({ loading: false });
    });
  };
  getChildContext() {
    const { location, navData, getRouteData } = this.props;
    const routeData = getRouteData('BasicLayout');
    const firstMenuData = navData.reduce((arr, current) => arr.concat(current.children), []);
    const menuData = this.getMenuData(firstMenuData, '');
    const breadcrumbNameMap = {};

    routeData.concat(menuData).forEach((item) => {
      breadcrumbNameMap[item.path] = {
        name: item.name,
        component: item.component,
      };
    });
    return { location, breadcrumbNameMap };
  }
  onMenuClick = ({ key }) => {
    if (key === 'logout') {
      this.props.dispatch({
        type: 'login/logout',
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.props.dispatch(routerRedux.push('/account/login'));
          }, 1000);
        }
      });
    }
  }
  getMenuData = (data, parentPath) => {
    let arr = [];
    data.forEach((item) => {
      if (item.children) {
        arr.push({ path: `${parentPath}/${item.path}`, name: item.name });
        arr = arr.concat(this.getMenuData(item.children, `${parentPath}/${item.path}`));
      }
    });
    return arr;
  }
  getCurrentMenuSelectedKeys(props) {
    const { location: { pathname } } = props || this.props;
    const keys = pathname.split('/').slice(1);
    if (keys.length === 1 && keys[0] === '') {
      return [this.menus[0].key];
    }
    return keys;
  }
  // 侧边menu
  getNavMenuItems(menusData) {
    if (!menusData) {
      return [];
    }

    // 路由keys
    const { location: { pathname }, forumCommon } = this.props;
    const keys = pathname.split('/').slice(1);

    // 只选择当前路由下的子菜单
    let menuParent = null;
    let menus = menusData;
    if (forumCommon.pay_info.end_status === 0) {
      menus = [
        {
          name: '概况',
          path: 'dashboard',
          children: {
            ...menusData[0].children[1],
          },
        },
      ];
    }
    menus.map((item) => {
      // 没有name不显示
      if (!item.name) {
        return null;
      }

      // 只选择当前一级路由下的子菜单
      if (keys.some(key => key === item.path)) {
        menuParent = item;
      }

      return null;
    });

    // 选定一级菜单
    if (!!menuParent && !!menuParent.children) {
      const menuItems = [];
      if (menuParent.children instanceof Array) {
        menuParent.children.forEach((child) => {
          menuItems.push(this.getNavMenuItem(child, menuParent.path));
        });
      } else {
        menuItems.push(this.getNavMenuItem(menuParent.children, menuParent.path));
      }

      return menuItems;
    }
    return [];
  }
  // 菜单item
  getNavMenuItem = (item, parentPath) => {
    // 主动隐藏
    if (item.hidden === true) {
      return null;
    }

    // icon
    const icon = item.icon && <Icon type={item.icon} />;

    // 跳转地址
    let itemPath;
    if (item.path.indexOf('http') === 0) {
      itemPath = item.path;
    } else {
      // 多个斜杠变成一个
      itemPath = `/${parentPath}/${item.path || ''}`.replace(/\/+/g, '/');
    }

    return (
      <Menu.Item key={item.key || item.path}>
        {
        /^https?:\/\//.test(itemPath) ? (
          <a href={itemPath} target={item.target}>
            {icon}<span>{item.name}</span>
          </a>
        ) : (
          <Link
            to={itemPath}
            replace={itemPath === this.props.location.pathname}
          >
            {icon}<span>{item.name}</span>
          </Link>
        )
        }
      </Menu.Item>
    );
  }
  renderQrCode = () => {
    const startTime = new Date('2017/08/07 09:00:00');
    const nowTime = new Date();
    switch (Math.floor(((nowTime - startTime) / 1000 / 60 / 60 / 24 / 6) % 2)) {
      case 0:
        this.setState({
          qrCode: 'https://cdn.myfans.cc/cMMFteMSHQl1530761156.jpg',
        });
        break;
      case 1:
        this.setState({
          qrCode: 'https://cdn.myfans.cc/OwYpX7s66Is1530761156.jpg',
        });
        break;
      default:
    }
  }
  getPageTitle() {
    const { location, getRouteData } = this.props;
    const { pathname } = location;
    let title = '粉丝圈';
    getRouteData('BasicLayout').forEach((item) => {
      if (item.path === pathname) {
        title = `${item.name} - 粉丝圈`;
      }
    });
    return title;
  }
  handleOpenChange = (openKeys) => {
    const lastOpenKey = openKeys[openKeys.length - 1];
    const isMainMenu = this.menus.some(
      item => lastOpenKey && (item.key === lastOpenKey || item.path === lastOpenKey)
    );
    this.setState({
      openKeys: isMainMenu ? [lastOpenKey] : [...openKeys],
    });
  }
  render() {
    const { getRouteData, forumCommon, location } = this.props;
    const { qrCode, loading } = this.state;
    const mySelectKey = location.pathname.split('/')[1];
    const expired = (forumCommon.pay_info.end_status === 0);
    // 顶部menu
    const HeaderMenu = (
      <Menu
        mode="horizontal"
        className={styles.headerMenu}
        theme="dark"
        selectedKeys={[mySelectKey]}
      >
        <Menu.Item key="dashboard" disabled={expired}>
          <Link to="/dashboard/summary">概况</Link>
        </Menu.Item>
        <Menu.Item key="forum" disabled={expired}>
          <Link to="/forum/post">社区管理</Link>
        </Menu.Item>
        <Menu.Item key="application" disabled={expired}>
          <Link to="/application/pay-content">付费服务</Link>
        </Menu.Item>
        <Menu.Item key="analysis" disabled={expired}>
          <Link to="/analysis/basic">数据分析</Link>
        </Menu.Item>
        <Menu.Item key="config" disabled={expired}>
          <Link to="/config/basic">设置</Link>
        </Menu.Item>
      </Menu>
    );

    // 粉丝圈menu
    const menu = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        <Menu.Item disabled={expired}>
          <Link to="/config/basic"><Icon type="setting" />粉丝圈设置</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/account/list"><Icon type="desktop" />切换粉丝圈</Link>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="logout">
          <Icon type="logout" />退出登录
        </Menu.Item>
      </Menu>
    );

    const menuProps = {
      openKeys: this.state.openKeys,
    };

    // 联系客服
    const customerContent = (
      <div className={styles.customerQrcode}>
        <img
          src={qrCode}
          alt="客服微信二维码"
        />
        <p style={{ fontSize: 14 }}>请扫描二维码，添加客服微信</p>
      </div>
    );

    const layout = (
      <Layout style={{ minHeight: '100%' }}>
        {/* 头部 */}
        <Header className={styles.header}>
          <Row className={styles.headerContent}>
            {/* logo */}
            <Col span={4} >
              <div className={styles.logo}>
                <Link to="/account/list">
                  <img src={logo} alt="logo" />
                </Link>
              </div>
            </Col>
            {/* 头部菜单 */}
            <Col span={14}>
              {HeaderMenu}
            </Col>
            <Col span={6}>
              <div className="text-right">
                {/* <Link style={{ color: '#fff' }} to="/application/letter">
                  <NoticeIcon className={styles.action} count={forumCommon.unread} />
                </Link> */}
                <Dropdown overlay={menu}>
                  <span className={`${styles.action} ${styles.account}`}>
                    <Avatar size="small" className={styles.avatar} src={forumCommon.forum_info.logo} />
                    {forumCommon.forum_info.name}
                  </span>
                </Dropdown>
              </div>
            </Col>
          </Row>
        </Header>
        {/* 主体 */}
        <Content>
          <Layout className={styles.layoutContent}>
            {/* 边栏menu */}
            <Sider
              trigger={null}
              width={210}
              className={styles.sider}
            >
              <Menu
                mode="inline"
                {...menuProps}
                onOpenChange={this.handleOpenChange}
                selectedKeys={this.getCurrentMenuSelectedKeys()}
                className={styles.siderMenu}
              >
                {this.getNavMenuItems(this.menus)}
              </Menu>
            </Sider>
            {/* 内容 */}
            <Content className={styles.mainContent}>
              {loading ?
                <Spin size="large" style={{ width: '100%', margin: '40px auto' }} /> :
                <div style={{ minHeight: 'calc(65vh - 50px)' }}>
                  <Switch>
                    {
                      getRouteData('BasicLayout').map(item =>
                        (
                          <Route
                            exact={item.exact}
                            key={item.path}
                            path={item.path}
                            component={item.component}
                          />
                        )
                      )
                    }
                    <Redirect exact from="/" to="/dashboard/summary" />
                    <Route component={NotFound} />
                  </Switch>
                </div>
              }
              {/* 底部信息 */}
              <GlobalFooter
                // links={[
                //   { title: '帮助', href: '#/dashboard/help' },
                //   { title: '条款', blankTarget: true, href: '/v2/b/account#/business' },
                // ]}
                copyright={
                  <div>
                    Copyright <Icon type="copyright" /> 2018 圈层网络
                  </div>
                }
              />
              <div className={styles.customer} ref={(n) => { this.customer = n; }}>
                <Popover
                  title="联系客服"
                  content={customerContent}
                  trigger="hover"
                  getPopupContainer={() => this.customer}
                >
                  <Row className={styles.row} type="flex" align="middle" justify="center">
                    <Col>
                      <img
                        className={styles.customerIcon}
                        src="https://cdn.myfans.cc/4w81RDaApwo1515058034.jpg"
                        alt="Customer Service"
                      />
                    </Col>
                    <Col>
                      <Button type="primary">联系客服</Button>
                    </Col>
                  </Row>
                </Popover>
              </div>
            </Content>
          </Layout>
        </Content>
      </Layout>
    );
    return (
      <DocumentTitle title={this.getPageTitle()}>
        {layout}
      </DocumentTitle>
    );
  }
}

export default connect(state => ({
  forumCommon: state.common.initData,
}))(BasicLayout);
