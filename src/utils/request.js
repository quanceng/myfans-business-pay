import fetch from 'dva/fetch';
import { message, notification } from 'antd';

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  if (response.status === 401) {
    setTimeout(() => {
      window.location.href = '#/account/login';
    }, 300);
  }
  if (response.status === 506) {
    setTimeout(() => {
      window.location.href = '#/dashboard/pay';
    }, 300);
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function checkBody(response) {
  if (response.msg) {
    if (response.code === 1) {
      message.success(response.msg, 2);
    } else {
      message.error(response.msg, 2);
    }
  }

  return response;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  const defaultOptions = {
    credentials: 'include',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'X-Requested-With': 'XMLHttpRequest',
    },
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT' || newOptions.method === 'DELETE') {
    newOptions.body = JSON.stringify(newOptions.body);
  }

  const urlAfter = `/b${url}`;

  return fetch(urlAfter, newOptions)
    .then(checkStatus)
    .then(response => response.json())
    .then(checkBody)
    .catch((error) => {
      if (error.code) {
        notification.error({
          message: error.name,
          description: error.message,
        });
      }
      if ('stack' in error && 'message' in error) {
        notification.error({
          message: '请求错误，',
          description: '请刷新页面重试！',
        });
      }
      return error;
    });
}
