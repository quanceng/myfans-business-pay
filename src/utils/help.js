import moment from 'moment';

export function timestampToFormat(timestamp, format) {
  return moment(Number(`${timestamp}000`)).format(format || 'YYYY-MM-DD HH:mm');
}
