import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd';
import lodash from 'lodash';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import Curved from '../../components/Echart/Curved';
import SearchFrom from '../../components/Echart/SearchForm';
import CardGrid from '../../components/Echart/CardGrid';
import styles from './Basic.less';

@connect(state => ({
  statsBasic: state.analysis.statsBasic,
}))
export default class Basic extends PureComponent {
  state = {
    loading: false,
  }

  componentDidMount() {
    this.getStatsBasic({ date_type: 'week' });
  }

  // 获取粉丝圈信息
  getStatsBasic = (params) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'analysis/getStatsBasic',
      payload: params,
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    const { statsBasic } = this.props;
    const { loading } = this.state;

    // 头部筛选数据
    const pageHeaderContent = <SearchFrom handleSearch={this.getStatsBasic} />;

    // 统计数据
    const cardGridData = [
      {
        key: 1,
        title: '访问量（PV）',
        tip: '用户对粉丝圈页面的浏览量，即用户每打开或刷新一个页面就记录1次，多次打开或刷新同一页面则浏览量累计。',
        data: (statsBasic && statsBasic.all_data.data.visits) || 0,
      },
      {
        key: 2,
        title: '独立访客数（UV）',
        tip: '通过互联网访问、浏览粉丝圈的访客，一天内同个访客多次访问仅计算1次',
        data: (statsBasic && statsBasic.all_data.data.users) || 0,
      },
      {
        key: 3,
        title: 'ip数（IP）',
        tip: '访问粉丝圈的ip总数，一天内同一ip多次访问、浏览粉丝圈算1次ip',
        data: (statsBasic && statsBasic.all_data.data.ips) || 0,
      },
    ];

    // 图表数据
    const xAxisData = [];
    const pvData = [];
    const uvData = [];
    const ipData = [];
    if (statsBasic) {
      lodash.forEach(statsBasic.date_list, (item, index) => {
        xAxisData.push(item);
        pvData.push(statsBasic.basic_data[index].data.visits);
        uvData.push(statsBasic.basic_data[index].data.users);
        ipData.push(statsBasic.basic_data[index].data.ips);
      });
    }
    const fields = { pv: '浏览量', uv: '访客数', ip: '独立访客数' };
    const chartData = {
      xAxisData,
      seriesData: [
        { name: fields.pv, data: pvData },
        { name: fields.uv, data: uvData },
        { name: fields.ip, data: ipData },
      ],
    };

    return (
      <PageHeaderLayout
        content={pageHeaderContent}
      >
        <Spin spinning={loading} >
          <CardGrid cardGridData={cardGridData} />
          <div className={styles.chartWrapper}>
            <Curved chartData={chartData} fields={fields} />
          </div>
        </Spin>
      </PageHeaderLayout>
    );
  }
}
