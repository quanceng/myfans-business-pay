import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd';
import lodash from 'lodash';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import Curved from '../../components/Echart/Curved';
import SearchFrom from '../../components/Echart/SearchForm';
import CardGrid from '../../components/Echart/CardGrid';
import styles from './Basic.less';

@connect(state => ({
  statsForum: state.analysis.statsForum,
}))
export default class Forum extends PureComponent {
  state = {
    loading: false,
  }

  componentDidMount() {
    this.getForumBasic({ date_type: 'week' });
  }

  // 获取粉丝圈信息
  getForumBasic = (params) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'analysis/getForumBasic',
      payload: params,
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    const { statsForum } = this.props;
    const { loading } = this.state;

    // 头部筛选数据
    const pageHeaderContent = <SearchFrom handleSearch={this.getForumBasic} />;

    // 统计数据
    const cardGridData = [
      {
        key: 1,
        title: '微帖量',
        width: '20%',
        data: (statsForum && statsForum.forum_data.data.post_total) || 0,
      },
      {
        key: 2,
        title: '评论数',
        width: '20%',
        data: (statsForum && statsForum.forum_data.data.comment_total) || 0,
      },
      {
        key: 3,
        title: '收藏量',
        width: '20%',
        data: (statsForum && statsForum.forum_data.data.favorite_total) || 0,
      },
      {
        key: 4,
        title: '分享量',
        width: '20%',
        data: (statsForum && statsForum.forum_data.data.share_total) || 0,
      },
      {
        key: 5,
        title: '点赞量',
        width: '20%',
        data: (statsForum && statsForum.forum_data.data.like_total) || 0,
      },
    ];

    // 图表数据
    const xAxisData = [];
    const postData = [];
    const commentData = [];
    const favoriteData = [];
    const shareData = [];
    const likeData = [];
    if (statsForum) {
      lodash.forEach(statsForum.date_list, (item, index) => {
        xAxisData.push(item);
        postData.push(statsForum.basic_data[index].data.post_total || 0);
        commentData.push(statsForum.basic_data[index].data.comment_total || 0);
        favoriteData.push(statsForum.basic_data[index].data.favorite_total || 0);
        shareData.push(statsForum.basic_data[index].data.share_total || 0);
        likeData.push(statsForum.basic_data[index].data.like_total || 0);
      });
    }
    const fields = {
      post: '微帖量',
      comment: '评论数',
      favorite: '收藏量',
      share: '分享量',
      like: '点赞量',
    };
    const chartData = {
      xAxisData,
      seriesData: [
        { name: fields.post, data: postData },
        { name: fields.comment, data: commentData },
        { name: fields.favorite, data: favoriteData },
        { name: fields.share, data: shareData },
        { name: fields.like, data: likeData },
      ],
    };
    return (
      <PageHeaderLayout
        content={pageHeaderContent}
      >
        <Spin spinning={loading} >
          <CardGrid cardGridData={cardGridData} />
          <div className={styles.chartWrapper}>
            <Curved chartData={chartData} fields={fields} />
          </div>
        </Spin>
      </PageHeaderLayout>
    );
  }
}
