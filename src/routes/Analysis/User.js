import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd';
import lodash from 'lodash';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import Curved from '../../components/Echart/Curved';
import SearchFrom from '../../components/Echart/SearchForm';
import CardGrid from '../../components/Echart/CardGrid';
import styles from './Basic.less';

@connect(state => ({
  statsUser: state.analysis.statsUser,
}))
export default class User extends PureComponent {
  state = {
    loading: false,
  }

  componentDidMount() {
    this.getUserBasic({ date_type: 'week' });
  }

  // 获取粉丝圈信息
  getUserBasic = (params) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'analysis/getUserBasic',
      payload: params,
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    const { statsUser } = this.props;
    const { loading } = this.state;

    // 头部筛选数据
    const pageHeaderContent = <SearchFrom handleSearch={this.getUserBasic} />;

    // 统计数据
    const cardGridData = [
      {
        key: 1,
        title: '新增用户数',
        data: (statsUser && statsUser.new_member) || 0,
      },
      {
        key: 2,
        title: '活跃用户数',
        tip: '每天进入过社区首页的用户数的总和',
        data: (statsUser && statsUser.visit_total) || 0,
      },
      {
        key: 3,
        title: '用户总数',
        data: (statsUser && statsUser.forum_member) || 0,
      },
    ];

    // 图表数据
    const xAxisData = [];
    const newData = [];
    const visitData = [];
    if (statsUser) {
      lodash.forEach(statsUser.date_list, (item, index) => {
        xAxisData.push(item);
        newData.push(statsUser.member_list[index].data.total || 0);
        visitData.push(statsUser.member_list[index].data.visits || 0);
      });
    }
    const fields = {
      new: '新增用户数',
      visit: '活跃用户数',
    };
    const chartData = {
      xAxisData,
      seriesData: [
        { name: fields.new, data: newData },
        { name: fields.visit, data: visitData },
      ],
    };

    return (
      <PageHeaderLayout
        content={pageHeaderContent}
      >
        <Spin spinning={loading} >
          <CardGrid cardGridData={cardGridData} />
          <div className={styles.chartWrapper}>
            <Curved chartData={chartData} fields={fields} />
          </div>
        </Spin>
      </PageHeaderLayout>
    );
  }
}
