import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form, Input, Button, Divider } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const FormItem = Form.Item;
const { TextArea } = Input;

@connect(state => ({
  forum: state.forum,
}))
@Form.create()
export default class Sensitive extends PureComponent {
  componentDidMount() {
    this.getSensitiveWords();
  }

  // 获取敏感词
  getSensitiveWords = () => {
    this.props.dispatch({
      type: 'forum/getSensitiveWords',
    });
  }

  // 更新敏感词
  updateSensitiveWords = (params) => {
    this.props.dispatch({
      type: 'forum/updateSensitiveWords',
      payload: params,
    });
  }

  // 提交表单
  handleSubmit = (e) => {
    e.preventDefault();
    const data = this.props.form.getFieldsValue();
    return this.updateSensitiveWords(data);
  }

  render() {
    const { sensitiveWords } = this.props.forum;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 2 },
      wrapperCol: { span: 12 },
    };
    const tailFormItemLayout = {
      wrapperCol: { span: 12, offset: 2 },
    };

    // 说明
    const explanation = (
      <div>
        <h2>说明：</h2>
        <p>1. 包含敏感词的帖子、评论、红包将无法发送</p>
        <p>2. 敏感词请勿输入标点符号及特殊字符</p>
        <p>3. 输入时，两个敏感词中间用中文全角分号隔开。例如需要屏蔽“张三”“李四”“王五”，则输入框内输入“张三；李四；王五”</p>
      </div>
    );

    return (
      <PageHeaderLayout>
        <Form
          onSubmit={this.handleSubmit}
        >
          <FormItem
            {...formItemLayout}
            label="敏感词"
          >
            {getFieldDecorator('sensitive_words', {
              initialValue: sensitiveWords && sensitiveWords.sensitive_words,
            })(
              <TextArea placeholder="请添加敏感词" rows={4} />
            )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
            >
              确定
            </Button>
          </FormItem>
        </Form>
        <Divider />
        {explanation}
      </PageHeaderLayout>
    );
  }
}
