import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form, Button, Input, Modal, Upload, Icon, message } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import styles from './Basic.less';

const FormItem = Form.Item;
const imageUrlPre = 'https://cdn.myfans.cc/';

@connect(state => ({
  forum: state.forum,
}))
@Form.create()
export default class BasicForum extends PureComponent {
  state = {
    nameModelVisible: false,
    updateNameLoading: false,
  }

  componentDidMount() {
    this.getForumInfo();
  }

  // 获取粉丝圈信息
  getForumInfo = () => {
    this.props.dispatch({
      type: 'forum/getForumInfo',
    });
  }

  // 更新粉丝圈信息
  updateForumInfo = (params) => {
    return this.props.dispatch({
      type: 'forum/updateForumInfo',
      payload: params,
    }).then((response) => {
      if (response.code === 1) {
        this.getForumInfo();
      }
      return response;
    });
  }

  changeNameModalVisible = (state) => {
    this.setState({ nameModelVisible: state });
  }

  // 确定更新name
  updateNameSure = () => {
    const name = this.props.form.getFieldValue('name');
    const forumId = this.props.forum.forumInfo.forum_id;
    this.updateForumInfo({
      data: {
        name,
      },
      forumId,
    }).then((response) => {
      if (response.code === 1) {
        this.changeNameModalVisible(false);
      }
    });
  }

  // 更新粉丝圈logo
  updateLogo = (info) => {
    const { status, response, uid, name } = info.file;
    const forumId = this.props.forum.forumInfo.forum_id;
    if (status === 'done') {
      if (response.code === 1) {
        const fileList = [{
          uid,
          name,
          status: 'done',
          url: `${imageUrlPre + response.data.filename}`,
        }];
        this.updateForumInfo({
          data: {
            logo: response.data.filename,
          },
          forumId,
        });
        return fileList;
      } else {
        message.error(response.msg);
      }
    } else if (status === 'error') {
      message.error(`${info.file.name}上传失败`);
    }

    return info.fileList;
  }

  // 更新粉丝圈二维码
  updateQr = (info) => {
    const { status, response, uid, name } = info.file;
    const forumId = this.props.forum.forumInfo.forum_id;
    if (status === 'done') {
      if (response.code === 1) {
        const fileList = [{
          uid,
          name,
          status: 'done',
          url: `${imageUrlPre + response.data.filename}`,
        }];
        this.updateForumInfo({
          forumId,
          data: {
            forum_kf_qrcode: response.data.filename,
          },
        });
        return fileList;
      } else {
        message.error(response.msg);
      }
    } else if (status === 'removed') {
      this.updateForumInfo({
        forumId,
        data: {
          forum_kf_qrcode: '',
        },
      });
      return [];
    } else if (status === 'error') {
      message.error(`${info.file.name}上传失败`);
    }

    return info.fileList;
  }

  render() {
    const { forumInfo } = this.props.forum;
    const { nameModelVisible, updateNameLoading } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 12 },
    };
    const nameModal = (
      <Modal
        title="修改粉丝圈名称"
        visible={nameModelVisible}
        onOk={this.updateNameSure}
        onCancel={this.changeNameModalVisible.bind(this, false)}
        confirmLoading={updateNameLoading}
      >
        {getFieldDecorator('name', {
          initialValue: forumInfo && forumInfo.name,
        })(
          <Input placeholder="请输入粉丝圈名称" />
        )}
      </Modal>
    );
    const fileList = [{
      uid: -1,
      name: 'default.jpg',
      status: 'done',
      url: forumInfo && forumInfo.logo,
    }];
    const qrFileList = forumInfo && !!forumInfo.forum_kf_qrcode ? [{
      uid: -1,
      name: 'default.jpg',
      status: 'done',
      url: forumInfo.forum_kf_qrcode,
    }] : [];
    return forumInfo && (
      <Form>
        <FormItem
          {...formItemLayout}
          label="粉丝圈名称"
        >
          {forumInfo.name}
          <Button
            className="ml-10"
            size="small"
            onClick={this.changeNameModalVisible.bind(this, true)}
          >修改
          </Button>
          {nameModal}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="粉丝圈Logo"
        >
          {getFieldDecorator('logo', {
            valuePropName: 'fileList',
            getValueFromEvent: this.updateLogo,
            initialValue: fileList,
          })(
            <Upload
              name="image"
              action="/b/upload-image"
              listType="picture-card"
            >
              <Icon type="sync" />
              <div>更换Logo</div>
            </Upload>
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="粉丝圈链接"
          extra="单击输入框复制链接"
        >
          <CopyToClipboard
            text={forumInfo.forum_url}
            onCopy={() => { message.success('复制成功'); }}
          >
            <Input value={forumInfo.forum_url} readOnly />
          </CopyToClipboard>
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="客服二维码"
        >
          {getFieldDecorator('forum_kf_qrcode', {
            valuePropName: 'fileList',
            getValueFromEvent: this.updateQr,
            initialValue: qrFileList,
          })(
            <Upload
              name="image"
              action="/b/upload-image"
              listType="picture-card"
            >
              <Icon type="sync" />
              <div>更换二维码</div>
            </Upload>
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="粉丝圈二维码"
        >
          <img
            className={styles.qrcode}
            src={forumInfo.qr_url}
            alt="qrcode"
          />
        </FormItem>
      </Form>
    );
  }
}
