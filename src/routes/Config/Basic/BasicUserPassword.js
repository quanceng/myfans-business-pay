import React, { PureComponent } from 'react';
import { Form, Input, Button, Modal, message } from 'antd';

const FormItem = Form.Item;

@Form.create()
export default class BasicUserPassword extends PureComponent {
  state = {
    visible: false,
  }

  openModal = () => {
    this.setState({ visible: true });
  }

  hideModal = () => {
    this.setState({ visible: false });
  }

  // 修改密码
  nextModal = () => {
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (values.newPassword !== values.confirmPassword) {
        message.info('两次密码输入不一致');
        return;
      }
      this.props.dispatch({
        type: 'forum/changePassword',
        payload: {
          password: this.props.form.getFieldValue('newPassword'),
        },
      }).then((response) => {
        if (response.code === 1) {
          this.props.form.resetFields();
          this.setState({ visible: false });
        }
      });
    });
  }

  render() {
    const { visible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const modalFormItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 15 },
    };
    // 修改密码modal
    const modal = (
      <Modal
        title="修改密码"
        visible={visible}
        onOk={this.nextModal}
        onCancel={this.hideModal}
        okText="确认"
        cancelText="取消"
      >
        <Form>
          <FormItem
            {...modalFormItemLayout}
            label="新密码"
          >
            {getFieldDecorator('newPassword', {
              rules: [{
                required: true, message: '请输入新密码',
              }],
            })(
              <Input type="password" placeholder="请输入新密码" />
            )}
          </FormItem>
          <FormItem
            {...modalFormItemLayout}
            label="确认密码"
          >
            {getFieldDecorator('confirmPassword', {
              rules: [{
                required: true, message: '请输入确认密码',
              }],
            })(
              <Input type="password" placeholder="请输入确认密码" />
            )}
          </FormItem>
        </Form>
      </Modal>
    );

    return (
      <div>
        <Button
          className="ml-10"
          icon="edit"
          size="small"
          onClick={this.openModal}
        >
          修改密码
        </Button>
        {modal}
      </div>
    );
  }
}
