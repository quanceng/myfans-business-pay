import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form, Row, Col } from 'antd';
import UserPassword from './BasicUserPassword';

const FormItem = Form.Item;

@connect(state => ({
  forum: state.forum,
  common: state.common,
}))
export default class BasicUser extends PureComponent {
  render() {
    const { forum, dispatch, common } = this.props;
    const { initData } = common;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 12 },
    };

    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label="手机"
        >
          <Row type="flex">
            <Col>{initData.user_info.username}</Col>
          </Row>
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="密码"
        >
          <Row type="flex">
            <Col>******</Col>
            <Col><UserPassword forum={forum} dispatch={dispatch} /></Col>
          </Row>
        </FormItem>
      </Form>
    );
  }
}
