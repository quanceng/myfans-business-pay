import { connect } from 'dva';
import React, { PureComponent } from 'react';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import BasicForum from './BasicForum';
import BasicUser from './BasicUser';

const tabList = [
  {
    key: 'forum',
    tab: '粉丝圈信息',
  },
  {
    key: 'user',
    tab: '个人信息',
  },
];
@connect(state => ({
  forum: state.forum,
}))
export default class Basic extends PureComponent {
  state = {
    tab: 'forum',
  }

  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }

  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      forum: BasicForum,
      user: BasicUser,
    };
    return componentMap[this.state.tab];
  }

  render() {
    const CurrentComponent = this.getCurrentComponent();
    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent />
      </PageHeaderLayout>
    );
  }
}
