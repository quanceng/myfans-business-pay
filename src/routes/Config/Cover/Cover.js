import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import CoverView from './CoverView';
import CoverBackground from './CoverBackground';

const tabList = [
  {
    key: 'view',
    tab: '数据显示',
  },
  {
    key: 'background',
    tab: '背景图',
  },
];

@connect(state => ({
  forum: state.forum,
}))
@Form.create()
export default class Cover extends PureComponent {
  state = {
    tab: 'view',
  }

  componentDidMount() {
    this.getCover();
  }

  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      view: CoverView,
      background: CoverBackground,
    };
    return componentMap[this.state.tab];
  }

  // 获取
  getCover = () => {
    this.props.dispatch({
      type: 'forum/getCover',
    });
  }

  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }

  render() {
    const { forum, dispatch, form } = this.props;
    const CurrentComponent = this.getCurrentComponent();

    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          forum={forum}
          form={form}
          dispatch={dispatch}
        />
      </PageHeaderLayout>
    );
  }
}
