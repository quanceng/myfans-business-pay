import React, { PureComponent } from 'react';
import { Form, Switch, Radio } from 'antd';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

export default class CoverView extends PureComponent {
  // 更新设置
  updateCover = (params) => {
    this.props.dispatch({
      type: 'forum/updateCover',
      payload: params,
    });
  }

  // 显示浏览量切换
  handleChangeViewCount = (e) => {
    this.updateCover({ is_show_view_count: e ? 1 : 0 });
  }

  // 显示用户量切换
  handleChangeUserCount = (e) => {
    this.updateCover({ is_show_user_count: e ? 1 : 0 });
  }

  // 显示用户等级切换
  handleChangeUserLevel = (e) => {
    this.updateCover({ is_show_user_level: e ? 1 : 0 });
  }
  // 首页帖子排序方式切换
  handleChangePostSort = (e) => {
    this.updateCover({ post_sort: e.target.value });
  }
  // 帖子置顶方式切换
  handleChangePostTop = (e) => {
    this.updateCover({ top_post_show: e.target.value });
  }

  render() {
    const { cover } = this.props.forum;
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 12 },
    };

    return cover && (
      <Form>
        <FormItem
          {...formItemLayout}
          label="是否显示浏览量"
        >
          <Switch
            onChange={this.handleChangeViewCount}
            defaultChecked={!!Number(cover.is_show_view_count)}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="是否显示用户量"
        >
          <Switch
            onChange={this.handleChangeUserCount}
            defaultChecked={!!Number(cover.is_show_user_count)}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="是否显示用户等级"
        >
          <Switch
            onChange={this.handleChangeUserLevel}
            defaultChecked={!!Number(cover.is_show_user_level)}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="首页帖子排序方式"
        >
          <RadioGroup onChange={this.handleChangePostSort} value={cover.post_sort}>
            <Radio value={2}>按照发帖时间排序</Radio>
            <Radio value={1}>按照评论时间排序</Radio>
          </RadioGroup>
        </FormItem>
        {/* <FormItem
          {...formItemLayout}
          label="帖子置顶方式"
        >
          <RadioGroup onChange={this.handleChangePostTop} value={cover.top_post_show}>
            <Radio value={1}>显示在置顶栏</Radio>
            <Radio value={2}>置顶在首页</Radio>
          </RadioGroup>
        </FormItem> */}
      </Form>
    );
  }
}
