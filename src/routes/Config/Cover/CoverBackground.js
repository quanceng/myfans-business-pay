import React, { PureComponent } from 'react';
import { Upload, message, Row, Col, Icon, Button, Card } from 'antd';
import { connect } from 'dva';
import styles from './Cover.less';

const { Dragger } = Upload;

@connect(state => ({
  common: state.common,
}))
export default class CoverBackground extends PureComponent {
  state = {
    backgroundSelected: null,
  }

  // 更新设置
  updateCover = (params) => {
    this.props.dispatch({
      type: 'forum/updateCover',
      payload: params,
    });
  }

  // 选择背景图
  handleClickBackground = (key) => {
    this.setState({
      backgroundSelected: key,
    });
  }

  // 保存背景图
  handleSaveBackground = () => {
    this.updateCover({
      forum_id: this.props.common.initData.forum_info.forum_id,
      attach: this.state.backgroundSelected || this.props.forum.cover.attach,
    });
  }

  // 上传图片
  handleUploadImage = (info) => {
    const { status, response } = info.file;
    if (status === 'done') {
      if (response.code === 1) {
        this.props.dispatch({
          type: 'forum/addBackground',
          payload: { attach: response.data.filename },
        });
      } else {
        message.error(response.msg);
      }
    } else if (status === 'error') {
      message.error(`${info.file.name}上传失败`);
    }
  }

  render() {
    const { cover, backgroundArray } = this.props.forum;
    const { backgroundSelected } = this.state;
    const imageUrlPre = 'https://cdn.myfans.cc/';

    // 背景图设置
    const backgroundSelect = [];
    backgroundArray.forEach((item) => {
      // 当前选择
      const attach = backgroundSelected || (cover && cover.attach);
      const isActive = attach === item.key;

      // 当前面使用
      const isCurrent = cover && (item.key === cover.attach);

      backgroundSelect.push((
        <Col key={item.key} span={8}>
          <div
            className={`${styles.imageItem} ${isActive && styles.imageItemActive}`}
            style={{ backgroundImage: `url(${imageUrlPre + item.key})` }}
            onClick={this.handleClickBackground.bind(this, item.key)}
          >
            <div className={styles.imageItemTag} />
            {item.custom && (<div className={styles.imageItemCustom}>自定义背景图</div>)}
          </div>
          <div className={styles.imageItemCurrent}>{isCurrent && '当前使用'}</div>
        </Col>
      ));
    });

    return (
      <div>
        <Card
          title="选择背景图"
          extra={
            <Button
              size="small"
              type="primary"
              onClick={this.handleSaveBackground}
            >
              保存
            </Button>
          }
        >
          <Row>
            { backgroundSelect }
          </Row>
        </Card>
        <Card
          className="mt-20"
          title="自定义背景图"
        >
          <Dragger
            name="image"
            action="/b/upload-image"
            onChange={this.handleUploadImage}
          >
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">点击或将文件拖拽到此区域上传</p>
            <p className="ant-upload-hint">请上传单个小于1MB的图片（建议尺寸750*300）</p>
          </Dragger>
        </Card>
      </div>
    );
  }
}
