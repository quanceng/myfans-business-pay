import React, { PureComponent } from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import BehaviorRule from './BehaviorRule';
import BehaviorLevel from './BehaviorLevel';

const tabList = [
  {
    key: 'rule',
    tab: '经验值',
  },
  {
    key: 'level',
    tab: '等级',
  },
];

@connect(state => ({
  forum: state.forum,
}))
export default class Behavior extends PureComponent {
  state = {
    tab: 'rule',
  }

  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      rule: BehaviorRule,
      level: BehaviorLevel,
    };
    return componentMap[this.state.tab];
  }

  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }

  render() {
    const { forum, dispatch } = this.props;
    const CurrentComponent = this.getCurrentComponent();

    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          forum={forum}
          dispatch={dispatch}
        />
      </PageHeaderLayout>
    );
  }
}
