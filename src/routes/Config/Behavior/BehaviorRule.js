import React, { PureComponent } from 'react';
import { Table, Button, InputNumber, Form, Divider } from 'antd';
import lodash from 'lodash';

const FormItem = Form.Item;
@Form.create()

export default class BehaviorRule extends PureComponent {
  state = {
    editor: false,
    loading: false,
    saveLoading: false,
    behaviorRulesEditor: {},
  };

  componentDidMount() {
    this.getBehaviorRules();
  }

  componentWillReceiveProps(nextProps) {
    this.getBehaviorRulesEditor(nextProps.forum.behaviorRules);
  }

  // 生成经验值规则要提交的数据
  getBehaviorRulesEditor = (behaviorRules) => {
    const behaviorRulesEditor = {};
    lodash.forEach(behaviorRules, (item, key) => {
      behaviorRulesEditor[key] = {
        rule_limit: item.rule_limit,
        change_points: item.change_points,
      };
    });
    this.setState({ behaviorRulesEditor });
  }

  // 获取经验值规则
  getBehaviorRules = () => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'forum/getBehaviorRules',
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  // 更新经验值规则
  updateBehaviorRules = () => {
    const { behaviorRulesEditor } = this.state;
    this.setState({ saveLoading: true });
    this.props.dispatch({
      type: 'forum/updateBehaviorRules',
      payload: { rules: JSON.stringify(behaviorRulesEditor) },
    }).then((response) => {
      this.setState({ saveLoading: false });
      if (response.code === 1) {
        this.setState({ editor: false });
        this.getBehaviorRules();
      }
    });
  }

  // 切换编辑模式
  changeEditorMode = (editor) => {
    this.setState({ editor });
  }

  // 修改数字
  handleChangeNumber = ({ key, column }, value) => {
    const { behaviorRulesEditor } = this.state;
    behaviorRulesEditor[key][column] = value;
    this.setState({ behaviorRulesEditor });
  }

  // render修改输入框
  renderInputNumber = (item, column, max) => {
    return (
      <InputNumber
        disabled={!item.is_disable}
        size="small"
        min={0}
        max={max}
        defaultValue={item[column]}
        onChange={this.handleChangeNumber.bind(this, { key: item.key, column })}
      />
    );
  }
  // 修改上限
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'forum/updateBehaviorLimit',
          payload: values,
        });
      }
    });
  };
  render() {
    const { loading, saveLoading, editor } = this.state;
    const { behaviorRules, behaviorLimit } = this.props.forum;
    const { getFieldDecorator } = this.props.form;

    // 数据处理
    const dataSource = lodash.map(behaviorRules, (item, key) => {
      return { ...item, key };
    });
    // 列渲染
    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
      },
      {
        title: '站点经验值',
        dataIndex: 'change_points',
        key: 'change_points',
        render: (value, item) => {
          const maxNum = item.type === 1 ? 500 : 100;
          if (editor) {
            return (
              <div>
                { this.renderInputNumber(item, 'change_points', maxNum) } 经验值
              </div>
            );
          } else {
            return `${value} 经验值`;
          }
        },
      },
    ];

    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem
            label="每天获取经验值上限"
          >
            {getFieldDecorator('daily_limit', {
              initialValue: behaviorLimit,
              rules: [{
                required: true,
                message: '请设置上限',
              }],
            })(
              <InputNumber
                min={0}
                max={500}
                precision={0}
              />)}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              更新
            </Button>
          </FormItem>
        </Form>
        <Table
          loading={loading}
          pagination={false}
          dataSource={dataSource}
          columns={columns}
        />
        {editor ?
          (
            <div className="mt-20">
              <Button type="primary" onClick={this.updateBehaviorRules} loading={saveLoading}>保存</Button>
              <Button className="ml-10" onClick={this.changeEditorMode.bind(this, false)}>取消</Button>
            </div>
          ) :
          (
            <div className="mt-20">
              <Button type="primary" onClick={this.changeEditorMode.bind(this, true)}>编辑</Button>
            </div>
          )
        }
        <Divider />
        <h2>说明：</h2>
        <p>1. 同一行为获取经验值每十分钟可获取一次</p>
        <p>2. 成员每日获取经验值上限最多为500</p>
      </div>
    );
  }
}
