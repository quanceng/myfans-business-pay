import React, { PureComponent } from 'react';
import { Table, Button, Input, InputNumber, message } from 'antd';
import lodash from 'lodash';
import { List } from 'immutable';

export default class BehaviorLevel extends PureComponent {
  state = {
    loading: false,
  }

  componentDidMount() {
    this.getLevel();
  }

  // 等级列表
  getLevel = () => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'forum/getLevel',
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  // 保存等级列表
  updateLevel = () => {
    const { level } = this.props.forum;
    const levelNew = [];
    let testingTemp = -1;
    let validate = true;

    // 上传前整理
    lodash.forEach(level, (item, index) => {
      // 经验值验证
      if (testingTemp >= item.score) {
        message.info(`等级${index + 1}设置有误`);
        validate = false;
        return false;
      }
      testingTemp = item.score;

      levelNew.push({
        level: item.level,
        score: item.score,
        index: index + 1,
      });
    });

    // 保存
    if (validate) {
      this.props.dispatch({
        type: 'forum/updateLevel',
        payload: {
          level_info: JSON.stringify(levelNew),
        },
      }).then((response) => {
        if (response.code === 1) {
          this.getLevel();
        }
      });
    }
  }

  // 修改等级名称
  changeLevelName = (index, e) => {
    const levelList = List(this.props.forum.level);
    const levelListNew = levelList.set(index, {
      ...levelList.get(index),
      level: e.target.value,
    });

    this.props.dispatch({
      type: 'forum/saveLevel',
      payload: levelListNew.toArray(),
    });
  }

  // 修改经验值
  changeLevelScore = (index, value) => {
    const levelList = List(this.props.forum.level);
    const levelListNew = levelList.set(index, {
      ...levelList.get(index),
      score: value,
    });

    this.props.dispatch({
      type: 'forum/saveLevel',
      payload: levelListNew.toArray(),
    });
  }

  // 删除一行
  deleteLevelItem = (index) => {
    const levelList = List(this.props.forum.level);
    if (levelList.size <= 5) {
      message.info('等级设置个数最少为5！');
    } else {
      this.props.dispatch({
        type: 'forum/saveLevel',
        payload: levelList.delete(index).toArray(),
      });
    }
  }

  // 添加一行
  addLevelItem = () => {
    const levelList = List(this.props.forum.level);
    if (levelList.size >= 20) {
      message.info('等级设置个数最多为20！');
    } else {
      const index = levelList.size + 1;
      this.props.dispatch({
        type: 'forum/saveLevel',
        payload: levelList.push({
          level: `lv${index}`,
          score: Number(levelList.last().score) + 1,
          index,
        }).toArray(),
      });
    }
  }

  render() {
    const { level } = this.props.forum;
    const { loading } = this.state;

    // 数据
    const dataSource = [];
    lodash.forEach(level, (item) => {
      dataSource.push({ ...item, key: item.index });
    });

    // 列渲染
    const columns = [
      {
        title: '等级',
        key: 'index',
        render: (value, item, index) => {
          return index + 1;
        },
      },
      {
        title: '等级名称',
        dataIndex: 'level',
        key: 'level',
        render: (value, item, index) => {
          return (
            <div>
              <Input
                size="small"
                value={value}
                style={{ width: '88px' }}
                onChange={this.changeLevelName.bind(this, index)}
              />
            </div>
          );
        },
      },
      {
        title: '所需经验值',
        dataIndex: 'score',
        key: 'score',
        render: (value, item, index) => {
          return (
            <InputNumber
              disabled={item.index === 1}
              size="small"
              min={0}
              value={value}
              onChange={this.changeLevelScore.bind(this, index)}
            />
          );
        },
      },
      {
        title: '操作',
        key: 'operate',
        render: (value, item, index) => {
          return index !== 0 ? (
            <Button size="small" onClick={this.deleteLevelItem.bind(this, index)}>删除</Button>
          ) : null;
        },
      },
    ];

    return (
      <div>
        <div className="mb-20">
          <Button type="primary" onClick={this.updateLevel}>保存</Button>
          <Button className="ml-10" onClick={this.addLevelItem}>添加新等级</Button>
        </div>
        <Table
          loading={loading}
          pagination={false}
          dataSource={dataSource}
          columns={columns}
        />
      </div>
    );
  }
}
