import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form, Input, Button, Icon, Tooltip, Popover, Spin,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import UploadCover from '../../components/UploadCover';
import styles from './Wechat.less';

const FormItem = Form.Item;
const imageUrlPre = 'https://cdn.myfans.cc/';

@connect(state => ({
  forum: state.forum,
}))
@Form.create()
export default class Post extends PureComponent {
  state = {
    editorMode: false,
    loading: false,
    imageLogoShow: null,
    imageQrShow: null,
  }

  componentDidMount() {
    this.getWechat();
  }

  // 获取公众号推广数据
  getWechat = () => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'forum/getWechat',
    }).then(() => {
      const { wechat } = this.props.forum;
      this.setState({
        loading: false,
        imageLogoShow: wechat && wechat.logo,
        imageQrShow: wechat && wechat.qrcode,
      });
    });
  }

  // 删除logo
  removeLogo = () => {
    this.setState({
      imageLogoShow: '',
    });
  };
  // 更换logo
  changeLogo = (value) => {
    this.setState({
      imageLogoShow: value,
    });
  };

  // 删除二维码
  removeQr = () => {
    this.setState({
      imageQrShow: '',
    });
  };
  // 更换二维码
  changeQr = (value) => {
    this.setState({
      imageQrShow: value,
    });
  };

  // 切换编辑模式
  changeEditorMode = (editorMode) => {
    this.setState({ editorMode });
  }

  // 提交
  handleSubmit = (e) => {
    e.preventDefault();
    const { imageLogoShow, imageQrShow } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const data = {
          ...values,
        };
        data.logo = imageLogoShow || '';
        data.qrcode = imageQrShow || '';
        this.props.dispatch({
          type: 'forum/updateWechat',
          payload: data,
        }).then((response) => {
          if (response.code === 1) {
            this.setState({ editorMode: false });
            this.getWechat();
          }
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { wechat } = this.props.forum;
    const { editorMode, loading, imageLogoShow, imageQrShow } = this.state;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 12 },
    };
    const ForumSubmitLayout = {
      wrapperCol: { span: 9, offset: 3 },
    };

    // 提示
    const imageTip = <img alt="tip" src={`${imageUrlPre}3AgeFXMZh3W1513324395.jpg`} />;
    const tip = (
      <Popover key="tip" placement="right" content={imageTip} trigger="hover">
        <Icon className="ml-10" type="question-circle-o" />
      </Popover>
    );
    const codeTip = (
      <Tooltip placement="right" title="该微信号为公众号的微信号，请在公众号后台基础设置当中查看哦~">
        <Icon className="ml-10" type="question-circle-o" />
      </Tooltip>
    );

    return (
      <PageHeaderLayout>
        <Spin spinning={loading}>
          <Form
            onSubmit={this.handleSubmit}
            hideRequiredMark
          >
            <FormItem
              {...formItemLayout}
              label="公众号名称"
            >
              {
                editorMode ?
                  getFieldDecorator('gzh_name', {
                    initialValue: wechat && wechat.gzh_name,
                  })(
                    <Input placeholder="请输入公众号名称" />
                  ) : (wechat && wechat.gzh_name) || '尚未设置'
              }
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="公众微信号"
            >
              {
                editorMode ?
                  getFieldDecorator('gzh_no', {
                    initialValue: wechat && wechat.gzh_no,
                  })(
                    <Input placeholder="请输入公众微信号" />
                  ) : <div>{(wechat && wechat.gzh_no) || '尚未设置'} {codeTip} </div>
              }
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="公众号logo"
            >
              {
                editorMode ?
                  <UploadCover filename={imageLogoShow} removeImg={this.removeLogo} changeImg={this.changeLogo} card={1} /> : (
                    wechat && wechat.logo &&
                    <img
                      className={styles.mpLogo}
                      alt="公众号logo"
                      src={`${imageUrlPre + wechat.logo}?imageView2/1/w/100/h/100`}
                    />
                  ) || '尚未设置'
              }
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="公众号简介"
            >
              {
                editorMode ?
                  getFieldDecorator('description', {
                    initialValue: wechat && wechat.description,
                  })(
                    <Input placeholder="请输入公众号简介" />
                  ) : (wechat && wechat.description) || '尚未设置'
              }
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="上传二维码"
              extra="图片像素为230*230,上传该图片后社区推广二维码显示该图片
              "
            >
              {
                editorMode ?
                  <UploadCover filename={imageQrShow} removeImg={this.removeQr} changeImg={this.changeQr} card={1} /> : (
                    wechat && wechat.qrcode &&
                    <img
                      className={styles.mpLogo}
                      alt="二维码图片"
                      src={`${imageUrlPre + wechat.qrcode}?imageView2/1/w/100/h/100`}
                    />
                  ) || '尚未设置'
              }
            </FormItem>
            <FormItem {...ForumSubmitLayout}>
              {
                editorMode ?
                [
                  <Button key="sure" type="primary" htmlType="submit">保存</Button>,
                  <Button
                    key="cancel"
                    className="ml-10"
                    onClick={this.changeEditorMode.bind(this, false)}
                  >取消
                  </Button>,
                ] : [
                  <Button
                    key="editor"
                    type="primary"
                    onClick={this.changeEditorMode.bind(this, true)}
                  >编辑
                  </Button>,
                  tip,
                ]
              }
            </FormItem>
          </Form>
        </Spin>
      </PageHeaderLayout>
    );
  }
}
