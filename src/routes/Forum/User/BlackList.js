import React, { PureComponent } from 'react';
import { Table } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import styles from './User.less';

export default class BlackList extends PureComponent {
  state = {
    loading: true,
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'user/blackList',
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 取消拉黑请求
  handleBlack = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancleBlack',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/blackList',
          }).then(() => {
            this.setState({ loading: false });
          });
        }, 1000);
      }
    });
  };
  // 表格翻页
  handleTableChange = (pagination) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/blackList',
      payload: {
        page: pagination.current,
      },
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 用户数据列表加载
  columns = () => {
    return [
      {
        title: '用户名',
        dataIndex: 'author',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img
              alt="avatar"
              className={styles.tableAvatar}
              src={record.avatar}
            />
            &nbsp;
            <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.nickname}</Ellipsis>
          </span>
        ),
      },
      {
        title: '等级/经验值',
        dataIndex: 'level',
        render: (text, record) => (
          <span>
            <span style={{ padding: '3px 5px', border: '1px solid #ddd' }}>
              {text.curr_level}
            </span>
            &nbsp;&nbsp;&nbsp;
            {`${record.points}/${Number(text.next_point)}`}&nbsp;
          </span>
        ),
      },
      {
        title: '创建时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <a
              onClick={this.handleBlack.bind(this, record.user_id)}
            >
              恢复
            </a>
          );
        },
      },
    ];
  };
  render() {
    const { blackList, blackPage } = this.props.user;
    const { loading } = this.state;
    return (
      <div>
        <Table
          rowKey={record => record.rel_id}
          className="mt-20"
          dataSource={blackList}
          pagination={blackPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
      </div>
    );
  }
}
