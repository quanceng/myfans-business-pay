import React, { PureComponent } from 'react';
import { Button, InputNumber, Table, Form, Select, Input, Popconfirm, Divider, Modal, message } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import moment from 'moment';
import findIndex from 'lodash/findIndex';
import styles from './User.less';

const FormItem = Form.Item;
const Option = Select.Option;
export default class BlackList extends PureComponent {
  state = {
    identityList: [],
    identityId: null,
    isCreate: 1,
    visible: false,
    userVisible: false,
    identityName: '',
    editIdtName: '',
    searchName: '',
    loading: false,
    seaUserList: [],
    seaPage: {},
  }
  componentDidMount() {
    this.fetch();
  }
  fetch = () => {
    this.props.dispatch({
      type: 'user/identityList',
    }).then((response) => {
      if (response.data.length) {
        this.setState({
          identityList: response.data,
          identityId: response.data[0].identity_id,
          editIdtName: response.data[0].identity_name,
        });
        this.setState({ loading: true });
        this.props.dispatch({
          type: 'user/identityUserList',
          payload: {
            identity_id: response.data[0].identity_id,
          },
        }).then(() => {
          this.setState({ loading: false });
        });
      }
    });
  }
  // 选择身份
  handleChange = (value) => {
    const { identityList } = this.state;
    const index = findIndex(identityList, title => title.identity_id === value);
    this.setState({
      identityId: value,
      editIdtName: identityList[index].identity_name,
      loading: true,
    });
    this.props.dispatch({
      type: 'user/identityUserList',
      payload: {
        identity_id: value,
      },
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 身份表格翻页
  handleTableChange = (pagination) => {
    const { identityId } = this.state;
    this.props.dispatch({
      type: 'user/identityUserList',
      payload: {
        page: pagination.current,
        identity_id: identityId,
      },
    });
  }
  // 用户表格翻页
  searchTableChange = (pagination) => {
    this.props.dispatch({
      type: 'user/searchUserList',
      payload: {
        page: pagination.current,
        nickname: this.state.searchName,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          seaUserList: response.data.data,
          seaPage: {
            current: response.data.current_page,
            total: response.data.total,
            pageSize: 10,
          },
        });
      }
    });
  }
  // 显示用户弹窗
  showUserModal = () => {
    this.setState({ userVisible: true });
  }
  // 隐藏用户弹窗
  hiddenUserModal = () => {
    this.setState({ userVisible: false, seaUserList: [], seaPage: {} });
  }
  // 保存搜索用户名
  searchChange = (e) => {
    this.setState({
      searchName: e.target.value,
    });
  }
  // 搜索用户
  searchUser = () => {
    const { searchName } = this.state;
    if (!searchName) {
      message.info('请输入搜索的用户名');
      return;
    }
    this.props.dispatch({
      type: 'user/searchUserList',
      payload: {
        nickname: searchName,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          seaUserList: response.data.data,
          seaPage: {
            current: response.data.current_page,
            total: response.data.total,
            pageSize: 10,
          },
        });
      }
    });
  }
  // 添加与移除用户
  updateUserIdt = (userId, isAdd) => {
    const { identityId } = this.state;
    this.setState({ loading: true });
    if (isAdd) {
      this.props.dispatch({
        type: 'user/updateUserIdt',
        payload: {
          identityId,
          data: {
            user_id: userId,
          },
        },
      }).then((response) => {
        if (response.code === 1) {
          this.setState({ userVisible: false, seaUserList: [], seaPage: {}, searchName: '' });
          this.props.dispatch({
            type: 'user/identityUserList',
            payload: {
              identity_id: identityId,
            },
          }).then(() => {
            this.setState({ loading: false });
          });
        }
      });
    } else {
      this.props.dispatch({
        type: 'user/updateUserIdt',
        payload: {
          identityId: 0,
          data: {
            user_id: userId,
          },
        },
      }).then((response) => {
        if (response.code === 1) {
          this.props.dispatch({
            type: 'user/identityUserList',
            payload: {
              identity_id: identityId,
            },
          }).then(() => {
            this.setState({ loading: false });
          });
        }
      });
    }
  }
  // 显示身份弹窗
  showConfirm = (status, identityName) => {
    this.setState({
      visible: true,
      isCreate: status,
      identityName,
    });
  };
  // 隐藏身份弹窗
  handleCancel = () => {
    this.setState({ visible: false });
  };
  // 保存身份名称
  changeName = (e) => {
    this.setState({ identityName: e.target.value });
  }
  // 新建与编辑身份请求
  handleUpdate = () => {
    const { identityId, isCreate, identityName } = this.state;
    if (!identityName) {
      message.info('请输入身份名称');
      return;
    }
    if (isCreate) {
      this.props.dispatch({
        type: 'user/createIdt',
        payload: {
          identity_name: identityName,
        },
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.setState({ visible: false, identityName: '' });
            this.fetch();
          }, 1000);
        }
      });
    } else {
      this.props.dispatch({
        type: 'user/editIdt',
        payload: {
          identity_name: identityName,
          identity_id: identityId,
        },
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.setState({ visible: false, identityName: '' });
            this.fetch();
          }, 1000);
        }
      });
    }
  };
  // 删除身份
  deleteIdentity = () => {
    this.props.dispatch({
      type: 'user/deleteIdt',
      payload: this.state.identityId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 用户数据列表表头
  renderHead = () => {
    return (
      <div style={{ overflow: 'hidden' }}>
        <span style={{ float: 'right' }}>
          <Popconfirm
            title="确定要删除这个自定义身份吗？"
            onConfirm={this.deleteIdentity}
          >
            <a href="javascript:;">删除</a>
          </Popconfirm>
        </span>
        <a
          style={{ float: 'right' }}
          href="javascript:;"
          onClick={this.showConfirm.bind(this, 0, this.state.editIdtName)}
        >
          编辑身份
          <Divider type="vertical" />
        </a>
        <a
          style={{ float: 'right' }}
          href="javascript:;"
          onClick={this.showUserModal}
        >
          新增用户
          <Divider type="vertical" />
        </a>
      </div>
    );
  };
  // 用户数据列表加载
  columns = () => {
    return [
      {
        title: '用户名',
        dataIndex: 'author',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img
              alt="avatar"
              className={styles.tableAvatar}
              src={record.avatar}
            />
            &nbsp;
            <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.nickname}</Ellipsis>
          </span>
        ),
      },
      {
        title: '等级/经验值',
        dataIndex: 'level',
        render: (text, record) => (
          <span>
            {
              this.state.verifyScore && record.user_id === this.state.selectId ?
                <span>
                  <InputNumber
                    value={this.state.score}
                    onChange={val => this.setState({ score: val })}
                    min={0}
                    style={{ width: 100 }}
                    placeholder="经验值数"
                  />
                  <Button
                    onClick={this.updateScore.bind(this, {
                      user_id: record.user_id, score: this.state.score })}
                  >
                    确定
                  </Button>
                </span> :
                <span>
                  <span style={{ padding: '3px 5px', border: '1px solid #ddd' }}>
                    {text.curr_level}
                  </span>
                  &nbsp;&nbsp;&nbsp;
                  {`${record.points}/${Number(text.next_point)}`}&nbsp;
                </span>
            }
          </span>
        ),
      },
      {
        title: '积分',
        dataIndex: 'integral',
      },
      {
        title: '创建时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <a
              onClick={this.updateUserIdt.bind(this, record.user_id, 0)}
            >
              移除
            </a>
          );
        },
      },
    ];
  };
  // 加载搜索后的用户列表
  searchColumns = () => {
    return [
      {
        title: '用户名',
        dataIndex: 'author',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img
              alt="avatar"
              className={styles.tableAvatar}
              src={record.avatar}
            />
            &nbsp;
            {record.nickname}
            &nbsp;
          </span>
        ),
      },
      {
        title: '操作',
        width: 100,
        render: (text, record) => (
          <a
            onClick={this.updateUserIdt.bind(this, record.user_id, 1)}
            href="javascript:;"
          >
            添加
          </a>
        ),
      },
    ];
  }
  render() {
    const { idtUserList, idtPage } = this.props.user;
    const { identityList, identityId, isCreate, visible, identityName, userVisible, searchName, loading, seaUserList, seaPage } = this.state;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            { identityId ?
              <Select
                style={{ width: 120 }}
                value={identityId}
                onChange={this.handleChange}
              >
                { identityList ? identityList.map((val) => {
                  return (
                    <Option key={val.identity_id} value={val.identity_id} >
                      {val.identity_name}
                    </Option>
                  );
                }) : ''}
              </Select> :
              <Select
                style={{ width: 120 }}
                placeholder="用户身份"
              />
            }
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              onClick={this.showConfirm.bind(this, 1, '')}
            >
              新建身份
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.rel_id}
          dataSource={idtUserList}
          pagination={idtPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          title={() => this.renderHead()}
          loading={loading}
        />
        <Modal
          title={isCreate ? '新建身份' : '编辑身份'}
          visible={visible}
          onOk={this.handleUpdate}
          onCancel={this.handleCancel}
        >
          <Form>
            <FormItem
              label="身份名称"
              labelCol={{ span: 4 }}
              wrapperCol={{ span: 8, offset: 1 }}
            >
              <Input
                placeholder="请输入身份名称"
                value={identityName}
                onChange={this.changeName}
              />
            </FormItem>
          </Form>
        </Modal>
        <Modal
          title="新增用户"
          visible={userVisible}
          onCancel={this.hiddenUserModal}
          footer={<div />}
        >
          <div style={{ textAlign: 'center' }}>
            <input
              style={{ width: 250, height: 30 }}
              placeholder="搜索用户名称"
              value={searchName}
              onChange={this.searchChange}
              onFocus={() => this.setState({ searchName: '' })}
            />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <Button type="primary" onClick={this.searchUser}>搜索</Button>
          </div>
          <div style={{ textAlign: 'center', marginTop: 15, borderRadius: 3 }}>
            {
              seaUserList.length > 0 ?
                <Table
                  style={{ width: 350, margin: '0 auto' }}
                  rowKey={record => record.user_id}
                  columns={this.searchColumns()}
                  pagination={seaPage}
                  dataSource={seaUserList}
                  onChange={this.searchTableChange}
                /> : ''
            }
          </div>
        </Modal>
      </div>
    );
  }
}
