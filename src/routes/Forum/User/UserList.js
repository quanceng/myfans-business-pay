import React, { PureComponent } from 'react';
import { Tag, Button, Form, Input, Select, Table, Modal, Menu, Dropdown, Icon } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import styles from './User.less';
import GapUserModal from '../../../components/GagUserModal';
import DatePickers from '../../../components/DatePicker';

const Option = Select.Option;
const FormItem = Form.Item;
export default class UserList extends PureComponent {
  state = {
    startDate: null,
    endDate: null,
    visible: false,
    loading: true,
    gagUserId: null,
    userData: {},
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'forum/getLevel',
    });
    this.props.dispatch({
      type: 'user/userList',
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 保存筛选时间
  changeDate = (startDate, endDate) => {
    this.setState({
      startDate,
      endDate,
    });
  };
  // 筛选用户列表
  handleSubmit = (e) => {
    e.preventDefault();
    const { startDate, endDate } = this.state;
    this.props.form.validateFields((errors, values) => {
      if (errors) {
        return;
      }
      const userData = {
        ...values,
      };
      delete userData.change_type;
      if (startDate && endDate) {
        userData.start_time = startDate;
        userData.end_time = endDate;
      }
      this.setState({ loading: true });
      this.props.dispatch({
        type: 'user/userList',
        payload: userData,
      }).then(() => {
        this.setState({ loading: false, userData });
      });
    });
  }
  // 拉黑请求
  handleBlack = (userId) => {
    const self = this;
    Modal.confirm({
      title: '确认提醒',
      content: '您是否确定要将该用户加入黑名单',
      onOk() {
        self.setState({ loading: true });
        self.props.dispatch({
          type: 'user/fetchBlack',
          payload: {
            user_id: userId,
          },
        }).then((response) => {
          if (response.code === 1) {
            setTimeout(() => {
              self.props.dispatch({
                type: 'user/userList',
                payload: self.state.userData,
              }).then(() => {
                self.setState({ loading: false });
              });
            }, 1000);
          }
        });
      },
      onCancel() {},
    });
  };
  // 取消拉黑请求
  cancleBlack = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancleBlack',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/userList',
            payload: this.state.userData,
          }).then(() => {
            this.setState({ loading: false });
          });
        }, 1000);
      }
    });
  };
  // 管理员请求
  handleAdmin = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/fetchAdmin',
      payload: {
        user_id: userId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/userList',
            payload: this.state.userData,
          }).then(() => {
            this.setState({ loading: false });
          });
        }, 1000);
      }
    });
  };
  // 取消管理员请求
  cancleAdmin = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancleAdmin',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/userList',
            payload: this.state.userData,
          }).then(() => {
            this.setState({ loading: false });
          });
        }, 1000);
      }
    });
  };
  // 禁言弹框提醒
  showGagModal = (userId, event) => {
    event.preventDefault();
    this.setState({
      visible: true,
      gagUserId: userId,
    });
  }
  // 收起禁言弹框
  handleCancel = () => {
    this.setState({
      visible: false,
      gagUserId: null,
    });
  }
  // 禁言请求发送
  handleGapModal = (reason, gagTime) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/fetchGag',
      payload: {
        user_id: this.state.gagUserId,
        reason,
        gag_time: gagTime,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/userList',
            payload: this.state.userData,
          }).then(() => {
            this.setState({
              loading: false,
              visible: false,
              gagUserId: null,
            });
          });
        }, 1000);
      }
    });
  };
  // 取消禁言请求
  cancelGag = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancelGag',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'user/userList',
            payload: this.state.userData,
          }).then(() => {
            this.setState({ loading: false });
          });
        }, 1000);
      }
    });
  };
  // 表格翻页
  handleTableChange = (pagination) => {
    const { userData } = this.state;
    userData.page = pagination.current;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/userList',
      payload: userData,
    }).then(() => {
      this.setState({ loading: false, userData });
    });
  }
  // 用户数据列表加载
  columns = () => {
    return [
      {
        title: '用户',
        dataIndex: 'author',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img
              alt="avatar"
              className={styles.tableAvatar}
              src={record.avatar}
            />
            &nbsp;
            <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.nickname}</Ellipsis>
            &nbsp;
            {
              Number(record.is_admin) ? <Tag color="orange">管理员</Tag> : ''
            }
          </span>
        ),
      },
      {
        title: '用户ID',
        dataIndex: 'user_id',
      },
      {
        title: '等级/经验值',
        dataIndex: 'level',
        render: (text, record) => (
          <span>
            <span style={{ padding: '3px 5px', border: '1px solid #ddd' }}>
              {text.curr_level}
            </span>
            &nbsp;&nbsp;&nbsp;
            {`${record.points}/${Number(text.next_point)}`}&nbsp;
          </span>
        ),
      },
      {
        title: '创建时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          const menu = (
            <Menu>
              <Menu.Item key="admin">
                {
                  Number(record.is_admin)
                  ? <a onClick={this.cancleAdmin.bind(this, record.user_id)}>取消管理员</a>
                  : <a onClick={this.handleAdmin.bind(this, record.user_id)}>成为管理员</a>
                }
              </Menu.Item>
              <Menu.Item key="say">
                {
                  Number(record.over_punish_dateline)
                  ? <a onClick={this.cancelGag.bind(this, record.user_id)}>取消禁言</a>
                  : <a onClick={this.showGagModal.bind(this, record.user_id)}>禁言</a>
                }
              </Menu.Item>
              <Menu.Item key="black">
                <a onClick={this.handleBlack.bind(this, record.user_id)}>加入黑名单</a>
              </Menu.Item>
            </Menu>
          );
          if (Number(record.is_black)) {
            return (
              <a
                onClick={this.cancleBlack.bind(this, record.user_id)}
              >
                移出黑名单
              </a>);
          }
          return (
            <Dropdown
              overlay={menu}
              trigger={['click']}
            >
              <a className="ant-dropdown-link" href="#">
                更多<Icon type="down" />
              </a>
            </Dropdown>
          );
        },
      },
    ];
  };
  render() {
    const { form, forum, user } = this.props;
    const { getFieldDecorator } = form;
    const { level } = forum;
    console.log(level);
    const { userList, userPage } = user;
    const { visible, loading } = this.state;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('nickname')(<Input
              placeholder="输入关键字"
              style={{ width: 100 }}
            />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('user_id')(<Input
              placeholder="输入用户ID"
              style={{ width: 100 }}
            />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('level')(
              <Select
                style={{ width: 100 }}
                placeholder="筛选等级"
                dropdownMatchSelectWidth={false}
              >
                { level ? level.map((val) => {
                  return (
                    <Option key={val.index} value={val.index} >
                      {val.level}
                    </Option>
                  );
                }) : ''}
              </Select>)}
          </FormItem>
          <FormItem>
            <DatePickers
              changeDate={this.changeDate}
            />
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.user_id}
          className="mt-20"
          dataSource={userList}
          pagination={userPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
        <GapUserModal
          visible={visible}
          handleGapModal={this.handleGapModal}
          handleCancel={this.handleCancel}
        />
      </div>
    );
  }
}
