import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import UserList from './UserList';
import AdminList from './AdminList';
import BlackList from './BlackList';
import CustomList from './CustomList';

const tabList = [
  {
    key: 'all',
    tab: '全部成员',
  },
  {
    key: 'admin',
    tab: '管理员',
  },
  {
    key: 'black',
    tab: '黑名单',
  },
  {
    key: 'custom',
    tab: '自定义',
  },
];
@connect(state => ({
  forum: state.forum,
  user: state.user,
}))
@Form.create()
export default class User extends PureComponent {
  state = {
    tab: 'all',
  }
  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      all: UserList,
      admin: AdminList,
      black: BlackList,
      custom: CustomList,
    };
    return componentMap[this.state.tab];
  }
  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }
  render() {
    const { forum, dispatch, form, user } = this.props;
    const CurrentComponent = this.getCurrentComponent();
    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          forum={forum}
          form={form}
          user={user}
          dispatch={dispatch}
        />
      </PageHeaderLayout>
    );
  }
}
