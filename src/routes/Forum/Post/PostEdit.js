import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import PostForm from './PostForm';
@connect(state => ({
  topic: state.topic,
  post: state.post,
}))
export default class PostEdit extends PureComponent {
  componentDidMount() {
    this.props.dispatch({
      type: 'post/fetchPostDetail',
      payload: this.props.match.params.id,
    });
    this.props.dispatch({
      type: 'topic/fetchTopic',
    });
  }
  // 修改微帖
  handleSubmit = (value) => {
    const data = {
      ...value,
      attachment_list: JSON.stringify({ file_path: value.image }),
      thread_id: this.props.match.params.id,
    };
    delete data.image;
    this.props.dispatch({
      type: 'post/editPost',
      payload: data,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/post'));
        }, 1000);
      }
    });
  };
  render() {
    const { topic, post } = this.props;
    const { topics } = topic;
    const { postDetail } = post;
    return (
      <PageHeaderLayout>
        {postDetail && <PostForm topics={topics} data={postDetail} handleSubmit={this.handleSubmit} />}
      </PageHeaderLayout>
    );
  }
}
