import React, { PureComponent } from 'react';
import { Table, Tag, Button, Form, Input, Divider, Modal } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import DatePickers from '../../../components/DatePicker';
import GapUserModal from '../../../components/GagUserModal';
import styles from './Post.less';

const FormItem = Form.Item;
@Form.create()
export default class Post extends PureComponent {
  state = {
    selectedRowKeys: [],
    startDate: null,
    endDate: null,
    postData: {},
    loading: false,
    visible: false,
    auditVisible: false,
    refuseData: null,
  };
  componentDidMount() {
    this.fetch();
  }
  fetch = () => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/auditList',
    }).then(() => {
      this.setState({ loading: false });
    });
  };
  // 保存勾选帖子的帖子ID
  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  }
  // 通过审核
  handlePass = (threadId) => {
    this.props.dispatch({
      type: 'post/passPost',
      payload: {
        review_id: threadId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.setState({
            visible: false,
          });
          this.fetch();
        }, 1000);
      }
    });
  };
  // 批量通过
  passMore = () => {
    this.props.dispatch({
      type: 'post/passPost',
      payload: {
        review_id: this.state.selectedRowKeys,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 查看帖子详情
  handleShow = (id) => {
    this.props.dispatch({
      type: 'post/auditDetail',
      payload: id,
    }).then(() => {
      this.setState({
        visible: true,
      });
    });
  };
  // 隐藏modal
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  }
  // A标签兼容操作
  openLink = (e) => {
    if (e.target.nodeName === 'A' && e.target.dataset.href) {
      window.open(e.target.dataset.href);
    }
  };
  // 加载帖子内容
  renderThreadContent = () => {
    const detail = this.props.post.auditDetail;
    const imgUrl = 'http://cdn.myfans.cc/';
    const qqMusicUrl = 'http://ws.stream.qqmusic.qq.com/';
    switch (Number(detail.type_id)) {
      // 图文
      case 1:
        return (
          <div>
            {detail.attachment_list.map(item => (
              <a
                href={imgUrl + item.file_path}
                target="_blank"
                key={item.file_path}
              >
                <img
                  style={{
                    marginRight: 10,
                    marginTop: 10,
                    verticalAlign: 'middle',
                    width: 120,
                  }}
                  src={imgUrl + item.file_path}
                  alt=""
                />
              </a>
            ))}
          </div>
        );
      // 图文
      case 2:
        return (
          <div>
            {detail.attachment_list.map(item => (
              <a
                href={imgUrl + item.file_path}
                target="_blank"
                key={item.file_path}
              >
                <img
                  style={{
                    marginRight: 10,
                    marginTop: 10,
                    verticalAlign: 'middle',
                    width: 120,
                  }}
                  src={imgUrl + item.file_path}
                  alt=""
                />
              </a>
            ))}
          </div>
        );
      // 语音
      case 3:
        return (
          <div>
            <audio
              controls
              preload="none"
              src={imgUrl + detail.attachment_list.file_path}
            />
          </div>
        );
      // 视频
      case 7:
        return (
          <video
            controls
            preload="meta"
            src={imgUrl + detail.attachment_list.file_path}
            width={400}
            height={520}
          />
        );
      // 音乐
      case 8:
        return (
          <div>
            <div>
              分享歌曲：{detail.attachment_list.singer}
              - {detail.attachment_list.song_name}
            </div>
            <audio
              controls
              preload="none"
              src={qqMusicUrl + detail.attachment_list.file_path}
            />
          </div>
        );
      default:
        return (
          <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
        );
    }
  };
  // 收起拒绝审核弹框
  handleGapCancel = () => {
    this.setState({
      auditVisible: false,
      refuseData: null,
    });
  }
  // 拒绝审核请求发送
  handleGapModal = (reason) => {
    this.setState({ loading: true });
    const { refuseData } = this.state;
    this.props.dispatch({
      type: 'post/refusePost',
      payload: {
        threadId: refuseData.postId,
        data: {
          author_id: refuseData.userId,
          content: reason,
        },
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.setState({
            auditVisible: false,
          });
          this.fetch();
        }, 1000);
      }
    });
  };
  // 拒绝审核弹框
  handleRefuse = (userId, postId) => {
    this.setState({
      refuseData: {
        userId,
        postId,
      },
      auditVisible: true,
      visible: false,
    });
  };
  // 帖子列表数据渲染
  columns = () => {
    return [
      {
        title: '用户',
        render: (text, record) => {
          return (
            <span className={styles.userLogo}>
              <img
                className={styles.tableAvatar}
                src={record.user.avatar}
                alt=""
              />
              &nbsp;
              <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.user.nickname}</Ellipsis>
            </span>
          );
        },
      },
      {
        title: '话题',
        dataIndex: 'topic',
        render: (text, record) => {
          if (record.topic_info) {
            return <Tag color="green">{record.topic_info.topic_name}</Tag>;
          } else {
            return <Tag>无</Tag>;
          }
        },
      },
      {
        title: '微帖类型',
        render: (text, record) => {
          switch (Number(record.type_id)) {
            case 0:
              return this.renderTag('green', '文字');
            case 1:
              return this.renderTag('green', '图文');
            case 2:
              return this.renderTag('green', '图文');
            case 3:
              return this.renderTag('blue', '语音');
            case 7:
              return this.renderTag('blue', '视频');
            case 8:
              return this.renderTag('blue', '音乐');
            default:
              return this.renderTag('', '未知');
          }
        },
      },
      {
        title: '发布时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <div>
              <a href="javascript:;" onClick={this.handleShow.bind(this, record.review_id)}>查看详情</a>
              <Divider type="vertical" />
              <a href="javascript:;" onClick={this.handlePass.bind(this, [record.review_id])}>通过</a>
              <Divider type="vertical" />
              <a href="javascript:;" onClick={this.handleRefuse.bind(this, record.author_id, record.review_id)}>拒绝</a>
            </div>
          );
        },
      },
    ];
  };
  // 帖子隐藏信息渲染函数
  renderContent = (record) => {
    const qqMusicUrl = 'http://ws.stream.qqmusic.qq.com/';
    switch (Number(record.type_id)) {
      case 1:
        return (
          <div>
            {
              record.attachment_list.map(item =>
                (
                  <a
                    href={item.file_path}
                    target="_blank"
                    key={item.file_path}
                  >
                    <img
                      style={{ marginRight: 10, marginTop: 10, width: 100 }}
                      src={item.file_path}
                      alt=""
                    />
                  </a>
                )
              )
            }
          </div>
        );
      case 2:
        return (
          <div>
            {
              record.attachment_list.map(item =>
                (
                  <a href={item.file_path} target="_blank" key={item.file_path}>
                    <img
                      style={{ marginRight: 10, marginTop: 10, width: 100 }}
                      src={item.file_path}
                      alt=""
                    />
                  </a>)
              )
            }
          </div>
        );
      case 3:
        return (
          <div>
            <audio
              controls
              preload="none"
              src={record.attachment_list.file_path}
            />
          </div>
        );
      case 7:
        return (
          <div>
            <video controls preload="none" style={{ width: 200 }} src={record.attachment_list.file_path} />
          </div>
        );
      case 8:
        return (
          <div>
            <div>分享歌曲：{record.attachment_list.singer} - {record.attachment_list.song_name}</div>
            <audio controls preload="none" src={qqMusicUrl + record.attachment_list.file_path} />
          </div>
        );
      default:
        return <div dangerouslySetInnerHTML={{ __html: record.message }} />;
    }
  }
  // 帖子类型彩色样式渲染
  renderTag = (color, text) => {
    return <Tag color={color}>{text}</Tag>;
  }

  // 表格翻页
  handleTableChange = (pagination) => {
    this.setState({ loading: true });
    const postData = this.state.postData;
    postData.page = pagination.current;
    this.props.dispatch({
      type: 'post/auditList',
      payload: postData,
    }).then(() => {
      this.setState({ loading: false, postData });
    });
  }
  // 保存筛选时间
  changeDate = (startDate, endDate) => {
    this.setState({
      startDate,
      endDate,
    });
  };
  // 筛选帖子列表
  handleSubmit = (e) => {
    e.preventDefault();
    const { startDate, endDate } = this.state;
    const query = this.props.form.getFieldValue('q');
    const params = {};
    if (startDate && endDate) {
      params.start_at = startDate;
      params.end_at = endDate;
    }
    if (query) {
      params.keyword = query;
    }
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/auditList',
      payload: params,
    }).then(() => {
      this.setState({ loading: false, postData: params });
    });
  }
  render() {
    const { post } = this.props;
    const { auditList, auditPage, auditDetail } = post;
    const { selectedRowKeys, loading, visible, auditVisible } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('q')(<Input
              placeholder="输入关键字"
              style={{ width: 100 }}
            />)}
          </FormItem>
          <FormItem>
            <DatePickers
              width={1}
              changeDate={this.changeDate}
            />
          </FormItem>
          <FormItem>
            <Button type="primary" disabled={!selectedRowKeys.length} onClick={this.passMore}>
              批量通过
            </Button>
          </FormItem>
          <FormItem
            style={{ marginRight: 0 }}
          >
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.review_id}
          dataSource={auditList}
          className="mt-20"
          pagination={auditPage}
          loading={loading}
          columns={this.columns()}
          expandedRowRender={record => this.renderContent(record)}
          rowSelection={rowSelection}
          onChange={this.handleTableChange}
        />
        {
          auditDetail ?
            <Modal
              title={
                <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: auditDetail.message }} />
              }
              visible={visible}
              footer={
                <div>
                  <Button onClick={this.handleRefuse.bind(this, auditDetail.author_id, auditDetail.review_id)}>
                    拒绝
                  </Button>
                  <Button type="primary" onClick={this.handlePass.bind(this, [auditDetail.review_id])}>
                    通过
                  </Button>
                </div>
              }
              onCancel={this.handleCancel}
            >
              <article>
                {this.renderThreadContent()}
              </article>
            </Modal> : ''
        }
        <GapUserModal
          visible={auditVisible}
          handleGapModal={this.handleGapModal}
          handleCancel={this.handleGapCancel}
          isAudit={1}
        />
      </div>
    );
  }
}
