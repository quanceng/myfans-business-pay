import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button, Input, Form, Select, message } from 'antd';
import UploadCover from '../../../components/UploadCover';
import Editor from '../../../components/Editor/Editor';
import Preview from '../../../components/Preview';

const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
@connect(state => ({
  common: state.common,
}))
export default class PostForm extends PureComponent {
  state = {
    subject: this.props.data && this.props.data.subject,
    preview: false,
  }
  // 提交
  handleSubmit = (e) => {
    e.preventDefault();
    const { subject } = this.state;
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!subject || subject === '<p><br></p>') {
        message.info('请勿提交空内容');
        return;
      }
      const data = {
        ...values,
        subject,
        type: 11,
      };
      this.props.handleSubmit(data);
    });
  }

  // 保存富文本编辑器内容
  changeEditContent = (value) => {
    this.setState({ subject: value });
  };

  // 上传图片
  handleUploadImage = (image) => {
    this.props.form.setFieldsValue({ image });
  }

  // 删除上传的图片
  handleRemoveImage = () => {
    this.props.form.setFieldsValue({ image: '' });
  }

  // 开启预览
  openPreview = () => {
    this.setState({ preview: true });
  };
  // 关闭预览
  closePreview = () => {
    this.setState({ preview: false });
  };
  render() {
    const { data, topics, common } = this.props;
    const { preview, subject } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <FormItem
          label="微帖标题："
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 6 }}
        >
          {getFieldDecorator(
            'message',
            {
              rules: [{
                required: true,
                message: '请填写文章标题',
              }],
              initialValue: data && data.message,
            },
          )(<Input
            placeholder="填写文章标题"
          />)}
        </FormItem>
        <FormItem
          label="微帖封面："
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 20 }}
        >
          {getFieldDecorator('image', {
            initialValue: data && data.attachment_list.file_path,
          })(
            <Input hidden />
          )}
          <UploadCover
            changeImg={this.handleUploadImage}
            removeImg={this.handleRemoveImage}
            filename={data && data.attachment_list.file_path}
            msg="推荐宽高比4：3"
          />
        </FormItem>
        <FormItem
          label="话题："
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 10 }}
          required
        >
          {getFieldDecorator(
            'topic_id',
            {
              initialValue: 0,
            },
          )(
            <Select
              style={{ width: 100 }}
            >
              <Option key="topicAll" value={0}>
                全部话题
              </Option>
              { topics ? topics.map((item) => {
                return (
                  <Option
                    key={`topic_id_${item.topic_id}`}
                    value={item.topic_id}
                  >
                    {item.topic_name}
                  </Option>
                );
                }) : ''
              }
            </Select>)}
        </FormItem>
        <FormItem
          label="微帖内容："
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 20 }}
          required
        >
          <Editor onChange={this.changeEditContent} initContent={subject} />
        </FormItem>
        <FormItem
          wrapperCol={{ span: 20, offset: 3 }}
        >
          <Button type="primary" onClick={this.handleSubmit}>保存添加</Button>
          &nbsp;&nbsp;&nbsp;
          <Button onClick={this.openPreview}>预览</Button>
          {preview ? <Preview initData={common.initData} content={subject} title={this.props.form.getFieldValue('message')} closePreview={this.closePreview} /> : ''}
        </FormItem>
      </Form>
    );
  }
}
