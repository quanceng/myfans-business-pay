import React, { PureComponent } from 'react';
import { Form, Select, Switch } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
export default class PostSet extends PureComponent {
  componentDidMount() {
    this.fetch();
  }
  // 获取设置
  fetch = () => {
    this.props.dispatch({
      type: 'post/postSet',
    });
  };
  // 更新设置
  updateConfig = (params) => {
    this.props.dispatch({
      type: 'post/updateConfig',
      payload: params,
    }).then((response) => {
      if (response.code === 1) {
        this.fetch();
      }
    });
  }

  // 等级发帖开启关闭
  handleChangePost = (e) => {
    this.updateConfig({ is_open_post_level: e ? 1 : 0 });
  }
  // 等级评论微帖开启关闭
  handleChangeComment = (e) => {
    this.updateConfig({ is_open_comment_level: e ? 1 : 0 });
  }
  // 允许用户发帖等级设置
  handlePostLevel = (value) => {
    this.updateConfig({ post_level: value });
  }
  // 允许用户评论等级设置
  handleCommentLevel = (value) => {
    this.updateConfig({ comment_level: value });
  }
  // 允许用户发帖时间设置
  handleUserTime = (value) => {
    this.updateConfig({ new_user_post_time: value });
  }
  // 发帖是否审核
  handleChangePostAudit = (e) => {
    this.updateConfig({ is_post_audit: e ? 1 : 0 });
  }

  render() {
    const { postConfig } = this.props.post;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 10 },
    };
    // 等级
    const level = [2, 3, 4, 5, 6, 7, 8, 9, 10];
    const levelOption = level.map((item) => {
      return (
        <Option key={item} value={item} >
          {item}级
        </Option>
      );
    });
    // 小时
    const hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    const hoursOption = hours.map((item) => {
      return (
        <Option key={item} value={item} >
          {item}小时
        </Option>
      );
    });
    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label="发帖是否审核"
        >
          <Switch
            onChange={this.handleChangePostAudit}
            checked={!!postConfig.is_post_audit}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="设置等级发表微帖"
        >
          <Switch
            onChange={this.handleChangePost}
            checked={!!postConfig.is_open_post_level}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="允许用户发帖等级"
        >
          <Select
            style={{ width: 100 }}
            value={postConfig.post_level}
            onChange={this.handlePostLevel}
            disabled={!postConfig.is_open_post_level}
          >
            {levelOption}
          </Select>
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="设置等级评论微帖"
        >
          <Switch
            onChange={this.handleChangeComment}
            checked={!!postConfig.is_open_comment_level}
            checkedChildren="是"
            unCheckedChildren="否"
          />
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="允许用户评论等级"
        >
          <Select
            style={{ width: 100 }}
            value={postConfig.comment_level}
            onChange={this.handleCommentLevel}
            disabled={!postConfig.is_open_comment_level}
          >
            {levelOption}
          </Select>
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="是否允许新用户发帖时间设置"
        >
          <Select
            style={{ width: 100 }}
            value={postConfig.new_user_post_time}
            onChange={this.handleUserTime}
          >
            {hoursOption}
          </Select>
        </FormItem>
      </Form>
    );
  }
}
