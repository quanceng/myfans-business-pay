import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import PostForm from './PostForm';

@connect(state => ({
  topic: state.topic,
}))
export default class PostCreate extends PureComponent {
  componentDidMount() {
    this.props.dispatch({
      type: 'topic/fetchTopic',
    });
  }
  // 创建微帖
  handleSubmit = (value) => {
    const data = {
      ...value,
      attachment_list: JSON.stringify({ file_path: value.image }),
    };
    delete data.image;
    this.props.dispatch({
      type: 'post/createPost',
      payload: data,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/post'));
        }, 1000);
      }
    });
  };
  render() {
    const { topics } = this.props.topic;
    return (
      <PageHeaderLayout>
        <PostForm topics={topics} handleSubmit={this.handleSubmit} />
      </PageHeaderLayout>
    );
  }
}
