import React, { PureComponent } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Link } from 'dva/router';
import { Table, Tag, Menu, Dropdown, Icon, Modal, Button, Form, Input, Select, Tooltip, message, Divider } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import GapUserModal from '../../../components/GagUserModal';
import DatePickers from '../../../components/DatePicker';
import { jsonToQueryString } from '../../../utils/utils';
import styles from './Post.less';

const imgUrl = 'http://cdn.myfans.cc/';
const Option = Select.Option;
const FormItem = Form.Item;
@Form.create()
export default class PostList extends PureComponent {
  state = {
    selectedRowKeys: [],
    startDate: null,
    endDate: null,
    postStyle: '-1',
    topicId: 0,
    postData: {},
    loading: true,
    visible: false,
    gagUserId: null,
    currentTopic: 0,
    q: '',
  };
  componentDidMount() {
    const { postData } = this.props.post;
    this.props.dispatch({
      type: 'post/fetchPost',
    }).then(() => {
      if (postData) {
        this.setState({
          loading: false,
          startDate: postData.start_time ? postData.start_time : null,
          endDate: postData.end_time ? postData.end_time : null,
          postStyle: postData.type_id ? postData.type_id : '-1',
          topicId: postData.topic_id ? postData.topic_id : 0,
          q: postData.q ? postData.q : '',
        });
      }
      this.setState({ loading: false });
    });
    this.props.dispatch({
      type: 'topic/fetchTopic',
    });
  }
  // 获取帖子列表
  fetch = () => {
    const { postData } = this.state;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/fetchPost',
      payload: postData,
    }).then(() => {
      this.setState({ loading: false });
    });
  };
  // 保存勾选帖子的帖子ID
  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  }
  // 保存选中更新的话题
  changeTopic = (value) => {
    this.setState({ currentTopic: value });
  };
  // 更新帖子话题请求
  updateTopics = () => {
    const { currentTopic, selectedRowKeys } = this.state;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/uploadTopic',
      payload: {
        threads_id: selectedRowKeys,
        topic_id: currentTopic,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({ selectedRowKeys: [] });
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 导出筛选帖子
  exportPost = () => {
    const { postData } = this.state;
    window.location.href = `/b/post-export?${jsonToQueryString(postData)}`;
  };
  // 帖子列表数据渲染
  columns = () => {
    const forumCommon = this.props.common.initData;
    return [
      {
        title: '用户',
        dataIndex: 'thread_id',
        render: (text, record) => {
          if (record.type_id === 11) {
            return (
              <span className={styles.userLogo}>
                <img
                  className={styles.tableAvatar}
                  src={forumCommon.forum_info.logo}
                  alt=""
                />
                &nbsp;
                <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{forumCommon.forum_info.name}</Ellipsis>
              </span>
            );
          } else {
            return (
              <span className={styles.userLogo}>
                <img
                  className={styles.tableAvatar}
                  src={record.user.avatar}
                  alt=""
                />
                &nbsp;
                <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.user.nickname}</Ellipsis>
              </span>
            );
          }
        },
      },
      {
        title: '话题',
        dataIndex: 'topic',
        render: (text, record) => {
          if (record.topic) {
            return <Tag color="green">{record.topic.topic_name}</Tag>;
          } else {
            return <Tag>无</Tag>;
          }
        },
      },
      {
        title: '微帖类型',
        render: (text, record) => {
          switch (Number(record.type_id)) {
            case 0:
              return this.renderTag('green', '文字');
            case 1:
              return this.renderTag('green', '图文');
            case 2:
              return this.renderTag('green', '图文');
            case 3:
              return this.renderTag('blue', '语音');
            case 7:
              return this.renderTag('blue', '视频');
            case 8:
              return this.renderTag('blue', '音乐');
            case 11:
              return this.renderTag('yellow', '文章');
            default:
              return this.renderTag('', '未知');
          }
        },
      },
      {
        title: '点赞',
        dataIndex: 'likes',
      },
      {
        title: '评论',
        dataIndex: 'comments',
      },
      {
        title: '浏览',
        dataIndex: 'views',
      },
      {
        title: '收藏',
        dataIndex: 'favorites',
      },
      {
        title: '转发',
        dataIndex: 'shares',
      },
      {
        title: '发布时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        width: 120,
        render: (text, record) => {
          const menu = (
            <Menu>
              <Menu.Item key="del">
                <a href="javascript:;" onClick={this.showDelModal.bind(this, record.thread_id)}>删除</a>
              </Menu.Item>
              {
                Number(record.type_id) === 11 ?
                  <Menu.Item key="edit">
                    <Link to={`/forum/post/${record.thread_id}/edit`}>编辑</Link>
                  </Menu.Item> : ''
              }
              <Menu.Item key="essence">
                {
                  Number(record.is_essence) === 0 ?
                    <a href="javascript:;" onClick={this.handleEssence.bind(this, record.thread_id)}>
                    加精
                    </a> :
                    <a href="javascript:;" onClick={this.cancleEssence.bind(this, record.thread_id)}>
                    取消加精
                    </a>
                }
              </Menu.Item>
              <Menu.Item key="top">
                {
                  Number(record.is_top) === 0 ?
                    <a href="javascript:;" onClick={this.handleTop.bind(this, record.thread_id)}>
                      置顶
                    </a> :
                    <a href="javascript:;" onClick={this.cancleTop.bind(this, record.thread_id)}>
                    取消置顶
                    </a>
                }
              </Menu.Item>
              {
                [5, 10, 11, 12, 16].indexOf(Number(record.type_id)) < 0 ?
                  <Menu.Item key="gag">
                    {record.author_advanced_info ?
                      Number(record.author_advanced_info.over_punish_dateline) === 0 ?
                        <a
                          href="javascript:;"
                          onClick={this.showGagModal.bind(this, record.user.user_id)}
                        >
                        禁言
                        </a> :
                        <a onClick={this.cancelGag.bind(this, record.user.user_id)}>
                        取消禁言
                        </a> :
                        <span>暂无法禁言</span>
                    }
                  </Menu.Item> : ''
              }
              {
                [5, 10, 11, 16, 12].indexOf(Number(record.type_id)) < 0 ?
                  <Menu.Item key="black">
                    <a
                      href="javascript:void(0)"
                      onClick={this.handleBlack.bind(this, record.user.user_id)}
                    >
                    加入黑名单
                    </a>
                  </Menu.Item> : ''
              }
              <Menu.Item key="link">
                <CopyToClipboard
                  text={`${forumCommon.forum_info.forum_host}/c/${forumCommon.forum_info.forum_code}/share?url=%2Fpost%2Fshow%2F${record.thread_id}`}
                  onCopy={() => { message.success('复制成功'); }}
                >
                  <a>复制微帖链接</a>
                </CopyToClipboard>
              </Menu.Item>
            </Menu>
          );
          return (
            <div>
              <Link to={`/forum/post/${record.thread_id}`}>回复</Link>
              <Divider type="vertical" />
              {
                !(record.author_advanced_info && Number(record.author_advanced_info.is_black) === 1) ?
                  <Dropdown
                    overlay={menu}
                    trigger={['click']}
                  >
                    <a className="ant-dropdown-link" href="#">
                      更多<Icon type="down" />
                    </a>
                  </Dropdown> :
                  <a
                    href="javascript:;"
                    onClick={this.cancleBlack.bind(this, record.user.user_id)}
                  >
                  移出黑名单
                  </a>
              }
            </div>
          );
        },
      },
    ];
  };
  // 帖子列表尾部渲染函数
  renderFooter = () => {
    const { updateTopics } = this.props.topic;
    return (
      <Form
        hideRequiredMark
        layout="inline"
      >
        <FormItem label="分类" >
          <Select
            style={{ width: 160, marginRight: 8 }}
            notFoundContent="没有话题分类请添加"
            placeholder="选择话题分类"
            value={this.state.currentTopic}
            onChange={this.changeTopic}
          >
            <Option key="none" value={0}>
              无
            </Option>
            { updateTopics ? updateTopics.map(item =>
              (
                <Option
                  key={item.topic_id}
                  value={item.topic_id}
                >
                  {item.topic_name}
                </Option>
              )
            ) : ''}
          </Select>
          <Button type="primary" onClick={this.updateTopics}>更新分类话题</Button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Tooltip title="导出默认为筛选最近7天内的微帖，无需勾选帖子">
            <Button type="primary" onClick={this.exportPost}>导出当前筛选微帖</Button>
          </Tooltip>
        </FormItem>
      </Form>
    );
  };
  // 帖子隐藏信息渲染函数
  renderContent = (record) => {
    const qqMusicUrl = 'http://ws.stream.qqmusic.qq.com/';
    switch (Number(record.type_id)) {
      case 1:
        return (
          <div>
            <div dangerouslySetInnerHTML={{ __html: record.message }} />
            {
              record.attachment_list.map(item =>
                (
                  <a
                    href={item.url}
                    target="_blank"
                    key={item.file_path}
                  >
                    <img
                      style={{ marginRight: 10, marginTop: 10, width: 100 }}
                      src={imgUrl + item.file_path}
                      key={item.file_path}
                      alt=""
                    />
                  </a>
                )
              )
            }
          </div>
        );
      case 2:
        return (
          <div>
            <div dangerouslySetInnerHTML={{ __html: record.message }} />
            {
              record.attachment_list.map(item =>
                (
                  <a href={item.url} target="_blank" key={item.file_path}>
                    <img
                      style={{ marginRight: 10, marginTop: 10, width: 100 }}
                      src={imgUrl + item.file_path}
                      key={item.file_path}
                      alt=""
                    />
                  </a>)
              )
            }
          </div>
        );
      case 3:
        return (
          <div>
            <div dangerouslySetInnerHTML={{ __html: record.message }} />
            <audio
              controls
              preload="none"
              src={imgUrl + record.attachment_list.file_path}
            />
          </div>
        );
      case 7:
        return (
          <div>
            <div dangerouslySetInnerHTML={{ __html: record.message }} />
            <video controls preload="none" style={{ width: 200 }} src={imgUrl + record.attachment_list.file_path} />
          </div>
        );
      case 8:
        return (
          <div>
            <div>分享歌曲：{record.attachment_list.singer} - {record.attachment_list.song_name}</div>
            <div dangerouslySetInnerHTML={{ __html: record.message }} />
            <audio controls preload="none" src={qqMusicUrl + record.attachment_list.file_path} />
          </div>
        );
      case 9:
        return (
          <div>
            {record.attachment_list.goods_name}
          </div>
        );
      default:
        return <div dangerouslySetInnerHTML={{ __html: record.message }} />;
    }
  }
  // 帖子类型彩色样式渲染
  renderTag = (color, text) => {
    return <Tag color={color}>{text}</Tag>;
  }
  // 删除帖子弹框提醒
  showDelModal = (postId, event) => {
    event.preventDefault();
    const self = this;
    Modal.confirm({
      title: '确定要删除这篇微帖吗？',
      content: '微帖删除后将无法恢复！！！',
      onOk() {
        self.setState({ loading: true });
        self.props.dispatch({
          type: 'post/delPost',
          payload: postId,
        }).then((response) => {
          if (response.code === 1) {
            setTimeout(() => {
              self.fetch();
            }, 1000);
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  // 禁言弹框提醒
  showGagModal = (userId, event) => {
    event.preventDefault();
    this.setState({
      visible: true,
      gagUserId: userId,
    });
  }
  // 收起禁言弹框
  handleCancel = () => {
    this.setState({
      visible: false,
      gagUserId: null,
    });
  }
  // 禁言请求发送
  handleGapModal = (reason, gagTime) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/fetchGag',
      payload: {
        user_id: this.state.gagUserId,
        reason,
        gag_time: gagTime,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.setState({ visible: false });
          this.fetch();
        }, 1000);
      }
    });
  };
  // 取消禁言请求
  cancelGag = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancelGag',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 拉黑请求
  handleBlack = (userId) => {
    const self = this;
    Modal.confirm({
      title: '确认提醒',
      content: '您是否确定要将该用户加入黑名单',
      onOk() {
        self.setState({ loading: true });
        self.props.dispatch({
          type: 'user/fetchBlack',
          payload: {
            user_id: userId,
          },
        }).then((response) => {
          if (response.code === 1) {
            setTimeout(() => {
              self.fetch();
            }, 1000);
          }
        });
      },
      onCancel() {},
    });
  };
  // 取消拉黑请求
  cancleBlack = (userId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'user/cancleBlack',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 点赞请求
  handleFavor = (threadId, status) => {
    this.props.dispatch({
      type: 'post/updateFavor',
      payload: {
        id: threadId,
        data: {
          hot_say: status,
        },
      },
    });
  };
  // 加精请求
  handleEssence = (threadId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/fetchEssence',
      payload: {
        thread_id: threadId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  }
  // 取消加精请求
  cancleEssence = (threadId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/cancleEssence',
      payload: threadId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  }
  // 置顶
  handleTop = (threadId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/fetchTop',
      payload: {
        thread_id: threadId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  }
  // 取消置顶
  cancleTop = (threadId) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'post/cancleTop',
      payload: threadId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  }
  // 帖子话题筛选选中回调
  handleTopic = (value) => {
    this.setState({ topicId: value });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    const postData = this.state.postData;
    postData.page = pagination.current;
    this.setState({ postData });
    setTimeout(() => {
      this.fetch();
    }, 1000);
  }
  // 帖子类型筛选选中回调
  handlePostStyle = (value) => {
    this.setState({ postStyle: value });
  }
  // 保存筛选时间
  changeDate = (startDate, endDate) => {
    this.setState({
      startDate,
      endDate,
    });
  };
  // 筛选帖子列表
  handleSubmit = (e) => {
    e.preventDefault();
    const { postStyle, topicId, startDate, endDate } = this.state;
    const query = this.props.form.getFieldValue('q');
    const params = {};
    if (topicId !== 0) {
      params.topic_id = topicId;
    }
    if (startDate && endDate) {
      params.start_at = startDate;
      params.end_at = endDate;
    }
    if (query) {
      params.keyword = query;
    }
    params.type_id = postStyle;
    this.setState({ postData: params });
    setTimeout(() => {
      this.fetch();
    }, 1000);
  }
  render() {
    const { post, topic } = this.props;
    const { posts, postPage } = post;
    const { selectedRowKeys, postStyle, topicId, loading, visible, q, startDate, endDate } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            <Button type="primary" style={{ backgroundColor: 'red', borderColor: 'red' }}>
              <Link to="/forum/post/create">新建微帖</Link>
            </Button>
          </FormItem>
        </Form>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('q', {
              initialValue: q,
            })(<Input
              placeholder="输入关键字"
              style={{ width: 100 }}
            />)}
          </FormItem>
          <FormItem>
            <DatePickers
              changeDate={this.changeDate}
              startDate={startDate}
              endDate={endDate}
            />
          </FormItem>
          <FormItem>
            <Select
              onChange={this.handleTopic}
              value={topicId}
              style={{ width: 100 }}
            >
              <Option key="topicAll" value={0}>
                全部话题
              </Option>
              { topic ? topic.topics.map((item) => {
                return (
                  <Option
                    key={`topic_id_${item.topic_id}`}
                    value={item.topic_id}
                  >
                    {item.topic_name}
                  </Option>
                );
              }) : ''
            }
            </Select>
          </FormItem>
          <FormItem>
            <Select
              onSelect={this.handlePostStyle}
              value={postStyle}
              style={{ width: 100 }}
            >
              <Option key="all" value="-1">
                全部类型
              </Option>
              <Option key="business" value="0">
                文字
              </Option>
              <Option key="pic" value="2">
                图文
              </Option>
              <Option key="voice" value="3">
                语音
              </Option>
              <Option key="video" value="7">
                视频
              </Option>
              <Option key="music" value="8">
                音乐
              </Option>
              <Option key="artile" value="11">
                文章
              </Option>
            </Select>
          </FormItem>
          <FormItem
            style={{ marginRight: 0 }}
          >
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Table
          className="mt-20"
          rowKey={record => record.thread_id}
          dataSource={posts}
          pagination={postPage}
          loading={loading}
          columns={this.columns()}
          expandedRowRender={record => this.renderContent(record)}
          rowSelection={rowSelection}
          onChange={this.handleTableChange}
          footer={() => this.renderFooter()}
        />
        <GapUserModal
          visible={visible}
          handleGapModal={this.handleGapModal}
          handleCancel={this.handleCancel}
        />
      </div>
    );
  }
}
