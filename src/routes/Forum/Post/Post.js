import React, { PureComponent } from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import PostList from './PostList';
import PostSet from './PostSet';
import PostAudit from './PostAudit';

const tabList = [
  {
    key: 'list',
    tab: '微帖列表',
  },
  {
    key: 'setting',
    tab: '微帖设置',
  },
  {
    key: 'audit',
    tab: '审核机制',
  },
];
@connect(state => ({
  post: state.post,
  common: state.common,
  topic: state.topic,
}))
export default class Post extends PureComponent {
  state = {
    tab: 'list',
  }
  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      list: PostList,
      setting: PostSet,
      audit: PostAudit,
    };
    return componentMap[this.state.tab];
  }
  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }
  render() {
    const { post, common, topic, dispatch } = this.props;
    const CurrentComponent = this.getCurrentComponent();// 头部筛选数据
    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          common={common}
          post={post}
          topic={topic}
          dispatch={dispatch}
        />
      </PageHeaderLayout>
    );
  }
}
