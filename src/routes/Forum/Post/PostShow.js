import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, message, Popconfirm, Modal, Table, Menu, Dropdown, Icon, Select } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import GapUserModal from '../../../components/GagUserModal';
import styles from './Post.less';

const imgUrl = 'http://cdn.myfans.cc/';
const Option = Select.Option;
@connect(state => ({
  post: state.post,
  vest: state.vest,
  common: state.common,
}))
export default class PostShow extends PureComponent {
  state = {
    visible: false,
    confirmLoading: false,
    isShowQr: false,
    comment: '',
    comPlaceholder: '请输入内容',
    commentId: 0,
    parentId: 0,
    isReply: 0,
    commentUserId: 0,
    receiverId: 0,
    threadId: this.props.match.params.id,
  }
  componentDidMount() {
    const { threadId } = this.state;
    this.props.dispatch({
      type: 'post/fetchPostDetail',
      payload: threadId,
    });
    this.props.dispatch({
      type: 'post/fetchComments',
      payload: {
        thread_id: threadId,
        page: 1,
      },
    });
    this.props.dispatch({
      type: 'vest/fetchVest',
      payload: { is_all: 1 },
    });
  }
  // 删除帖子
  deletePost = (id) => {
    this.props.dispatch({
      type: 'post/delPost',
      payload: id,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/post'));
        }, 1000);
      }
    });
  };
  // 拉黑
  pullBlack = (userId) => {
    const self = this;
    Modal.confirm({
      title: '确认提醒',
      content: '您是否确定要将该用户加入黑名单',
      onOk() {
        self.props.dispatch({
          type: 'user/fetchBlack',
          payload: {
            user_id: userId,
          },
        }).then((response) => {
          if (response.code === 1) {
            setTimeout(() => {
              self.props.dispatch({
                type: 'post/fetchPostDetail',
                payload: self.state.threadId,
              });
              self.props.dispatch({
                type: 'post/fetchComments',
                payload: {
                  thread_id: self.state.threadId,
                  page: self.props.post.commentsPage,
                },
              });
            }, 1000);
          }
        });
      },
      onCancel() {},
    });
  }
  // 取消拉黑
  cancleBlack = (userId) => {
    this.props.dispatch({
      type: 'user/cancleBlack',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: this.state.threadId,
          });
          this.props.dispatch({
            type: 'post/fetchComments',
            payload: {
              thread_id: this.state.threadId,
              page: this.props.post.commentsPage,
            },
          });
        }, 1000);
      }
    });
  }
  // 加精请求
  handleEssence = (threadId) => {
    this.props.dispatch({
      type: 'post/fetchEssence',
      payload: {
        thread_id: threadId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: threadId,
          });
        }, 1000);
      }
    });
  }
  // 取消加精请求
  cancleEssence = (threadId) => {
    this.props.dispatch({
      type: 'post/cancleEssence',
      payload: threadId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: threadId,
          });
        }, 1000);
      }
    });
  }
  // 置顶
  handleTop = (threadId) => {
    this.props.dispatch({
      type: 'post/fetchTop',
      payload: {
        thread_id: threadId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: threadId,
          });
        }, 1000);
      }
    });
  }
  // 取消置顶
  cancleTop = (threadId) => {
    this.props.dispatch({
      type: 'post/cancleTop',
      payload: threadId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: threadId,
          });
        }, 1000);
      }
    });
  }
  // 禁言弹框提醒
  showGagModal = (userId, event) => {
    event.preventDefault();
    this.setState({
      visible: true,
      gagUserId: userId,
    });
  }
  // 收起禁言弹框
  handleCancel = () => {
    this.setState({
      visible: false,
      gagUserId: null,
    });
  }
  // 禁言请求发送
  handleGapModal = (reason, gagTime) => {
    this.props.dispatch({
      type: 'user/fetchGag',
      payload: {
        user_id: this.state.gagUserId,
        reason,
        gag_time: gagTime,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.setState({
            visible: false,
            gagUserId: null,
          });
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: this.state.threadId,
          });
          this.props.dispatch({
            type: 'post/fetchComments',
            payload: {
              thread_id: this.state.threadId,
              page: this.props.post.commentsPage,
            },
          });
        }, 1000);
      }
    });
  };
  // 取消禁言请求
  cancelGag = (userId) => {
    this.props.dispatch({
      type: 'user/cancelGag',
      payload: userId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchPostDetail',
            payload: this.state.threadId,
          });
          this.props.dispatch({
            type: 'post/fetchComments',
            payload: {
              thread_id: this.state.threadId,
              page: this.props.post.commentsPage,
            },
          });
        }, 1000);
      }
    });
  };
  // 显示帖子链接二维码
  showQR = () => {
    this.setState({ isShowQr: true });
  };
  // 隐藏帖子链接二维码
  hideQR = () => {
    this.setState({ isShowQr: false });
  };
  // A标签兼容操作
  openLink = (e) => {
    if (e.target.nodeName === 'A' && e.target.dataset.href) {
      window.open(e.target.dataset.href);
    }
  };
  // 输入评论内容
  clickFocus = (record) => {
    this.textTextarea.focus();
    if (record) {
      this.setState({
        comPlaceholder: `回复@${record.author.nickname}:`,
        receiverId: record.author.user_id,
        commentId: record.comment_id,
        parentId: record.comment_id,
        isReply: 1,
      });
    } else {
      this.setState({
        comPlaceholder: '',
        receiverId: 0,
        commentId: 0,
        parentId: 0,
        isReply: 1,
      });
    }
  };
  // 输入子评论内容
  clickChildFocus = (record, commentId) => {
    this.textTextarea.focus();
    this.setState({
      comPlaceholder: `回复@${record.author.nickname}:`,
      receiverId: record.author.user_id,
      commentId: record.comment_id,
      parentId: commentId,
      isReply: 0,
    });
  }
  // 保存评论者ID
  handleCommentUser= (value) => {
    this.setState({ commentUserId: value });
  }
  // 保存评论内容
  setComment = (e) => {
    this.setState({ comment: e.target.value });
  };
  // 发表评论
  postComment = () => {
    const { postDetail } = this.props.post;
    const { vests } = this.props.vest;
    const { commentId, parentId, receiverId, comment, threadId, commentUserId, isReply } = this.state;
    if (!vests.length) {
      message.info('请先创建马甲用户');
      return;
    }
    if (!commentUserId) {
      message.info('请先选择评论者身份');
      return;
    }
    if (!comment) {
      message.error('请输入评论内容');
      return;
    }
    this.props.dispatch({
      type: 'post/createComments',
      payload: {
        thread_id: postDetail.thread_id,
        author_id: commentUserId,
        type: 0,
        receiver_id: receiverId,
        parent_id: parentId,
        comment_id: commentId,
        content: comment,
        is_reply: isReply,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          comment: '',
          comPlaceholder: '请输入内容',
          commentId: 0,
          parentId: 0,
          receiverId: 0,
          isReply: 0,
        });
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchComments',
            payload: {
              thread_id: threadId,
              page: 1,
            },
          });
        }, 1000);
      }
    });
  }
  // 删除评论
  deleteComment = (commentId) => {
    this.props.dispatch({
      type: 'post/delComments',
      payload: commentId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'post/fetchComments',
            payload: {
              thread_id: this.state.threadId,
              page: 1,
            },
          });
        }, 1000);
      }
    });
  }
  // 加载帖子操作
  renderUserSetting = () => {
    const postDetail = this.props.post.postDetail;
    const { visible, confirmLoading } = this.state;
    return (
      <span>
        {Number(postDetail.is_essence) === 0 ?
          <a
            href="javascript:;"
            onClick={this.handleEssence.bind(this, postDetail.thread_id)}
          >
            加精
          </a> :
          <a
            href="javascript:;"
            onClick={this.cancleEssence.bind(this, postDetail.thread_id)}
          >
            取消加精
          </a>
        }&nbsp;
        {Number(postDetail.is_top) === 0 ?
          <a
            href="javascript:;"
            onClick={this.handleTop.bind(this, postDetail.thread_id)}
          >
            置顶
          </a> :
          <a
            href="javascript:;"
            onClick={this.cancleTop.bind(this, postDetail.thread_id)}
          >
            取消置顶
          </a>
        }&nbsp;
        {Number(postDetail.type_id) === 11 ?
          postDetail.user ?
          Number(postDetail.user.over_punish_dateline) === 0 ?
            <span>
              <a
                href="javascript:;"
                onClick={this.showGagModal.bind(this, postDetail.author_id)}
              >
                禁言
              </a>&nbsp;
            </span> :
            <span>
              <a
                href="javascript:;"
                onClick={this.cancelGag.bind(this, postDetail.author_id)}
              >
                取消禁言
              </a>
            &nbsp;
            </span> :
            <span>暂无法禁言</span> :
          ''
        }
        {Number(postDetail.type_id) === 11 ?
          <span>
            <a
              href="javascript:;"
              onClick={this.pullBlack.bind(this, postDetail.author_id)}
            >
              加入黑名单
            </a>&nbsp;
          </span> :
        ''}
        <GapUserModal
          visible={visible}
          loading={confirmLoading}
          handleGapModal={this.handleGapModal}
          handleCancel={this.handleCancel}
        />
      </span>
    );
  };
  // 加载帖子内容
  renderContent = () => {
    const detail = this.props.post.postDetail;
    const { isShowQr } = this.state;
    const qqMusicUrl = 'http://ws.stream.qqmusic.qq.com/';
    switch (Number(detail.type_id)) {
      // 图文
      case 1:
        return (
          <div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            {detail.attachment_list.map(item => (
              <a href={imgUrl + item.file_path} target="_blank" key={item.file_path}>
                <img
                  style={{
                    marginRight: 10,
                    marginTop: 10,
                    verticalAlign: 'middle',
                    width: 120,
                  }}
                  key={item.file_path}
                  src={imgUrl + item.file_path}
                  alt=""
                />
              </a>
            ))}
          </div>
        );
      // 图文
      case 2:
        return (
          <div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            {detail.attachment_list.map(item => (
              <a
                href={imgUrl + item.file_path}
                target="_blank"
                key={item.file_path}
              >
                <img
                  style={{
                    marginRight: 10,
                    marginTop: 10,
                    verticalAlign: 'middle',
                    width: 120,
                  }}
                  key={item.file_path}
                  src={imgUrl + item.file_path}
                  alt=""
                />
              </a>
            ))}
          </div>
        );
      // 语音
      case 3:
        return (
          <div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            <audio
              controls
              preload="none"
              src={imgUrl + detail.attachment_list.file_path}
            />
          </div>
        );
      // 视频
      case 7:
        return (
          <div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            <video
              controls
              preload="meta"
              src={imgUrl + detail.attachment_list.file_path}
              width={400}
              height={520}
            />
          </div>
        );
      // 音乐
      case 8:
        return (
          <div>
            <div>
              分享歌曲：{detail.attachment_list.singer}
              - {detail.attachment_list.song_name}
            </div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            <audio
              controls
              preload="none"
              src={qqMusicUrl + detail.attachment_list.file_path}
            />
          </div>
        );
      // 文章
      case 11:
        return (
          <div>
            <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
            <div
              style={{ position: 'relative' }}
            >
              <a
                href="#"
                onFocus={() => {}}
                onBlur={() => {}}
                onMouseOver={this.showQR}
                onMouseOut={this.hideQR}
              >
                <Icon type="qrcode" />查看原文
              </a>
              { isShowQr ? <img
                src={`/b/post-qrcode/${detail.thread_id}`}
                alt="qr_code"
                style={{
                  width: 150,
                  position: 'absolute',
                  left: 15,
                  top: 20,
                }}
              /> : '' }
            </div>
          </div>
        );
      default:
        return (
          <div onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: detail.message }} />
        );
    }
  };
  // 加载评论列表
  columns() {
    return [
      {
        title: '用户评论',
        dataIndex: 'comment_id',
        render: (text, record) => {
          const menu = (
            <Menu>
              <Menu.Item>
                <a
                  href="javascript:;"
                  onClick={this.clickFocus.bind(this, record)}
                >
                  回复
                </a>
              </Menu.Item>
              <Menu.Item>
                <a onClick={this.showGagModal.bind(this, record.author.user_id)}>禁言</a>
              </Menu.Item>
              <Menu.Item>
                <a onClick={this.pullBlack.bind(this, record.author.user_id)}>加入黑名单</a>
              </Menu.Item>
              <Menu.Item>
                <Popconfirm
                  title="确定要删除这条评论吗？"
                  onConfirm={this.deleteComment.bind(this, record.comment_id)}
                >
                  <a href="#">删除</a>
                </Popconfirm>
              </Menu.Item>
            </Menu>
          );
          return (
            <div>
              <div className={styles.comment}>
                <img
                  className={styles.avatar}
                  src={record.author.avatar}
                  alt="avatar"
                />
                <div className={styles.comment_container}>
                  <div className={styles.user_meta}>
                    <span className={styles.name}>
                      {record.author.nickname}
                    </span>
                  </div>
                  <div className={styles.comment_content}>
                    {this.renderCommentContent(record)}
                  </div>
                  <p className={styles.action}>{record.updated_at}</p>
                  <div>
                    { record.son ? record.son.map(item => (this.renderChildrenContent(item, record.comment_id))) : '' }
                  </div>
                </div>
              </div>
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link">
                  <Icon type="ellipsis" style={{ paddingTop: 12, fontSize: 18 }} />
                </a>
              </Dropdown>
            </div>
          );
        },
      },
    ];
  }
  // 评论列表翻页
  handleTableChange = (pagination) => {
    this.props.dispatch({
      type: 'post/fetchComments',
      payload: {
        thread_id: this.state.threadId,
        page: pagination.current,
      },
    });
  }
  // 加载评论内容
  renderCommentContent = (record) => {
    switch (record.type) {
      case 0:
        return (
          <div>
            <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.content }} />
          </div>
        );
      case 1:
        return (
          <div>
            <div>
              <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.content }} />
            </div>
            <a
              href={imgUrl + record.attachment_list.file_path}
              target="_blank"
            >
              <img
                style={{
                  marginRight: 10,
                  marginTop: 10,
                  width: 120,
                }}
                src={imgUrl + record.attachment_list.file_path}
                alt=""
              />
            </a>
          </div>
        );
      case 2:
        return (
          <div>
            <div>
              <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.content }} />
            </div>
            <audio
              controls
              preload="none"
              src={imgUrl + record.attachment_list.file_path}
            />
          </div>
        );
      default:
        return <div />;
    }
  }
  // 加载子评论内容
  renderChildrenContent = (record, commentId) => {
    const menu = (
      <Menu>
        <Menu.Item>
          <a
            href="javascript:;"
            onClick={this.clickChildFocus.bind(this, record, commentId)}
          >
            回复
          </a>
        </Menu.Item>
        <Menu.Item>
          <a onClick={this.showGagModal.bind(this, record.author.user_id)}>禁言</a>
        </Menu.Item>
        <Menu.Item>
          <a onClick={this.pullBlack.bind(this, record.author.user_id)}>加入黑名单</a>
        </Menu.Item>
        <Menu.Item>
          <Popconfirm
            title="确定要删除这条评论吗？"
            onConfirm={this.deleteComment.bind(this, record.comment_id)}
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Menu.Item>
      </Menu>
    );
    switch (record.type) {
      case 0:
        return (
          <div key={record.comment_id} style={{ position: 'relative' }}>
            <div style={{ lineHeight: '30px', paddingLeft: 10, paddingRight: 18 }}>
              <img
                className={styles.child_avatar}
                src={record.author.avatar}
                alt="avatar"
              />
              { record.child_receiver_info ?
                <span>
                  <a>{record.author.nickname}</a>
                    回复
                  <a>
                    {record.child_receiver_info.nickname}
                  </a>：
                </span> :
                <a>{record.author.nickname}：</a>
              }
              <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.content }} />
            </div>
            <span style={{ position: 'absolute', right: 0, top: 7 }}>
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link">
                  <Icon type="ellipsis" style={{ fontSize: 16 }} />
                </a>
              </Dropdown>
            </span>
          </div>
        );
      case 1:
        return (
          <div key={record.comment_id} style={{ position: 'relative' }}>
            <div>
              { record.child_receiver_info ?
                <span>
                  <a>{record.author.nickname}</a>回复
                  <a>{record.child_receiver_info.nickname}</a>：
                </span> :
                <a>{record.author.nickname}：</a>
              }
              <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.message }} />
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link" href="#">
                  <Icon type="ellipsis" style={{ float: 'right', fontSize: 18 }} />
                </a>
              </Dropdown>
            </div>
            <a
              href={imgUrl + record.attachment_list.file_path}
              target="_blank"
            >
              <img
                style={{
                  marginRight: 10,
                  marginTop: 10,
                  width: 120,
                }}
                src={imgUrl + record.attachment_list.file_path}
                alt=""
              />
            </a>
            <span style={{ position: 'absolute', right: 0, top: 7 }}>
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link">
                  <Icon type="ellipsis" style={{ fontSize: 16 }} />
                </a>
              </Dropdown>
            </span>
          </div>
        );
      case 2:
        return (
          <div key={record.comment_id} style={{ position: 'relative' }}>
            <div>
              { record.child_receiver_info ?
                <span>
                  <a>{record.author.nickname}</a>回复
                  <a>{record.child_receiver_info.nickname}</a>：
                </span> :
                <a>{record.author.nickname}：</a>
              }
              <span onClick={(e) => { this.openLink(e); }} dangerouslySetInnerHTML={{ __html: record.message }} />
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link" href="#">
                  <Icon type="ellipsis" style={{ float: 'right', fontSize: 18 }} />
                </a>
              </Dropdown>
            </div>
            <audio
              controls
              preload="none"
              src={imgUrl + record.attachment_list.file_path}
            />
            <span style={{ position: 'absolute', right: 0, top: 7 }}>
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link">
                  <Icon type="ellipsis" style={{ fontSize: 16 }} />
                </a>
              </Dropdown>
            </span>
          </div>
        );
      default:
        return <div key={record.comment_id} />;
    }
  }
  render() {
    const { common, post, vest } = this.props;
    const { comments, commentsPage, postDetail } = post;
    const { vests } = vest;
    const { comment, comPlaceholder, commentUserId } = this.state;
    if (postDetail) {
      return (
        <PageHeaderLayout>
          <div className={styles.detail}>
            <div className={styles.container}>
              <div className={styles.avatar}>
                <img src={postDetail.type_id === 11 ? common.initData.forum_info.logo : postDetail.user.avatar} alt="avatar" />
              </div>
              <div className={styles.info}>
                <h3>{postDetail.type_id === 11 ? common.initData.forum_info.name : postDetail.user.nickname}</h3>
                <p>
                  {postDetail.created_at}
                </p>
              </div>
            </div>
            <div className={styles.content}>
              <div className={styles.article}>
                <article>
                  {this.renderContent()}
                </article>
                <div className={styles.operation}>
                  <a
                    href="javascript:;"
                    onClick={this.clickFocus.bind(this, '')}
                  >
                    回复
                  </a>
                  &nbsp;
                  <Popconfirm
                    title="确定要删除这条微帖吗？"
                    onConfirm={this.deletePost.bind(this, postDetail.thread_id)}
                  >
                    <a href="#">删除</a>
                  </Popconfirm>
                  &nbsp;
                  {[11].indexOf(Number(postDetail.type_id)) > 0 || (postDetail.user && postDetail.user.is_black === 0) ?
                    this.renderUserSetting() :
                    <a href="javascript:;" onClick={this.cancleBlack.bind(this, postDetail.author_id)}>移出黑名单</a>}
                </div>
              </div>
            </div>
            <h4 style={{ margin: '10px auto' }}>
              评论 &nbsp; 共{postDetail.comments}条
            </h4>
            <textarea
              value={comment}
              onChange={this.setComment}
              style={{
                width: '100%',
                border: '1px solid #909090',
                borderRadius: 5,
              }}
              ref={(textarea) => { this.textTextarea = textarea; }}
              type="textarea"
              rows={4}
              placeholder={comPlaceholder}
            />
            <div style={{ marginTop: 10, overflow: 'hidden' }}>
              <Button
                style={{ float: 'right' }}
                type="primary"
                onClick={this.postComment}
              >
                发表
              </Button>
              <Select
                onChange={this.handleCommentUser}
                value={commentUserId}
                style={{ width: 200, float: 'right', marginRight: 10 }}
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                placeholder="请选择评论者/马甲身份"
              >
                <Option value={0}>请选择马甲</Option>
                { vests ? vests.map((item) => {
                  return (
                    <Option
                      value={item.user_id}
                      key={item.user_id}
                    >
                      {item.user_info.nickname}
                    </Option>
                  );
                }) : null
              }
              </Select>
            </div>
            <div className={styles.comments}>
              <Table
                rowKey={record => record.comment_id}
                columns={this.columns()}
                pagination={commentsPage}
                dataSource={comments}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </PageHeaderLayout>
      );
    }
    return (
      <div />
    );
  }
}
