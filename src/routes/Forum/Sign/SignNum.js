import React, { PureComponent } from 'react';
import lodash from 'lodash';
import { Spin } from 'antd';
import Curved from '../../../components/Echart/Curved';
import SearchFrom from '../../../components/Echart/SearchForm';
import styles from './Sign.less';

export default class SignNum extends PureComponent {
  state = {
    loading: false,
  }
  componentDidMount() {
    this.getSignNum({ date_type: 'week' });
  }
  getSignNum = (params) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'sign/signNum',
      payload: params,
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  render() {
    const { signNum } = this.props.sign;
    // 图表数据
    const xAxisData = [];
    const signNumData = [];
    if (signNum) {
      lodash.forEach(signNum, (item) => {
        xAxisData.push(item.time);
        signNumData.push(item.num);
      });
    }
    const fields = { signTime: '签到人数' };
    const chartData = {
      xAxisData,
      seriesData: [
        { name: fields.signTime, data: signNumData },
      ],
    };
    return (
      <div>
        <SearchFrom handleSearch={this.getSignNum} />
        <Spin spinning={this.state.loading} >
          <div className={styles.chartWrapper}>
            <Curved chartData={chartData} fields={fields} />
          </div>
        </Spin>
      </div>
    );
  }
}
