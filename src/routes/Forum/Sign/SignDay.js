import React, { PureComponent } from 'react';
import { Button, Form, DatePicker, Table, Alert } from 'antd';
import styles from './Sign.less';

const FormItem = Form.Item;
export default class SignDay extends PureComponent {
  state = {
    signData: {},
    loading: false,
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'sign/signDay',
    });
  }
  // 筛选签到用户列表
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const signData = {
        ...values,
      };
      signData.day = signData.day.format('YYYY-MM-DD');
      this.setState({ loading: true });
      this.props.dispatch({
        type: 'sign/signDay',
        payload: signData,
      }).then(() => {
        this.setState({
          loading: false,
          signData,
        });
      });
    });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    const { signData } = this.state;
    this.setState({ loading: true });
    signData.page = pagination.current;
    this.props.dispatch({
      type: 'sign/signDay',
      payload: signData,
    }).then(() => {
      this.setState({
        loading: false,
        signData,
      });
    });
  }
  // 签到数据列表加载
  columns = () => {
    return [
      {
        title: '序号',
        dataIndex: 'user_id',
        render: (text, record, index) => {
          return <span>{index + 1}</span>;
        },
      },
      {
        title: '用户',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img className={styles.tableAvatar} src={record.user_info.avatar} alt="" />
            &nbsp;
            {record.user_info.nickname}
          </span>
        ),
      },
      {
        title: '签到时间',
        dataIndex: 'created_at',
      },
    ];
  };
  render() {
    const { signDayList, signDayPage } = this.props.sign;
    const { getFieldDecorator } = this.props.form;
    const { loading } = this.state;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('day', {
               rules: [{ required: true, message: '请选择查询日期' }],
            })(
              <DatePicker />
            )}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Alert className="mt-20" message="连续签到可获经验值奖励（每个月的第7天额外增加经验7点、第30天额外增加经验30点）" type="info" showIcon />
        <Table
          rowKey={record => record.user_id}
          dataSource={signDayList}
          pagination={signDayPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
      </div>
    );
  }
}
