import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import SignNum from './SignNum';
import SignList from './SignList';
import SignDay from './SignDay';

const tabList = [
  {
    key: 'num',
    tab: '签到人数',
  },
  {
    key: 'list',
    tab: '签到统计',
  },
  {
    key: 'day',
    tab: '签到详细',
  },
];
@connect(state => ({
  sign: state.sign,
}))
@Form.create()
export default class Sign extends PureComponent {
  state = {
    tab: 'num',
  }
  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      num: SignNum,
      list: SignList,
      day: SignDay,
    };
    return componentMap[this.state.tab];
  }
  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }
  render() {
    const { sign, form, dispatch } = this.props;
    const CurrentComponent = this.getCurrentComponent();// 头部筛选数据
    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          sign={sign}
          form={form}
          dispatch={dispatch}
        />
      </PageHeaderLayout>
    );
  }
}
