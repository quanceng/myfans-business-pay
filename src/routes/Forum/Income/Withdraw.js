import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Form, InputNumber, Divider, Modal, Row, Col, Icon, Input } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import SMSButton from '../../../components/SMSButton';
import styles from './Income.less';

const FormItem = Form.Item;
@connect(state => ({
  income: state.income,
  common: state.common,
}))
@Form.create()
export default class Record extends PureComponent {
  state = {
    loading: false,
    visible: false,
    txt: '添加',
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'income/getMoney',
    });
    this.getUser();
  }
  // 提现账户获取
  getUser = () => {
    this.props.dispatch({
      type: 'income/getUser',
    });
  };
  // 添加/修改支付宝
  handleFixbind = (e) => {
    e.preventDefault();
    this.props.form.validateFields(['fix_sms_code', 'real_name', 'logonid'], (err, values) => {
      if (err) {
        return;
      }
      this.props.dispatch({
        type: 'income/updataAccount',
        payload: {
          sms_code: values.fix_sms_code,
          real_name: values.real_name,
          logonid: values.logonid,
          account_id: this.props.income.account ? this.props.income.account.account_id : 0,
        },
      }).then((response) => {
        if (response.code === 1) {
          this.setState({ visible: false });
          setTimeout(() => {
            this.getUser();
          }, 1000);
        }
      });
    });
  }
  // 提现
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(['withdraw_amount'], (err, values) => {
      if (err) {
        return;
      }
      this.props.dispatch({
        type: 'income/withdrawal',
        payload: {
          withdraw_amount: values.withdraw_amount,
          relation_type: 2,
          relation_id: this.props.income.account.account_id,
        },
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.props.dispatch(routerRedux.push('/forum/income'));
          }, 1000);
        }
      });
    });
  }
  render() {
    const { income, form, common } = this.props;
    const { account, wallet } = income;
    const { getFieldDecorator } = form;
    const { initData } = common;
    const { loading, visible, txt } = this.state;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 8 },
    };
    const errMsg = <span style={{ color: 'red' }}>{account && account.account_info.err_msg}</span>;
    return (
      <PageHeaderLayout>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
        >
          <FormItem
            {...formItemLayout}
            label="可提现金额："
          >
            <span style={{ color: 'red' }}>{wallet.balance}元</span>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="选择提现账号："
            extra={errMsg}
          >
            { account && account.account_id ?
              <Row style={{ backgroundColor: '#2db7f5', color: '#fff' }}>
                <Col span={10} className={styles.textAlignCenter}>
                  支付宝帐号
                </Col>
                <Col span={10} className={styles.textAlignCenter}>
                  { account.account_info.real_name }
                </Col>
                <Col span={4} className={styles.textAlignCenter}>
                  <a
                    onClick={() => { this.setState({ visible: true, txt: '修改' }); }}
                    style={{ color: '#fff' }}
                  >
                    { account.account_info.err_code ? '更正' : '修改' }
                  </a>
                </Col>
              </Row> :
              <div>
                <span className="ant-form-text">
                  <a href="javascript:;" onClick={() => this.getUser()}>刷新</a>
                </span>
                <span className="ant-form-text">
                  <a
                    onClick={() => { this.setState({ visible: true, txt: '添加' }); }}
                  >
                    添加绑定提现账号
                  </a>
                </span>
              </div> }
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="提现金额："
          >
            {getFieldDecorator(
                'withdraw_amount',
                {
                  rules: [{
                    required: true,
                    message: '请填写提现金额',
                  },
                ],
                },
              )(<InputNumber
                placeholder="请输入金额"
                style={{ width: 200 }}
                min={1}
                max={wallet.balance}
                step="0.01"
              />)}
          </FormItem>
          <FormItem
            wrapperCol={{ span: 16, offset: 3 }}
          >
            <Button type="primary" htmlType="submit" loading={loading}>确认提现</Button>
          </FormItem>
        </Form>
        <Divider />
        <div className="mt-20">
          <h3><Icon type="exclamation-circle-o" className="mr-5" /> 特别说明</h3>
          <p>1.提现时间：工作日9：00 - 17：00 （每月1-10日支付系统例行对账+维护）</p>
          <p>2.到账时间：T+3 （双休日，法定节假日和系统维护期顺延）</p>
          <p>3.提现额度：每圈每日提现一次，提现额度1-2000元</p>
          <p>4.提现过程中请不要随意更改提现账号，若有未到账单请联系客服</p>
        </div>
        <Modal
          visible={visible}
          title={`${txt}支付宝帐号`}
          onCancel={() => { this.setState({ visible: false }); }}
          destroyOnClose
          footer={[
            <Button key="back" type="ghost" onClick={() => { this.setState({ visible: false }); }}>取消</Button>,
            <Button key="submit" type="primary" loading={loading} onClick={this.handleFixbind}>
              {txt}
            </Button>,
          ]}
        >
          <Form
            hideRequiredMark
            className="fixAlipay"
          >
            <FormItem
              label="手机"
              labelCol={{ span: 4 }}
              wrapperCol={{ span: 19, offset: 1 }}
            >
              {`${initData.user_info.username.substring(0, 3)}****${initData.user_info.username.substring(7)}`}
            </FormItem>
            <FormItem
              label="支付宝帐号"
              wrapperCol={{ span: 19, offset: 1 }}
              labelCol={{ span: 4 }}
            >
              {getFieldDecorator(
                'logonid',
                {
                  rules: [{
                    required: true,
                    message: '请填写支付宝帐号',
                  }],
                  initialValue: account && account.account_info.logonid,
                },
              )(<Input
                placeholder="请输入要绑定的支付宝帐号"
                style={{ width: 200 }}
              />)}
            </FormItem>
            <FormItem
              label="姓名"
              wrapperCol={{ span: 19, offset: 1 }}
              labelCol={{ span: 4 }}
            >
              {getFieldDecorator(
                'real_name',
                {
                  rules: [{
                    required: true,
                    message: '请填写身份证姓名',
                  }],
                  initialValue: account && account.account_info.real_name,
                },
              )(<Input
                placeholder="输入身份证姓名"
                style={{ width: 200 }}
              />)}
            </FormItem>
            <FormItem
              label="验证码"
              wrapperCol={{ span: 19, offset: 1 }}
              labelCol={{ span: 4 }}
            >
              {getFieldDecorator(
                'fix_sms_code',
                {
                  rules: [{
                    required: true,
                    message: '请输入短信验证码',
                  }],
                },
              )(<Input
                placeholder="请输入短信验证码"
                style={{ width: 200, marginRight: 15 }}
              />)}
              <SMSButton dispatch={this.props.dispatch} phone={initData.user_info.username} />
            </FormItem>
          </Form>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
