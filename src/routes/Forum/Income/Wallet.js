import React, { PureComponent } from 'react';
import { Row, Col } from 'antd';
import { Link } from 'dva/router';
import styles from './Income.less';

export default class Wallet extends PureComponent {
  render() {
    const { common, income } = this.props;
    const { wallet } = income;
    return (
      <div className={styles.profile_header}>
        <Row>
          <Col lg={8} sm={24} xs={24} className={styles.item}>
            <Row>
              <Col span="10">
                <img src={common.forum_info.logo} alt="" />
              </Col>
              <Col span="14">
                <h2 className={styles.title}>{common.forum_info.name}</h2>
              </Col>
            </Row>
          </Col>
          <Col lg={8} sm={24} xs={24} className={styles.item}>
            <Row className={styles.subtitle}>
              <Col span="12">7天收入</Col>
              <Col span="12" className="text-right">
                <Link to="/forum/income/record">提现记录</Link>
              </Col>
            </Row>
            <p className={styles.cash}>
              <span className={styles.number}>{wallet.seven_day_revenue || 0}</span> 元
            </p>
          </Col>
          <Col lg={8} sm={24} xs={24} className={styles.item}>
            <Row className={styles.subtitle}>
              <Col span="12">余额</Col>
              <Col span="12" className="text-right">
                <Link to="/forum/income/withdraw">提现</Link>
              </Col>
            </Row>
            <p className={styles.cash}>
              <span className={styles.number}>{wallet.balance || 0}</span> 元
            </p>
          </Col>
        </Row>
      </div>
    );
  }
}
