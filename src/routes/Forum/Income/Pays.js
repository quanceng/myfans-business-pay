import React, { PureComponent } from 'react';
import { Table, Button, Form } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import DatePicker from '../../../components/DatePicker';

const FormItem = Form.Item;
export default class Pays extends PureComponent {
  state = {
    startDate: null,
    endDate: null,
  }
  // 保存筛选时间
  changeDate = (startDate, endDate) => {
    this.setState({
      startDate,
      endDate,
    });
  };
  // 筛选流水列表
  handleSubmit = (e) => {
    e.preventDefault();
    const { startDate, endDate } = this.state;
    this.props.handleSubmit(startDate, endDate);
  }
  // 列表翻页
  handleTableChange = (pagination) => {
    this.props.handleTableChange(pagination);
  }
  // 加载付费入圈流水数据
  payInColumns = () => {
    return [
      {
        title: '流水号',
        width: 380,
        dataIndex: 'order_no',
      },
      {
        title: '付款时间',
        dataIndex: 'created_at',
      },
      {
        title: '付款人',
        render: (text, record) => {
          return (
            <Ellipsis length={9} tooltip>{record.nickname}</Ellipsis>
          );
        },
      },
      {
        title: '分销人',
        render: (text, record) => {
          return (
            <Ellipsis length={9} tooltip>{record.distributor_nickname}</Ellipsis>
          );
        },
      },
      {
        title: '提成（元）',
        dataIndex: 'distributor_amount',
      },
      {
        title: '金额(元)',
        dataIndex: 'forum_amount',
      },
    ];
  }
  render() {
    const { walletList, walletPage, loading } = this.props;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            <DatePicker
              changeDate={this.changeDate}
            />
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.order_no}
          className="mt-20"
          dataSource={walletList}
          pagination={walletPage}
          loading={loading}
          columns={this.payInColumns()}
          onChange={this.props.handleTableChange}
        />
      </div>
    );
  }
}
