import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Table, Form, Tag } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

@connect(state => ({
  income: state.income,
}))
@Form.create()
export default class Record extends PureComponent {
  state = {
    loading: false,
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'income/getRecord',
    });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'income/getRecord',
      payload: {
        page: pagination.current,
      },
    }).then(() => {
      this.setState({
        loading: false,
      });
    });
  }
  // 加载流水数据
  columns = () => {
    return [
      {
        title: '提现单号',
        dataIndex: 'trans_no',
        render: (text, record) => {
          return (
            <Ellipsis length={13} tooltip>{record.trans_no}</Ellipsis>
          );
        },
      },
      {
        title: '提现时间',
        dataIndex: 'created_at',
      },
      {
        title: '提现金额',
        dataIndex: 'withdraw_amount',
      },
      {
        title: '提现方式',
        render: (text, record) => {
          return (
            <span>{ record.withdraw_method === 'alipay' ? '支付宝' : '微信'}</span>
          );
        },
      },
      {
        title: '提现状态',
        render: (text, record) => {
          return (
            <span>{ record.withdraw_status === 11005 ? <Tag color="green">已到帐</Tag> : record.withdraw_status === 11004 ? <Tag color="red">已撤回</Tag> : <Tag color="blue">正在打款</Tag>}</span>);
        },
      },
      {
        title: '外部单号',
        render: (text, record) => {
          return (
            <Ellipsis length={13} tooltip>{record.gw_txn_no}</Ellipsis>
          );
        },
      },
      {
        title: '到账时间',
        render: (text, record) => {
          return (
            <span>{ record.paid_at ? record.paid_at : '暂未到账'}</span>
          );
        },
      },
      {
        title: '到账帐号',
        render: (text, record) => {
          return (
            <Ellipsis length={5} tooltip>{ !record.account_snapshot ? '' : record.account_snapshot.real_name ? record.account_snapshot.real_name : record.account_snapshot.nickname }</Ellipsis>
          );
        },
      },
    ];
  }
  render() {
    const { recordPage, recordList } = this.props.income;
    const { loading } = this.state;
    return (
      <PageHeaderLayout>
        <Table
          rowKey={record => record.withdraw_id}
          className="mt-20"
          dataSource={recordList}
          pagination={recordPage}
          loading={loading}
          columns={this.columns()}
          onChange={this.handleTableChange}
        />
      </PageHeaderLayout>
    );
  }
}
