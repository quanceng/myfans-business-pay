import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { message } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import Wallet from './Wallet';
import Pays from './Pays';

const tabList = [
  {
    key: 'pay',
    tab: '付费入圈',
  },
];
@connect(state => ({
  income: state.income,
  common: state.common,
}))
export default class Income extends PureComponent {
  state = {
    tab: 'pay',
    loading: false,
    walletData: {
      type: 5,
      page: 1,
    },
    tabKey: 1,
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'income/getWalletList',
      payload: this.state.walletData,
    });
    this.props.dispatch({
      type: 'income/getMoney',
    });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    const walletData = {
      ...this.state.walletData,
    };
    walletData.page = pagination.current;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'income/getWalletList',
      payload: walletData,
    }).then(() => {
      this.setState({
        loading: false,
        walletData,
      });
    });
  }
  // 筛选流水列表
  handleSubmit = (startDate, endDate) => {
    const walletData = {
      ...this.state.walletData,
    };
    if (startDate && endDate) {
      walletData.page = 1;
      walletData.start_time = startDate;
      walletData.end_time = endDate;
      this.setState({ loading: true });
      this.props.dispatch({
        type: 'income/getWalletList',
        payload: walletData,
      }).then(() => {
        this.setState({
          loading: false,
          walletData,
        });
      });
    } else {
      message.info('请选择时间范围');
    }
  }
  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      pay: Pays,
    };
    return componentMap[this.state.tab];
  }
  // 标签切换
  handleTabChange = (key) => {
    this.setState({ loading: true, tab: key });
    const typeKeys = {
      pay: 5,
    };
    const walletData = {
      type: typeKeys[key],
      page: 1,
    };
    this.props.dispatch({
      type: 'income/getWalletList',
      payload: walletData,
    }).then(() => {
      this.setState({
        walletData,
        tabKey: typeKeys[key],
        loading: false,
      });
    });
  }
  render() {
    const { income, common } = this.props;
    const { walletList, walletPage } = this.props.income;
    const { loading, tabKey } = this.state;
    // 头部钱包数据
    const walletContent = <Wallet income={income} common={common.initData} />;
    const CurrentComponent = this.getCurrentComponent();
    return (
      <PageHeaderLayout
        tabList={tabList}
        content={walletContent}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          walletList={walletList}
          walletPage={walletPage}
          loading={loading}
          handleTableChange={this.handleTableChange}
          handleSubmit={this.handleSubmit}
          tabKey={tabKey}
        />
      </PageHeaderLayout>
    );
  }
}
