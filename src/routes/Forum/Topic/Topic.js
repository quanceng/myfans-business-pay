import React, { PureComponent } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { connect } from 'dva';
import { Table, Button, Form, Input, Icon, Switch, Divider, Modal, message } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from './Topic.less';

const FormItem = Form.Item;
@connect(state => ({
  topic: state.topic,
  user: state.user,
  common: state.common,
}))
@Form.create()
export default class Topic extends PureComponent {
  state = {
    visible: false,
    isCreate: 1,
    topicName: '',
    isAdmin: false,
    topicId: null,
    searchName: '',
    userId: 0,
    seaUserList: [],
    seaPage: {},
    isLink: false,
    link: '',
  }
  componentDidMount() {
    this.fetch();
    this.props.dispatch({
      type: 'topic/fetchTopicDisplay',
    });
  }
  fetch = () => {
    this.props.dispatch({
      type: 'topic/fetchTopic',
    });
  };
  // 显示话题弹窗
  showConfirm = (status, topicName, isAdmin, topicId, userId, nickname, link) => {
    this.setState({
      visible: true,
      isCreate: status,
      topicName,
      isAdmin: isAdmin ? true : false,
      topicId,
      userId,
      link,
      isLink: link ? true : false,
      searchName: nickname,
    });
  };
  // 隐藏话题弹窗
  handleCancel = () => {
    this.setState({
      visible: false,
      userId: 0,
      searchName: '',
      seaUserList: [],
    });
  };
  // 设置是否发帖默认选择话题
  changeForce = (checked) => {
    this.props.dispatch({
      type: 'topic/updateTopicDisplay',
      payload: {
        is_force: checked ? 1 : 0,
      },
    });
  };
  // 设置是否发帖强制选择话题
  changeUserForceTopic = (checked) => {
    this.props.dispatch({
      type: 'topic/updateTopicDisplay',
      payload: {
        force_user_classify: checked ? 1 : 0,
      },
    });
  };
  // 设置是否仅允许管理员发帖
  changeAdmin = (checked) => {
    this.setState({ isAdmin: checked });
  };
  // 设置是否开启话题链接
  isOpenLink = (checked) => {
    this.setState({ isLink: checked });
  };
  // 保存话题名称
  changeName = (e) => {
    this.setState({ topicName: e.target.value });
  }
  // 保存话题链接
  changeLink = (e) => {
    this.setState({ link: e.target.value });
  }
  // 保存搜索用户名
  searchChange = (e) => {
    this.setState({
      searchName: e.target.value,
    });
  }
  // 搜索用户
  searchUser = () => {
    const { searchName } = this.state;
    if (!searchName) {
      message.info('请输入搜索的用户名');
      return;
    }
    this.props.dispatch({
      type: 'user/searchUserList',
      payload: {
        nickname: searchName,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          seaUserList: response.data.data,
          seaPage: {
            current: response.data.current_page,
            total: response.data.total,
            pageSize: 10,
          },
        });
      }
    });
  }
  // 选中用户
  addTopicAdmin = (userId, userName) => {
    this.setState({
      userId,
      searchName: userName,
    });
    message.success(`已选择用户${userName}`);
  };
  // 添加与编辑话题请求
  handleUpdateTopic = () => {
    const { topicId, isAdmin, isCreate, topicName, userId, link, isLink } = this.state;
    if (!topicName) {
      message.info('请输入话题名称');
      return;
    }
    if (isLink && !link) {
      message.info('请输入话题链接');
      return;
    }
    const data = {
      topic_name: topicName,
      is_admin: isAdmin ? 1 : 0,
    };
    if (isCreate) {
      if (link) {
        data.link = link;
      }
      this.props.dispatch({
        type: 'topic/fetchEditTopic',
        payload: {
          ...data,
          admin_id: userId,
        },
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.setState({ visible: false, topicName: '', isAdmin: false, userId: 0, seaUserList: [], seaPage: {}, isLink: false, link: '' });
            this.fetch();
          }, 1000);
        }
      });
    } else {
      if (isLink) {
        data.link = link;
      }
      this.props.dispatch({
        type: 'topic/fetchEditTopic',
        payload: {
          topic_id: topicId,
          user_id: userId,
          ...data,
        },
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.setState({ visible: false, topicName: '', isAdmin: false, userId: 0, seaUserList: [], seaPage: {}, isLink: false, link: '' });
            this.fetch();
          }, 1000);
        }
      });
    }
  };
  // 删除话题
  deleteTopics = (topicId) => {
    this.props.dispatch({
      type: 'topic/deleteTopic',
      payload: topicId,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 话题排序
  sortTopic = (firstId, secondId) => {
    this.props.dispatch({
      type: 'topic/sortTopic',
      payload: {
        topic_first: firstId,
        topic_second: secondId,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.fetch();
        }, 1000);
      }
    });
  };
  // 用户表格翻页
  searchTableChange = (pagination) => {
    this.props.dispatch({
      type: 'user/searchUserList',
      payload: {
        page: pagination.current,
        nickname: this.state.searchName,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.setState({
          seaUserList: response.data.data,
          seaPage: {
            current: response.data.current_page,
            total: response.data.total,
            pageSize: 10,
          },
        });
      }
    });
  }
  // 加载话题列表
  columns = () => {
    return [
      {
        title: '编号',
        dataIndex: 'topic_id',
        render: (text, record, index) => {
          return index + 1;
        },
      },
      {
        title: '话题',
        dataIndex: 'topic_name',
      },
      {
        title: '管理员',
        dataIndex: 'username',
      },
      {
        title: '排序',
        render: (text, record, index) => {
          const { allTopics } = this.props.topic;
          return (
            <div>
              {
                index === 0 ? null :
                <Button
                  type="primary"
                  size="small"
                  icon="arrow-up"
                  style={{ marginRight: 5 }}
                  onClick={this.sortTopic.bind(this, allTopics[index - 1].topic_id, record.topic_id)}
                />
              }
              {
                index === (allTopics.length - 1) && index === 0 ?
                  <Icon type="minus" /> : null
              }
              {
                index === (allTopics.length - 1) ? null :
                <Button
                  type="primary"
                  size="small"
                  icon="arrow-down"
                  onClick={this.sortTopic.bind(this, record.topic_id, allTopics[index + 1].topic_id)}
                />
              }
            </div>
          );
        },
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <span>
              <a
                href="javascript:;"
                onClick={this.deleteTopics.bind(this, record.topic_id)}
              >
                删除
              </a>
              <Divider type="vertical" />
              <a
                href="javascript:;"
                onClick={this.showConfirm.bind(this, 0, record.topic_name, record.is_admin, record.topic_id, record.admin_id, record.username, record.link, record.is_post_audit)}
              >
                编辑
              </a>
              {record.link ? '' :
              <span>
                <Divider type="vertical" />
                <CopyToClipboard
                  text={`${this.props.common.initData.forum_info.forum_host}/c/${this.props.common.initData.forum_info.forum_code}/share?url=topicId/${record.topic_id}`}
                  onCopy={() => { message.success('复制成功'); }}
                >
                  <a>复制话题链接</a>
                </CopyToClipboard>
              </span>
              }
            </span>
          );
        },
      },
    ];
  };
  // 加载搜索后的用户列表
  searchColumns = () => {
    return [
      {
        title: '用户名',
        dataIndex: 'author',
        render: (text, record) => (
          <span className={styles.userLogo}>
            <img
              alt="avatar"
              className={styles.tableAvatar}
              src={record.avatar}
            />
            &nbsp;
            {record.nickname}
            &nbsp;
          </span>
        ),
      },
      {
        title: '操作',
        width: 100,
        render: (text, record) => (
          <a
            onClick={this.addTopicAdmin.bind(this, record.user_id, record.nickname)}
            href="javascript:;"
          >
            添加
          </a>
        ),
      },
    ];
  }
  render() {
    const { visible, isCreate, isAdmin, topicName, searchName, seaUserList, seaPage, isLink, link } = this.state;
    const { allTopics, topicDisplay } = this.props.topic;
    return (
      <PageHeaderLayout>
        <Form
          hideRequiredMark
        >
          <FormItem
            label="默认话题"
            extra="手机社区内某一话题下点击发帖，发帖页面默认选择该话题"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 20, offset: 1 }}
          >
            <Switch checkedChildren="开" unCheckedChildren="关" checked={!!topicDisplay.is_force} onChange={this.changeForce} />
          </FormItem>
          <FormItem
            label="强制话题"
            extra="手机社区内点击发帖，发帖页面必须强制选择一个话题才能发帖"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 20, offset: 1 }}
          >
            <Switch checkedChildren="开" unCheckedChildren="关" checked={!!topicDisplay.force_user_classify} onChange={this.changeUserForceTopic} />
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              onClick={this.showConfirm.bind(this, 1, '', 0, null, 0, '', '', 0)}
            >
              添加话题
            </Button>
          </FormItem>
          <FormItem>
            <Table
              rowKey={record => record.topic_id}
              columns={this.columns()}
              dataSource={allTopics}
              pagination={false}
            />
          </FormItem>
        </Form>
        <Modal
          title={isCreate ? '添加话题' : '编辑话题'}
          visible={visible}
          onOk={this.handleUpdateTopic}
          onCancel={this.handleCancel}
        >
          <Form>
            <FormItem
              label="是否设置话题链接"
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 3, offset: 1 }}
            >
              <Switch checkedChildren="是" unCheckedChildren="否" checked={isLink} onChange={this.isOpenLink} />
            </FormItem>
            {!isLink ?
              <FormItem
                label="仅管理员发帖"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 3, offset: 1 }}
              >
                <Switch checkedChildren="开" unCheckedChildren="关" checked={isAdmin} onChange={this.changeAdmin} />
              </FormItem>
            : ''}
            <FormItem
              label="话题名称"
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 8, offset: 1 }}
            >
              <Input
                placeholder="请输入话题名称"
                value={topicName}
                onChange={this.changeName}
              />
            </FormItem>
            {isLink ?
              <FormItem
                label="话题链接"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 8, offset: 1 }}
              >
                <Input
                  placeholder="请输入话题链接"
                  value={link}
                  onChange={this.changeLink}
                />
              </FormItem> : ''
            }
            {!isLink ?
              <FormItem
                label="话题管理员"
                extra="话题管理员只能管理当前话题下的微贴"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 16, offset: 1 }}
              >
                <Input
                  style={{ width: 158 }}
                  placeholder="搜索用户名称"
                  value={searchName}
                  onChange={this.searchChange}
                  onFocus={() => this.setState({ userId: 0, searchName: '' })}
                />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button type="primary" onClick={this.searchUser}>搜索</Button>
              </FormItem>
              : ''}
            <div style={{ textAlign: 'center', marginTop: 15, borderRadius: 3 }}>
              {
                seaUserList.length > 0 ?
                  <Table
                    style={{ width: 350, margin: '0 auto' }}
                    rowKey={record => record.user_id}
                    columns={this.searchColumns()}
                    pagination={seaPage}
                    dataSource={seaUserList}
                    onChange={this.searchTableChange}
                  /> : ''
              }
            </div>
          </Form>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
