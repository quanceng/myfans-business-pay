import React, { PureComponent } from 'react';
import { Link } from 'dva/router';
import { Table, Button, Icon } from 'antd';

export default class VestList extends PureComponent {
  state = {
    loading: false,
  }
  componentDidMount() {
    this.getIndex();
  }
  getIndex = (params) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'vest/fetchVest',
      payload: params,
    }).then(() => {
      this.setState({
        loading: false,
      });
    });
  };
  // 翻页
  handleChangePage = (page) => {
    this.getIndex({
      page,
    });
  }
  // 公告列表加载
  columns = () => {
    return [
      {
        title: '马甲ID',
        dataIndex: 'username',
      },
      {
        title: '马甲昵称',
        dataIndex: 'user_info.nickname',
      },
      {
        title: '创建时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <Link to={`/forum/vest/${record.user_id}/edit`}>编辑</Link>);
        },
      },
    ];
  };
  render() {
    const { vests, vestPage } = this.props.vest;
    const { loading } = this.state;
    return (
      <div>
        <div className="mt-20">
          <h3><Icon type="exclamation-circle-o" className="mr-5" /> 注意事项：</h3>
          <p className="mt-20">1.马甲用户为虚拟用户，不能进行支付操作。</p>
          <p>2.马甲身份只能在当前圈内使用，不能使用马甲进入其他圈子。</p>
          <p>3.可在手机端设置页面切换马甲身份和自己的微信号。</p>
        </div>
        <Button type="primary">
          <Link to="/forum/vest/create">新建马甲</Link>
        </Button>
        <Table
          rowKey={record => record.id}
          className="mt-20"
          dataSource={vests}
          pagination={{
            total: vestPage.total,
            pageSize: vestPage.pageSize,
            current: vestPage.current,
            onChange: this.handleChangePage,
          }}
          columns={this.columns()}
          loading={loading}
        />
      </div>
    );
  }
}
