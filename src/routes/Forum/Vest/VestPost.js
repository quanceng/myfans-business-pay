import React, { PureComponent } from 'react';
import { Form, Button, Input, Upload, message, Icon, Select } from 'antd';
import { selectData } from '../../../utils/utils';
import './Vest.less';

const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
@Form.create()

export default class VestPost extends PureComponent {
  state = {
    fileList: [],
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'vest/fetchVest',
      payload: { is_all: 1 },
    });
  }
  // 上传图片
  handleChange = (res) => {
    // 上传显示
    this.setState({
      fileList: res.fileList.slice(-9),
    });

    // 上传状态判断
    switch (res.file.status) {
      case 'error':
        message.error('上传图片失败');
        break;
      case 'done':
        message.success('上传图片成功');
        break;
      default:
        break;
    }
  }
  // 新建微帖
  handleSubmit = (e) => {
    e.preventDefault();
    const { fileList } = this.state;
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = {
        ...values,
      };
      if (fileList.length) {
        data.attachment_list = selectData(fileList, 'response');
        data.type = 2;
        data.message = data.message ? data.message : '分享图片';
      } else if (data.message) {
        data.type = 0;
      } else {
        message.info('请完善微帖内容！');
        return;
      }
      this.props.dispatch({
        type: 'post/createPost',
        payload: data,
      }).then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            this.props.form.resetFields();
            this.props.form.setFieldsValue({
              topic_id: 0,
            });
            this.setState({ fileList: [] });
          }, 1000);
        }
      });
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { vests } = this.props.vest;
    const { topics } = this.props.topic;
    const { fileList } = this.state;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 16 },
    };
    const tailFormItemLayout = {
      wrapperCol: { span: 16, offset: 3 },
    };
    const uploadProps = {
      name: 'image',
      action: '/b/upload-image',
      accept: 'image/png,image/jpg,image/jpeg,imge/bmp,image/gif',
      listType: 'picture',
      multiple: true,
      beforeUpload(file) {
        const isPic = file.type === 'image/jpeg' || file.type === 'image/png';
        const limitedSize = file.size < 1048576;
        if (!isPic) {
          message.error('非法的图片格式，请重新选择');
          return false;
        }
        if (!limitedSize) {
          message.error('图片体积过大，请重新选择');
          return false;
        }
        return isPic && limitedSize;
      },
      onChange: this.handleChange,
    };
    return (
      <Form
        onSubmit={this.handleSubmit}
      >
        <FormItem
          {...formItemLayout}
          label="选择马甲"
          required
        >
          {getFieldDecorator('author_id', {
            rules: [
              { required: true, message: '请选择马甲' },
            ],
          })(
            <Select placeholder="请选择马甲">
              { vests.map((item) => {
                return (
                  <Option
                    key={item.user_id}
                    value={item.user_id}
                  >
                    {item.user_info.nickname}
                  </Option>
                );
                })
              }
            </Select>
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="选择话题"
          required
        >
          {getFieldDecorator(
            'topic_id',
            {
              initialValue: 0,
            },
          )(
            <Select>
              <Option key="topicAll" value={0}>
                全部话题
              </Option>
              { topics.map((item) => {
                  return (
                    <Option
                      key={`topic_id_${item.topic_id}`}
                      value={item.topic_id}
                    >
                      {item.topic_name}
                    </Option>
                  );
                })
              }
            </Select>)}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="微帖图片"
        >
          <Upload {...uploadProps} fileList={fileList}>
            <Button>
              <Icon type="upload" /> 上传图片
            </Button>
          </Upload>
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="微帖内容"
        >
          {getFieldDecorator('message')(
            <TextArea placeholder="请输入微帖内容" rows={4} />
          )}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={!vests.length}
          >
            保存添加
          </Button>
        </FormItem>
      </Form>
    );
  }
}
