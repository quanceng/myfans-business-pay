import React, { PureComponent } from 'react';
import { Button, Input, Form } from 'antd';
import UploadCover from '../../../components/UploadCover';

const FormItem = Form.Item;

@Form.create()
export default class VestForm extends PureComponent {
  // 提交
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = {
        ...values,
      };
      if (data.password === '******') {
        delete data.password;
      }
      if (data.avatar.substring(0, 22) === 'https://cdn.myfans.cc/') {
        delete data.avatar;
      }
      this.props.handleSubmit(data);
    });
  }

  // 上传图片
  handleUploadImage = (avatar) => {
    this.props.form.setFieldsValue({ avatar });
  }

  // 删除上传的图片
  handleRemoveImage = () => {
    this.props.form.setFieldsValue({ avatar: '' });
  }
  render() {
    const { data } = this.props;
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    return (
      <Form
        onSubmit={this.handleSubmit}
      >
        {data ?
          <FormItem
            label="马甲ID"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            <Input
              value={data.username}
              disabled
            />
          </FormItem> :
        ''}
        <FormItem
          label="马甲昵称"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 6 }}
        >
          {getFieldDecorator(
            'nickname',
            {
              rules: [{
                required: true,
                max: 10,
                message: '马甲昵称最多十个字符',
              }],
              initialValue: data && data.user_info.nickname,
            },
          )(<Input
            placeholder="填写马甲昵称"
          />)}
        </FormItem>
        <FormItem
          label="马甲登录密码:"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 6 }}
        >
          {getFieldDecorator(
            'password',
            {
              rules: [{
                required: true,
                len: 6,
                message: '马甲登录密码为六个字符',
              }],
              initialValue: data ? '******' : '',
            },
          )(<Input
            placeholder="请填写马甲登录密码"
            onFocus={() => setFieldsValue({ password: '' })}
          />)}
        </FormItem>
        <FormItem
          label="马甲logo"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 20 }}
        >
          {getFieldDecorator('avatar', {
            initialValue: data && data.user_info.avatar,
            rules: [{
              required: true,
              message: '请上传马甲logo',
            }],
          })(
            <Input hidden />
          )}
          <UploadCover
            changeImg={this.handleUploadImage}
            removeImg={this.handleRemoveImage}
            filename={data && data.user_info.avatar}
            card={1}
          />
        </FormItem>
        <FormItem
          wrapperCol={{ span: 20, offset: 3 }}
        >
          <Button type="primary" htmlType="submit">保存添加</Button>
        </FormItem>
      </Form>
    );
  }
}
