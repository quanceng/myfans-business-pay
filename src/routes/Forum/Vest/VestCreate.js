import React, { PureComponent } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import VestForm from './VestForm';
@connect(state => ({
  vest: state.vest,
}))
export default class VestCreate extends PureComponent {
  // 创建马甲
  handleSubmit = (data) => {
    this.props.dispatch({
      type: 'vest/fetchAddVest',
      payload: data,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/vest'));
        }, 1000);
      }
    });
  };
  render() {
    return (
      <PageHeaderLayout>
        <VestForm handleSubmit={this.handleSubmit} />
      </PageHeaderLayout>
    );
  }
}
