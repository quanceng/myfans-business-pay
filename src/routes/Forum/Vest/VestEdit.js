import React, { PureComponent } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import VestForm from './VestForm';
@connect(state => ({
  vest: state.vest,
}))
export default class VestEdit extends PureComponent {
  state = {
    vestInfo: {},
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'vest/vestDetail',
      payload: this.props.match.params.id,
    }).then(() => {
      this.setState({
        vestInfo: this.props.vest,
      });
    });
  }
  // 修改马甲
  handleSubmit = (data) => {
    this.props.dispatch({
      type: 'vest/fetchEditVest',
      payload: {
        vestId: this.props.match.params.id,
        data,
      },
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/vest'));
        }, 1000);
      }
    });
  };
  render() {
    const { vestInfo } = this.state.vestInfo;
    return (
      <PageHeaderLayout>
        {vestInfo && <VestForm handleSubmit={this.handleSubmit} data={vestInfo} />}
      </PageHeaderLayout>
    );
  }
}
