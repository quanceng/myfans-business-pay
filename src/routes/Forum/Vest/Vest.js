import React, { PureComponent } from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import VestList from './VestList';
import VestPost from './VestPost';

const tabList = [
  {
    key: 'list',
    tab: '马甲列表',
  },
  {
    key: 'post',
    tab: '马甲发帖',
  },
];
@connect(state => ({
  vest: state.vest,
  post: state.post,
  topic: state.topic,
}))
export default class Vest extends PureComponent {
  state = {
    tab: 'list',
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'topic/fetchTopic',
    });
  }
  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      list: VestList,
      post: VestPost,
    };
    return componentMap[this.state.tab];
  }
  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }
  render() {
    const CurrentComponent = this.getCurrentComponent();// 头部筛选数据
    const { vest, dispatch, topic } = this.props;
    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent
          vest={vest}
          dispatch={dispatch}
          topic={topic}
        />
      </PageHeaderLayout>
    );
  }
}
