import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import NoticeForm from './NoticeForm';
@connect(state => ({
  notice: state.notice,
}))
export default class NoticeCreate extends PureComponent {
  // 创建公告
  handleSubmit = (value) => {
    this.props.dispatch({
      type: 'notice/editNotice',
      payload: value,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/notice'));
        }, 1000);
      }
    });
  };
  render() {
    return (
      <PageHeaderLayout>
        <NoticeForm handleSubmit={this.handleSubmit} />
      </PageHeaderLayout>
    );
  }
}
