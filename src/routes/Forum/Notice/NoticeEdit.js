import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import NoticeForm from './NoticeForm';

@connect(state => ({
  notice: state.notice,
}))
export default class NoticeEdit extends PureComponent {
  componentDidMount() {
    this.getNoticeDetail();
  }
  getNoticeDetail = () => {
    this.props.dispatch({
      type: 'notice/noticeDetail',
      payload: this.props.match.params.id,
    });
  }
  // 编辑公告
  handleSubmit = (data) => {
    this.props.dispatch({
      type: 'notice/editNotice',
      payload: {
        id: this.props.match.params.id,
        ...data,
      },
    }).then((response) => {
      if (response.code === 1) {
        this.getNoticeDetail();
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/forum/notice'));
        }, 1000);
      }
    });
  };
  render() {
    const { noticeInfo } = this.props.notice;
    return (
      <PageHeaderLayout>
        {noticeInfo && <NoticeForm handleSubmit={this.handleSubmit} data={noticeInfo} />}
      </PageHeaderLayout>
    );
  }
}
