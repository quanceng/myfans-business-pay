import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Table, Button, Popconfirm, Alert, Divider } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

@connect(state => ({
  notice: state.notice,
}))
export default class Blank extends PureComponent {
  state = {
    loading: true,
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'notice/noticeList',
    }).then(() => {
      this.setState({
        loading: false,
      });
    });
  }
  // 删除公告
  deleteNotice = (id) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'notice/removeNotice',
      payload: id,
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch({
            type: 'notice/noticeList',
          }).then(() => {
            this.setState({
              loading: false,
            });
          });
        }, 1000);
      }
    });
  };
  // 表格翻页
  handleTableChange = (pagination) => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'notice/noticeList',
      payload: {
        page: pagination.page,
      },
    }).then(() => {
      this.setState({
        loading: false,
      });
    });
  }
  // 公告列表加载
  columns = () => {
    return [
      {
        title: '标题',
        dataIndex: 'title',
      },
      {
        title: '时间',
        dataIndex: 'created_at',
      },
      {
        title: '操作',
        render: (text, record) => {
          return (
            <span>
              <Link to={`/forum/notice/${record.id}/edit`}>编辑</Link>
              <Divider type="vertical" />
              <Popconfirm
                title="确定要删除这个公告吗？"
                onConfirm={this.deleteNotice.bind(this, record.id)}
              >
                <a href="javascript:;">删除</a>
              </Popconfirm>
            </span>);
        },
      },
    ];
  };
  render() {
    const { noticeList, noticePage } = this.props.notice;
    const { loading } = this.state;
    return (
      <PageHeaderLayout>
        <Button type="primary">
          <Link to="/forum/notice/create">新建公告</Link>
        </Button>
        <Alert className="mt-20" message="公告只显示最新的一条，不支持多条轮播" type="info" showIcon />
        <Table
          rowKey={record => record.id}
          dataSource={noticeList}
          pagination={noticePage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
      </PageHeaderLayout>
    );
  }
}
