import React, { PureComponent } from 'react';
import { Button, Input, Form, message } from 'antd';
import Editor from '../../../components/Editor/EditorSimple';

const FormItem = Form.Item;

@Form.create()
export default class NoticeForm extends PureComponent {
  state = {
    subject: this.props.data && this.props.data.content,
  }
  // 提交
  handleSubmit = (e) => {
    e.preventDefault();
    const { subject } = this.state;
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!subject || subject === '<p><br></p>') {
        message.info('请勿提交空内容');
        return;
      }
      const data = {
        ...values,
        content: subject,
      };
      this.props.handleSubmit(data);
    });
  }

  // 保存富文本编辑器内容
  changeEditContent = (value) => {
    this.setState({ subject: value });
  };
  render() {
    const { data } = this.props;
    const { subject } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <FormItem
          label="公告标题:"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 6 }}
        >
          {getFieldDecorator(
            'title',
            {
              rules: [{
                required: true,
                message: '请填写公告标题',
              }],
              initialValue: data && data.title,
            },
          )(<Input
            placeholder="填写公告标题"
          />)}
        </FormItem>
        <FormItem
          label="公告内容"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 20 }}
          required
        >
          <Editor onChange={this.changeEditContent} initContent={subject} />
        </FormItem>
        <FormItem
          wrapperCol={{ span: 20, offset: 3 }}
        >
          <Button type="primary" onClick={this.handleSubmit}>保存添加</Button>
        </FormItem>
      </Form>
    );
  }
}
