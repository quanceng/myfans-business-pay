import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Row, Col, Card, Avatar, Button } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import Curved from '../../components/Echart/Curved';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './Summary.less';

@connect(state => ({
  summary: state.summary,
  common: state.common,
}))
export default class Summary extends PureComponent {
  state = {
    loading: false,
  }

  componentDidMount() {
    this.getSummaryForum();
  }

  componentWillUnmount() {
    this.props.dispatch({
      type: 'summary/clearDateList',
    });
  }

  // 获取粉丝圈概况数据
  getSummaryForum = () => {
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'summary/forum',
    }).then(() => {
      this.setState({ loading: false });
    });
  }

  // 表格数据
  getChartData = (dateList, dataList, fieldName) => {
    const data = [];
    dateList.forEach((item, key) => {
      data.push(dataList[key] || 0);
    });

    return {
      xAxisData: dateList,
      seriesData: [
        { name: fieldName, data, areaStyle: { normal: {} } },
      ],
    };
  }

  render() {
    const { loading } = this.state;
    const forumCommon = this.props.common.initData;
    const { summary } = this.props;
    const { forum } = summary;
    const time = new Date().getHours();
    let greetings = '';
    if (time < 5) {
      greetings = '凌晨';
    } else if (time >= 5 && time < 7) {
      greetings = '清晨';
    } else if (time >= 7 && time < 9) {
      greetings = '早上';
    } else if (time >= 9 && time < 12) {
      greetings = '上午';
    } else if (time >= 12 && time < 14) {
      greetings = '中午';
    } else if (time >= 14 && time < 18) {
      greetings = '下午';
    } else if (time >= 18 && time < 24) {
      greetings = '晚上';
    }
    // 内容头部
    const pageHeaderContent = (
      <div className={styles.pageHeaderContent}>
        <div className={styles.avatar}>
          <Avatar src={forumCommon.forum_info.logo} />
        </div>
        <div className={styles.content}>
          <div className={styles.contentTitle}>
            <Ellipsis length={24} tooltip>{`${greetings}好，${forumCommon.forum_info.name}，祝您开心每一天！`}
            </Ellipsis>
          </div>
          <div>粉丝圈付费版{ forumCommon.pay_info.is_pay ? '' : '（试用）'} | 截止：<b>{forumCommon.pay_info.ended_at}</b></div>
          {
            forumCommon.pay_info.end_status === 2 ?
              <div>
                距离到期还有<span className="red">{forumCommon.pay_info.end_day}</span>天，请及时续费以免影响正常使用。
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button type="primary">
                  <Link to="/dashboard/pay">
                    立即续费
                  </Link>
                </Button>
              </div>
              : forumCommon.pay_info.end_status === 0 ?
                <div>
                  <span className={styles.danger}>
                    已过期，请及时续费以免影响正常使用。
                  </span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Button type="primary">
                    <Link to="/dashboard/pay">
                      立即续费
                    </Link>
                  </Button>
                </div>
              : ''
          }
        </div>
      </div>
    );

    // 内容头部扩展
    const pageHeaderExtra = (
      <div className={styles.pageHeaderExtra}>
        <div>
          <p>昨日新增微帖</p>
          <p>{forum.post_total}</p>
        </div>
        <div>
          <p>昨日活跃用户</p>
          <p>{forum.visit_yesterday}</p>
        </div>
        <div>
          <p>总用户</p>
          <p>{forum.forum_member_total}</p>
        </div>
      </div>
    );

    // 趋势图
    const postChartData = this.getChartData(forum.date_list, forum.post_list, '微帖数');
    const memberChartData = this.getChartData(forum.date_list, forum.visit_list, '活跃用户数');

    return (
      <PageHeaderLayout
        content={pageHeaderContent}
        extraContent={pageHeaderExtra}
      >
        <Row gutter={24}>
          <Col span={24}>
            <Card
              loading={loading}
            >
              <Curved
                chartData={postChartData}
                title="微帖数趋势"
                height="200px"
              />
            </Card>
            <Card
              className="mt-20"
              loading={loading}
            >
              <Curved
                chartData={memberChartData}
                title="活跃用户数趋势"
                height="200px"
              />
            </Card>
          </Col>
        </Row>
      </PageHeaderLayout>
    );
  }
}
