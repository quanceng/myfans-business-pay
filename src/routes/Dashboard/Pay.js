import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Checkbox, Avatar, Button, Modal } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './Pay.less';

@connect(state => ({
  common: state.common,
}))
export default class Pay extends PureComponent {
  state = {
    isRead: true,
    isClick: false,
  }
  // 是否同意协议
  changeRead = (e) => {
    this.setState({ isRead: e.target.checked });
  };
  // 付费
  changePay = () => {
    const { common } = this.props;
    this.setState({ isClick: true });
    if (common.initData.forum_info.pay_type === 'alipay_pc') {
      this.props.dispatch({
        type: 'common/payFroum',
        payload: {
          pay_type: 'alipay_pc',
          forum_id: common.initData.forum_info.forum_id,
        },
      }).then((response) => {
        this.setState({ isClick: false });
        if (response.code === 1) {
          window.location.href = response.data.payment_url;
        }
      });
    } else {
      this.props.dispatch({
        type: 'common/payFroum',
        payload: {
          pay_type: 'NATIVE',
          forum_id: common.initData.forum_info.forum_id,
        },
      }).then((response) => {
        this.setState({ isClick: false });
        if (response.code === 1) {
          const self = this;
          Modal.info({
            title: '请用微信扫描二维码付款',
            okText: '完成',
            okType: 'default',
            maskClosable: true,
            content: (
              <img style={{ width: '80%', margin: '0 auto' }} src={`data:image/jpeg;base64,${response.data.qr_code}`} alt="code" />
            ),
            onOk() {
              self.props.dispatch(routerRedux.push('/account/list'));
            },
          });
        }
      });
    }
  };
  render() {
    const forumCommon = this.props.common.initData;
    const { isRead, isClick } = this.state;
    // 内容头部
    const pageHeaderContent = (
      <div className={styles.pageHeaderContent}>
        <div className={styles.avatar}>
          <Avatar src={forumCommon.forum_info.logo} />
        </div>
        <div className={styles.content}>
          <br />
          <div className={styles.contentTitle}>{forumCommon.forum_info.name}</div>
        </div>
      </div>
    );

    // 内容头部扩展
    const pageHeaderExtra = (
      <div>
        <div className={styles.left}>
          <b>粉丝圈付费版{ forumCommon.pay_info.is_pay ? '' : '（试用）'}</b>&nbsp;&nbsp;
          <span>截止：{forumCommon.pay_info.ended_at}</span>
        </div>
        <br /><br />
        <div className={styles.left}>
          {
            forumCommon.pay_info.end_status === 0 ? '服务已过期' : `距离到期还有${forumCommon.pay_info.end_day}天`
          }
        </div>
      </div>
    );

    return (
      <PageHeaderLayout
        content={pageHeaderContent}
        extraContent={pageHeaderExtra}
      >
        <div className={styles.payContent}>
          <div className={styles.payHead}>
            <h2>付费版</h2>
            <p>付费独立社区，不受其他社区的干扰，打造专业品牌必备</p>
          </div>
          <div className={styles.payContair}>
            <div className={styles.conRight}>
              <div>公众号通知消息下发</div>
              <div>1对1客服服务特权</div>
              <div>运营特权</div>
              <div>打造专业品牌</div>
              <div>开通特权（支持7天免费试用期）</div>
              <div>支付费率<b>0.6%</b>（微信支付官方收取通道费用，不包括付费入圈社区和付费话题功能）</div>
              <div>更多服务：</div>
              <div>即时响应客户服务<br />答疑解难，专属客服1对VIP服务（5*8全天候）</div>
              <div>稳定高效运维服务<br />专属专业级独立服务器+高速带宽（24小时技术在线）</div>
              <div>全方位综合指导服务<br />根据专业特性，提供匹配的运营方案（5*8全天候）</div>
            </div>
          </div>
          <div className={styles.payFooter}>
            <b>￥1999</b><span>/年</span>
            <br />
            <Button
              type="primary"
              size="large"
              className={styles.payBtn}
              disabled={!isRead}
              loading={isClick}
              onClick={this.changePay}
            >
              立即续费
            </Button>
            <br />
            <Checkbox checked={isRead} onChange={this.changeRead}>我已阅读并同意<a href="/v2/b/account#/business">《粉丝圈付费版协议》</a></Checkbox>
          </div>
        </div>
      </PageHeaderLayout>
    );
  }
}
