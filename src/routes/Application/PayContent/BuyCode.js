import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button, Form, InputNumber, Modal } from 'antd';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from './PayContent.less';

const FormItem = Form.Item;

@Form.create()
@connect(state => ({
  payContent: state.payContent,
  common: state.common,
}))
export default class BuyCode extends PureComponent {
  state = {
    amount: 0,
    webeiExtract: 0,
    isPay: 1,
  }
  componentDidMount() {
    this.getData();
  }
  // 获取数据
  getData = () => {
    this.props.dispatch({
      type: 'payContent/getPayIn',
    }).then((response) => {
      if (response.code === 1 && response.data) {
        this.setState({
          amount: response.data.amount,
          webeiExtract: response.data.webei_extract,
          isPay: response.data.is_pay,
        });
      }
    });
  }
  // 付钱
  handleSubmit = (e) => {
    e.preventDefault();
    const { common } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (common.initData.forum_info.pay_type === 'alipay_pc') {
          // 支付宝
          this.props.dispatch({
            type: 'payContent/buyCode',
            payload: {
              ...values,
              pay_type: 'alipay_pc',
            },
          }).then((response) => {
            if (response.code === 1) {
              window.location.href = response.data.payment_url;
            }
          });
        } else {
          // 微信
          this.props.dispatch({
            type: 'payContent/buyCode',
            payload: {
              ...values,
              pay_type: 'NATIVE',
            },
          }).then((response) => {
            if (response.code === 1) {
              const self = this;
              Modal.info({
                title: '请用微信扫描二维码付款',
                okText: '完成',
                okType: 'default',
                maskClosable: true,
                content: (
                  <img style={{ width: '80%', margin: '0 auto' }} src={`data:image/jpeg;base64,${response.data.qr_code}`} alt="code" />
                ),
                onOk() {
                  self.props.dispatch(routerRedux.push('/application/pay-content'));
                },
              });
            }
          });
        }
      }
    });
  };
  render() {
    const { common } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { amount, webeiExtract, isPay } = this.state;
    return (
      <PageHeaderLayout>
        <Form
          onSubmit={this.handleSubmit}
        >
          <FormItem
            label="激活码"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            {(amount * webeiExtract) / 100}元
          </FormItem>
          <FormItem
            label="收费类型"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            {isPay === 1 ? '1月' : isPay === 12 ? '1年' : '永久'}
          </FormItem>
          <FormItem
            label="激活码个数"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            {getFieldDecorator(
              'quantity',
              {
                initialValue: 1,
                rules: [{
                  required: true,
                  message: '请输入激活码购买数量',
                }],
              },
            )(
              <InputNumber
                placeholder="请输入整数"
                style={{ width: 180 }}
                min={0}
                precision={0}
              />)}
          </FormItem>
          <FormItem
            label="购买金额"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            {(Number((amount * webeiExtract) / 100) * Number(getFieldValue('quantity'))).toFixed(2)}元
          </FormItem>
          <FormItem
            label="购买方式"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 6 }}
          >
            <Button className={styles.btn}>{common.initData.forum_info.pay_type === 'alipay_pc' ? '支付宝' : '微信'}</Button>
          </FormItem>
          <FormItem
            wrapperCol={{ span: 20, offset: 3 }}
          >
            <Button type="primary" htmlType="submit">确定</Button>
          </FormItem>
        </Form>
      </PageHeaderLayout>
    );
  }
}
