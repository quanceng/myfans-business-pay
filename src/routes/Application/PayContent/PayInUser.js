import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button, Form, Select, Table } from 'antd';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';

const Option = Select.Option;
const FormItem = Form.Item;
const options = [
  {
    value: '全部用户',
    status: 0,
  },
  {
    value: '已到期',
    status: 1,
  },
  {
    value: '未到期',
    status: 2,
  },
  {
    value: '7天到期',
    status: 3,
  },
];

@connect(state => ({
  payContent: state.payContent,
}))
@Form.create()
export default class UserList extends PureComponent {
  state = {
    loading: true,
    userData: {},
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'payContent/payInUser',
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 筛选用户列表
  handleSubmit = (e) => {
    e.preventDefault();
    const data = this.props.form.getFieldValue('is_expire');
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'payContent/payInUser',
      payload: {
        is_expire: data,
      },
    }).then(() => {
      this.setState({
        userData: {
          is_expire: data,
        },
        loading: false,
      });
    });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    const { userData } = this.state;
    userData.page = pagination.current;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'payContent/payInUser',
      payload: userData,
    }).then(() => {
      this.setState({ loading: false, userData });
    });
  }
  // 用户数据列表加载
  columns = () => {
    return [
      {
        title: '用户',
        dataIndex: 'order_info',
        render: (text, record) => (
          <Ellipsis style={{ display: 'inherit' }} length={9} tooltip>{record.order_info && record.order_info.nickname}</Ellipsis>
        ),
      },
      {
        title: '用户ID',
        dataIndex: 'user_id',
      },
      {
        title: '付款时间',
        dataIndex: 'created_at',
      },
      {
        title: '到期时间',
        dataIndex: 'ended_at',
      },
      {
        title: '用户状态',
        dataIndex: 'user_expire_status',
      },
      {
        title: '付款金额',
        dataIndex: 'order_info.total_amount',
      },
    ];
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { payInUserList, payInUserPage } = this.props.payContent;
    const { loading } = this.state;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('is_expire', {
              initialValue: 0,
            })(
              <Select
                style={{ width: 100 }}
                dropdownMatchSelectWidth={false}
              >
                { options.map((val) => {
                  return (
                    <Option key={val.status} value={val.status} >
                      {val.value}
                    </Option>
                  );
                })}
              </Select>)}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.order_no}
          className="mt-20"
          dataSource={payInUserList}
          pagination={payInUserPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
      </div>
    );
  }
}
