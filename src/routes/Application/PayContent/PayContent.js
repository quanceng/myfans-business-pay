import React, { PureComponent } from 'react';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import PayIn from './PayIn';
import PayInUser from './PayInUser';
import BuyList from './BuyList';


const tabList = [
  {
    key: 'payIn',
    tab: '付费入圈',
  },
  {
    key: 'payInUser',
    tab: '付费入圈用户',
  },
  {
    key: 'buyList',
    tab: '激活码列表',
  },
];
export default class PayContent extends PureComponent {
  state = {
    tab: 'payIn',
  }

  // 标签切换
  handleTabChange = (key) => {
    this.setState({ tab: key });
  }

  // 页面切换
  getCurrentComponent() {
    const componentMap = {
      payIn: PayIn,
      payInUser: PayInUser,
      buyList: BuyList,
    };
    return componentMap[this.state.tab];
  }

  render() {
    const CurrentComponent = this.getCurrentComponent();

    return (
      <PageHeaderLayout
        tabList={tabList}
        onTabChange={this.handleTabChange}
      >
        <CurrentComponent />
      </PageHeaderLayout>
    );
  }
}
