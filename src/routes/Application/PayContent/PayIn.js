import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Button, Form, Radio, Icon, Divider, Input, InputNumber, message, Modal, Select } from 'antd';
import UploadCover from '../../../components/UploadCover';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const { TextArea } = Input;

@connect(state => ({
  payIn: state.payContent,
}))
@Form.create()
export default class PayContentPost extends PureComponent {
  state = {
    isPay: 1,
    introduction: '',
    amount: 50,
    qrDetail: null,
    visible: false,
    isOpenDistribute: 0,
    distributePrice: 50,
    distributeRate: 1,
    people: 0,
    showImg: false,
  }
  componentDidMount() {
    this.getData();
  }

  // 获取数据
  getData = () => {
    this.props.dispatch({
      type: 'payContent/getPayIn',
    }).then((response) => {
      if (response.code === 1 && response.data) {
        this.setState({
          isPay: response.data.is_pay ? response.data.is_pay : 1,
          introduction: response.data.introduction,
          amount: response.data.amount,
          isOpenDistribute: response.data.is_open_distribute,
          distributePrice: response.data.distribute_price,
          distributeRate: response.data.distribute_rate,
          people: response.data.people,
          wechatId: response.data.wechat_id,
          wechatQrcode: response.data.wechat_qrcode,
          showImg: true,
        });
      }
    });
  }
  // 设置是否付费
  changePay = (e) => {
    this.setState({ isPay: e.target.value });
  }
  // 设置是否分销
  changeDistribute = (e) => {
    this.setState({ isOpenDistribute: e.target.value });
  }
  // 获取禁用状态
  getInputDisabled = () => {
    return !this.state.isPay;
  }
  // 获取付费类型
  setPayType = (isPay) => {
    this.setState({
      isPay,
    });
  }
  // 上传图片
  handleUploadImage = (image) => {
    this.props.form.setFieldsValue({ wechat_qrcode: image });
  }

  // 删除上传的图片
  handleRemoveImage = () => {
    this.props.form.setFieldsValue({ wechat_qrcode: '' });
  }
  // 提交
  handleSubmit = (e) => {
    e.preventDefault();
    const { isPay, isOpenDistribute } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const data = {
          ...values,
        };
        if (!data.amount) {
          message.info('请输入付费入圈价格！！');
        }
        data.is_pay = isPay;
        data.is_open_distribute = isOpenDistribute;
        this.props.dispatch({
          type: 'payContent/updatePayIn',
          payload: data,
        }).then((response) => {
          if (response.code === 1) {
            this.getData();
          }
        });
      }
    });
  }

  render() {
    const { introduction, isPay, amount, qrDetail, isOpenDistribute, distributePrice, distributeRate, people, wechatId, wechatQrcode, showImg } = this.state;
    const { getFieldDecorator, getFieldValue, setFieldsValue } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 12 },
    };
    const tailFormItemLayout = {
      wrapperCol: { span: 12, offset: 3 },
    };

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            {...formItemLayout}
            label="开启状态"
          >
            <RadioGroup
              value={isPay > 0 ? 1 : 0}
              onChange={this.changePay}
            >
              {/* <Radio value={0}>免费入圈</Radio> */}
              <Radio value={1}>付费入圈</Radio>
            </RadioGroup>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="收费类型"
          >
            <Button
              type={isPay === 1 ? 'primary' : 'ghost'}
              disabled={this.getInputDisabled()}
              style={{ marginRight: 10 }}
              onClick={this.setPayType.bind(this, 1)}
            >
              月
            </Button>
            <Button
              type={isPay === 12 ? 'primary' : 'ghost'}
              disabled={this.getInputDisabled()}
              style={{ marginRight: 10 }}
              onClick={this.setPayType.bind(this, 12)}
            >
              年
            </Button>
            <Button
              type={isPay === 100 ? 'primary' : 'ghost'}
              disabled={this.getInputDisabled()}
              onClick={this.setPayType.bind(this, 100)}
            >
              永久
            </Button>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="收费价格"
          >
            {getFieldDecorator('amount', {
              initialValue: amount,
              getValueFromEvent: (value) => {
                if (value < 50) {
                  return 50;
                }
                return value;
              },
              rules: [
                {
                  validator: (rule, value, callback) => {
                    if (getFieldValue('distribute_price') > value) {
                      setFieldsValue({ distribute_price: value });
                    }
                    callback();
                  },
                },
              ],
            })(
              <InputNumber
                placeholder="请输入不小于50的整数"
                style={{ width: 180 }}
                precision={0}
                disabled={this.getInputDisabled()}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="圈内人数"
          >
            {getFieldDecorator('people', {
              initialValue: people,
            })(
              <InputNumber
                placeholder="请设置圈内人数"
                style={{ width: 180 }}
                min={0}
                precision={0}
                disabled={this.getInputDisabled()}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="圈子简介"
          >
            {getFieldDecorator('introduction', {
              initialValue: introduction,
            })(
              <TextArea
                placeholder="请填写不多于100字的圈子简介"
                disabled={this.getInputDisabled()}
                rows={3}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="微信号"
          >
            {getFieldDecorator('wechat_id', {
              initialValue: wechatId,
            })(
              <Input
                placeholder="请输入正确的微信号方便用户联系购买激活码"
                disabled={this.getInputDisabled()}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="圈主二维码"
          >
            {getFieldDecorator('wechat_qrcode', {
              initialValue: wechatQrcode || '',
            })(
              <Input hidden />
            )}
            {
              showImg ?
                <UploadCover
                  changeImg={this.handleUploadImage}
                  removeImg={this.handleRemoveImage}
                  filename={wechatQrcode}
                  card={1}
                />
              : ''
            }
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="分成功能"
          >
            <RadioGroup
              value={isOpenDistribute}
              onChange={this.changeDistribute}
              disabled={this.getInputDisabled()}
            >
              <Radio value={0}>关闭</Radio>
              <Radio value={1}>开启</Radio>
            </RadioGroup>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="邀请价格"
          >
            {getFieldDecorator('distribute_price', {
              initialValue: distributePrice,
            })(
              <InputNumber
                placeholder="请输入不小于50的整数"
                style={{ width: 180 }}
                min={50}
                max={getFieldValue('amount')}
                precision={0}
                disabled={!(!this.getInputDisabled() && isOpenDistribute)}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="设置分成比例"
          >
            {getFieldDecorator('distribute_rate', {
              initialValue: distributeRate,
            })(
              <Select
                style={{ width: 100 }}
                disabled={!(!this.getInputDisabled() && isOpenDistribute)}
              >
                <Option key={10} value={10} >
                  10
                </Option>
                <Option key={20} value={20} >
                  20
                </Option>
                <Option key={30} value={30} >
                  30
                </Option>
                <Option key={40} value={40} >
                  40
                </Option>
                <Option key={50} value={50} >
                  50
                </Option>
                <Option key={60} value={60} >
                  60
                </Option>
                <Option key={70} value={70} >
                  70
                </Option>
                <Option key={80} value={80} >
                  80
                </Option>
              </Select>)} %
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="提成收入"
          >
            {((getFieldValue('distribute_price') * getFieldValue('distribute_rate')) / 100).toFixed(2)}元
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
            >
              保存
            </Button>
            <Button
              style={{ marginLeft: 48 }}
            >
              <Link to="/application/pay-content/buy">2折购买激活码</Link>
            </Button>
          </FormItem>
        </Form>
        <Divider />
        <div>
          <h3><Icon type="exclamation-circle-o" className="mr-5" /> 注意事项：</h3>
          <p>1. 开启付费入圈功能后粉丝圈的所有用户（包括开通前的已有粉丝）都需支付费用才能进入粉丝圈</p>
          <p>2. 粉丝圈运营者因修改价格、中断服务造成的纠纷由粉丝圈运营者承担</p>
          <p>3. 因粉丝圈运营者管理不当导致圈子被关闭所造成的纠纷由粉丝圈运营者自行承担。</p>
          <p>4. 平台抽取费用的20%作为平台使用费</p>
        </div>
        {qrDetail ?
          <Modal
            title={null}
            visible={this.state.visible}
            onCancel={() => { this.setState({ visible: false }); }}
            closable={false}
            footer={null}
          >
            <div style={{ textAlign: 'center' }}>
              <h3>请截图保存分享该二维码</h3>
              <img src={`data:image/png;base64,${qrDetail.qrcode}`} alt="免费入圈二维码" />
              <p>二维码有效期由{qrDetail.start_at}到{qrDetail.end_at}</p>
              <p>二维码一经关闭不再显示</p>
              <Button
                type="primary"
                onClick={() => { this.setState({ visible: false }); }}
              >
                知道了
              </Button>
            </div>
          </Modal> : ''}
      </div>
    );
  }
}
