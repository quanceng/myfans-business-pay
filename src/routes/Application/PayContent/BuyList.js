import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button, Form, Select, Table } from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;
const options = [
  {
    value: '全部',
    status: 0,
  },
  {
    value: '已使用',
    status: 1,
  },
  {
    value: '未使用',
    status: 2,
  },
];

@connect(state => ({
  payContent: state.payContent,
}))
@Form.create()
export default class BuyList extends PureComponent {
  state = {
    loading: true,
    userData: {},
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'payContent/buyList',
    }).then(() => {
      this.setState({ loading: false });
    });
  }
  // 筛选用户列表
  handleSubmit = (e) => {
    e.preventDefault();
    const data = this.props.form.getFieldValue('status');
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'payContent/buyList',
      payload: {
        status: data,
      },
    }).then(() => {
      this.setState({
        userData: {
          status: data,
        },
        loading: false,
      });
    });
  }
  // 表格翻页
  handleTableChange = (pagination) => {
    const { userData } = this.state;
    userData.page = pagination.current;
    this.setState({ loading: true });
    this.props.dispatch({
      type: 'payContent/buyList',
      payload: userData,
    }).then(() => {
      this.setState({ loading: false, userData });
    });
  }
  // 用户数据列表加载
  columns = () => {
    return [
      {
        title: '激活码',
        dataIndex: 'code',
      },
      {
        title: '价格/元',
        dataIndex: 'current_amount',
      },
      {
        title: '收费类型',
        dataIndex: 'type',
        render(text) {
          return <span>{text === 1 ? '1月' : text === 12 ? '1年' : '永久'}</span>;
        },
      },
      {
        title: '生成时间',
        dataIndex: 'created_at',
      },
      {
        title: '使用时间',
        dataIndex: 'active_at',
      },
      {
        title: '使用者',
        dataIndex: 'nickname',
      },
    ];
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { buyList, buyPage } = this.props.payContent;
    const { loading } = this.state;
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          hideRequiredMark
          layout="inline"
        >
          <FormItem>
            {getFieldDecorator('status', {
              initialValue: 0,
            })(
              <Select
                style={{ width: 100 }}
                dropdownMatchSelectWidth={false}
              >
                { options.map((val) => {
                  return (
                    <Option key={val.status} value={val.status} >
                      {val.value}
                    </Option>
                  );
                })}
              </Select>)}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </FormItem>
          <FormItem>
            <Button type="primary">
              <a href="/b/activation-code-export">导出未使用激活码</a>
            </Button>
          </FormItem>
        </Form>
        <Table
          rowKey={record => record.active_id}
          className="mt-20"
          dataSource={buyList}
          pagination={buyPage}
          columns={this.columns()}
          onChange={this.handleTableChange}
          loading={loading}
        />
      </div>
    );
  }
}
