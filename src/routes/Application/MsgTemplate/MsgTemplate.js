import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Form, Icon, Button, Divider, Select, InputNumber } from 'antd';
import lodash from 'lodash';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

const FormItem = Form.Item;
const Option = Select.Option;
const formItemStyle = {
  labelCol: { span: 3 },
  wrapperCol: { span: 12 },
};
const formBtnStyle = {
  wrapperCol: {
    offset: 3,
    span: 12,
  },
};
const templateListItem = {
  '%CT_EMPTY%': '空',
  '%CT_CONTENT%': '详细内容',
  '%CT_TITLE%': '标题',
  '%CT_BEHAVIOR%': '行为名称',
  '%CT_TIME%': '时间',
  '%CT_FORUM%': '粉丝圈名称',
};

@connect(state => ({
  msgTemplate: state.msgTemplate,
}))
@Form.create()
export default class MsgTemplate extends PureComponent {
  componentDidMount() {
    this.getData();
  }

  getData = () => {
    this.props.dispatch({
      type: 'msgTemplate/msgTemplate',
    });
    this.props.dispatch({
      type: 'msgTemplate/msgTemplateGo',
    });
  }

  // 渲染模板子集
  selectedTemplateList = () => {
    const { getFieldValue, getFieldDecorator } = this.props.form;
    const { msgTemplate, msgTemplateGo } = this.props.msgTemplate;
    const templateId = getFieldValue('template_id');
    if (templateId) {
      const template = lodash.find(msgTemplateGo, { template_id: templateId });
      if (template) {
        return lodash.map(template.data, (val, key) => {
          return (
            <FormItem
              {...formItemStyle}
              label={key}
              key={key + val}
            >
              {getFieldDecorator(`templateListItem[${val}]`, {
                initialValue: (msgTemplate.template_id === templateId && msgTemplate.template_data[val]) || '%CT_EMPTY%',
              })(
                <Select>
                  {
                    lodash.map(templateListItem, (v, k) => {
                      return <Option key={k} value={k}>{v}</Option>;
                    })
                  }
                </Select>
              )}
            </FormItem>
          );
        });
      }
    }
    return null;
  }

  // 提交设置
  submitSave = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const template = lodash.find(
          this.props.msgTemplate.msgTemplateGo,
          { template_id: values.template_id }
        );
        const data = {
          ...values,
          title: template.title,
        };
        this.props.dispatch({
          type: 'msgTemplate/saveMsgTemplate',
          payload: data,
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { msgTemplate, msgTemplateGo } = this.props.msgTemplate;
    return (
      <PageHeaderLayout>
        <Form>
          <FormItem
            {...formItemStyle}
            label="推送时间"
            extra="模板消息推送时间间隔"
          >
            {getFieldDecorator('interval_time', {
              initialValue: msgTemplate.interval || 0,
              rules: [{
                required: true,
                message: '请设置时间',
              }],
            })(
              <InputNumber
                min={0}
                max={60}
              />
            )} 分
          </FormItem>
          <FormItem
            {...formItemStyle}
            label="模板名称"
          >
            {getFieldDecorator('template_id', {
              initialValue: (msgTemplate && msgTemplate.template_id) || 0,
            })(
              <Select>
                {
                  msgTemplate && msgTemplate.template_id ? null : <Option key={0} value={0}>暂未设置</Option>
                }
                {
                  lodash.map(msgTemplateGo, (item) => {
                    return <Option key={item.template_id} value={item.template_id}>{item.title}</Option>;
                  })
                }
              </Select>
            )}
          </FormItem>
          { this.selectedTemplateList() }
          <FormItem {...formBtnStyle}>
            <Button type="primary" htmlType="submit" onClick={this.submitSave}>确定</Button>
          </FormItem>
        </Form>
        <Divider />
        <div>
          <h3>
            <Icon type="exclamation-circle-o" /> 注意事项
          </h3>
          <p className="mt-20">
            1. 为了给您提供更符合场景的消息模板，现将模板的选择权开放给您。您可点击这里<a href="https://ocrjl5j3c.qnssl.com/v2/7bb19a085afdd75d5089c084e2cfcb79.jpg" target="_blank" rel="noopener noreferrer">添加公众号消息模板</a>了解详情
          </p>
          <p>2. 此模板应用于点赞、评论、拉黑、禁言、打赏等操作行为，其它应用相关的消息模板统一在myfans公众号内下发</p>
          <p>3. 未设置消息模板的社区将无法发送模板消息</p>
          <p>4. 重新授权或公众号后台修改当前使用模板后需要重新配置模板字段</p>
        </div>
      </PageHeaderLayout>
    );
  }
}
