import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Steps, Form, Input, Button, Upload, Icon, message } from 'antd';
import styles from './ForumList.less';

const FormItem = Form.Item;
const Step = Steps.Step;
const imageUrlPre = 'https://cdn.myfans.cc/';
@Form.create()
@connect(state => ({
  login: state.login,
  common: state.common,
}))

export default class AddList extends Component {
  state = {
    current: 0,
    steps: [
      {
        title: '填写基础资料',
      }, {
        title: '选择付费类型',
      }, {
        title: '支付费用',
      },
    ],
    forumId: 0,
  }
  // 退出登录
  logOut = () => {
    this.props.dispatch({
      type: 'login/logout',
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/account/login'));
        }, 1000);
      }
    });
  }
  // 上传粉丝圈logo
  updateLogo = (info) => {
    const { status, response, uid, name } = info.file;
    if (status === 'done') {
      if (response.code === 1) {
        const fileList = [{
          uid,
          name,
          status: 'done',
          url: `${imageUrlPre + response.data.filename}`,
          logo: response.data.filename,
        }];
        return fileList;
      } else {
        message.error(response.msg);
      }
    } else if (status === 'error') {
      message.error(`${info.file.name}上传失败`);
    }

    return info.fileList;
  }
  // 创建圈子
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = {
        ...values,
        logo: values.logo[0].logo,
      };
      this.props.dispatch({
        type: 'common/createForum',
        payload: data,
      }).then((response) => {
        if (response.code === 1) {
          this.setState({
            current: 1,
            forumId: response.data.forum_id,
          });
        }
      });
    });
  }
  // 付费
  changePay = () => {
    this.props.dispatch({
      type: 'common/payFroum',
      payload: {
        pay_type: 'alipay_pc',
        forum_id: this.state.forumId,
      },
    }).then((response) => {
      if (response.code === 1) {
        window.location.href = response.data.payment_url;
      }
    });
  };
  // 第一步
  renderFirstStep = () => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem>
          圈子名称：
        </FormItem>
        <FormItem>
          {getFieldDecorator('name', {
            rules: [{
              required: true, message: '请输入圈子名！',
            }],
          })(
            <Input
              size="large"
              placeholder="请输入圈子名"
            />
          )}
        </FormItem>
        <FormItem>
          圈子logo：
        </FormItem>
        <FormItem>
          {getFieldDecorator('logo', {
            valuePropName: 'fileList',
            getValueFromEvent: this.updateLogo,
            rules: [{
              required: true, message: '请上传logo',
            }],
          })(
            <Upload
              name="image"
              action="/b/upload-image"
              listType="picture-card"
            >
              <Icon type="sync" />
              <div>更换Logo</div>
            </Upload>
          )}
        </FormItem>
        <FormItem className={styles.additional}>
          <Button size="large" className={styles.submit} type="primary" htmlType="submit">
            下一步
          </Button>
        </FormItem>
      </Form>
    );
  };
  // 第二步
  renderSecondStep = () => {
    return (
      <div className={styles.payContent}>
        <div className={styles.payHead}>
          <h2>付费版</h2>
          <p>付费独立社区，不受其他社区的干扰，打造专业品牌必备</p>
        </div>
        <div className={styles.payFooter}>
          <b>￥1999</b><span>/年</span>
          <br />
          <Button
            size="large"
            className={styles.payBtn}
            href="/#/account/list"
          >
            7天试用
          </Button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Button
            type="primary"
            size="large"
            className={styles.payBtn}
            onClick={() => { this.setState({ current: 2 }); }}
          >
            立即升级
          </Button>
          <br />
        </div>
        <div className={styles.payContair}>
          <div className={styles.conRight}>
            <div>公众号通知消息下发</div>
            <div>1对1客服服务特权</div>
            <div>运营特权</div>
            <div>打造专业品牌</div>
            <div>开通特权（支持7天免费试用期）</div>
            <div>支付费率<b>0.6%</b>（微信支付官方收取通道费用，不包括付费入圈社区和付费话题功能）</div>
            <div>更多服务：</div>
            <div>即时响应客户服务<br />答疑解难，专属客服1对VIP服务（5*8全天候）</div>
            <div>稳定高效运维服务<br />专属专业级独立服务器+高速带宽（24小时技术在线）</div>
            <div>全方位综合指导服务<br />根据专业特性，提供匹配的运营方案（5*8全天候）</div>
          </div>
        </div>
      </div>
    );
  };
  // 第三步
  renderThirdStep = () => {
    return (
      <div style={{ textAlign: 'center' }}>
        <h1>付款金额：1999元／年</h1>
        <br />
        <Button
          size="large"
          style={{ width: '50%' }}
          onClick={this.changePay}
        >
          去支付
        </Button>
        <div className="mt-20">使用支付宝支付</div>
        <div className="mt-20">注：请在30分钟内完成支付，否则将不再保留订单</div>
      </div>
    );
  };
  render() {
    const { current, steps } = this.state;
    return (
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <div className={styles.headwrap}>
            <a className={styles.logo} href="/">
              粉丝圈
            </a>
            <div className={styles.logout} onClick={this.logOut}>
              退出登录
            </div>
          </div>
        </div>
        <div className={styles.content}>
          <Steps current={current}>
            {steps.map(item => <Step key={item.title} title={item.title} />)}
          </Steps>
          <div className={styles.steps_con}>
            {current === 0 ? this.renderFirstStep() : current === 1 ? this.renderSecondStep() : this.renderThirdStep()}
          </div>
        </div>
      </div>
    );
  }
}
