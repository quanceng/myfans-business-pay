import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import styles from './ForumList.less';

@connect(state => ({
  login: state.login,
}))

export default class ForumList extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'login/getForumList',
    });
  }
  // 退出登录
  logOut = () => {
    this.props.dispatch({
      type: 'login/logout',
    }).then((response) => {
      if (response.code === 1) {
        setTimeout(() => {
          this.props.dispatch(routerRedux.push('/account/login'));
        }, 1000);
      }
    });
  }
  // 选择圈子
  forumEnter = (forumid, endStatus) => {
    this.props.dispatch({
      type: 'login/chooseForum',
      payload: { forum_id: forumid },
    })
      .then((response) => {
        if (response.code === 1) {
          setTimeout(() => {
            if (endStatus === 0) {
              this.props.dispatch(routerRedux.push('/dashboard/pay'));
            } else {
              this.props.dispatch(routerRedux.push('/dashboard/summary'));
            }
          }, 1000);
        }
      });
  }
  render() {
    const { forumList } = this.props.login;
    return (
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <div className={styles.headwrap}>
            <a className={styles.logo} href="/">
              粉丝圈
            </a>
            <div className={styles.logout} onClick={this.logOut}>
              退出登录
            </div>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.title}>
            粉丝圈列表
          </div>
          <ul className={styles.forumlist}>
            {
              forumList.map((item) => {
                return (
                  <li className={styles.forumitem} onClick={this.forumEnter.bind(this, item.forum_id, item.end_status)} key={item.forum_id}>
                    <img src={item.logo} alt="" />
                    <div className={styles.iteminfo}>
                      <h4><Ellipsis length={9} tooltip>{item.name}</Ellipsis></h4>
                      <p>浏览量：{item.views_total}</p>
                      <p>用户数：{item.member_total}</p>
                    </div>
                    {
                      item.forum_type ? <div className={styles.prologo} /> : ''
                    }
                  </li>
                );
              })
            }
            {/* <li className={styles.forumitem} style={{ textAlign: 'center' }}>
              <Link to="/account/add">
                <Icon className={styles.icon_add} type="plus-circle" theme="outlined" />
                <p className={styles.txt_add}>创建新的圈子</p>
              </Link>
            </li> */}
          </ul>
        </div>
      </div>
    );
  }
}
