import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Form, Input, Button, Icon } from 'antd';
import styles from './Login.less';

const FormItem = Form.Item;

@connect(state => ({
  login: state.login,
}))
@Form.create()
export default class Login extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'login/login',
          payload: {
            ...values,
          },
        }).then((response) => {
          if (response.code === 1) {
            this.props.dispatch({
              type: 'login/getForumList',
            }).then((res) => {
              if (res.code) {
                if (res.data.total > 1) {
                  setTimeout(() => {
                    this.props.dispatch(routerRedux.push('/account/list'));
                  }, 1000);
                } else {
                  this.props.dispatch({
                    type: 'login/chooseForum',
                    payload: { forum_id: res.data.data[0].forum_id },
                  }).then((result) => {
                    if (result.code === 1) {
                      setTimeout(() => {
                        this.props.dispatch(routerRedux.push('/dashboard/summary'));
                      }, 1000);
                    }
                  });
                }
              }
            });
          }
        });
      }
    });
  }

  render() {
    const { form, login } = this.props;
    const { getFieldDecorator } = form;
    return (
      <div className={styles.wrapper}>
        <div className={styles.main}>
          <div className={styles.top}>
            <div className={styles.header}>
              <span className={styles.title}>粉丝圈付费版</span>
            </div>
            <div className={styles.desc} />
          </div>
          <Form onSubmit={this.handleSubmit}>
            <FormItem>
              {getFieldDecorator('phone', {
                rules: [{
                  required: true, message: '请输入账户名！',
                }],
              })(
                <Input
                  size="large"
                  prefix={<Icon type="user" className={styles.prefixIcon} />}
                  placeholder="请输入账户名"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{
                  required: true, message: '请输入密码！',
                }],
              })(
                <Input
                  size="large"
                  prefix={<Icon type="lock" className={styles.prefixIcon} />}
                  type="password"
                  placeholder="请输入密码"
                />
              )}
            </FormItem>
            <FormItem className={styles.additional}>
              <Button size="large" loading={login.submitting} className={styles.submit} type="primary" htmlType="submit">
                登录
              </Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}
