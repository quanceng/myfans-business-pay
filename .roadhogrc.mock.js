import mockjs from 'mockjs';
import { format, delay } from 'roadhog-api-doc';
// import * from './mock/mock.js';
const mock = require('./mock/api/mock.js');

// 是否禁用代理
const noProxy = process.env.NO_PROXY === 'true';

// 代码中会兼容本地 service mock 以及部署站点的静态数据
const proxy = {
  'POST /b/upload-image': mock.uploadImg,
  'POST /b/login': mock.login,
  'POST /b/forum-list': mock.forumItem,
  'GET /b/forum-list': mock.forumList,
  'GET /b/base-data': mock.baseData,
  // 粉丝圈概况
  'GET /b/forum-index': mock.forumIndex,
  // 微帖管理
  'GET /b/comment': mock.postComments,
  'GET /b/post': mock.postList,
  'GET /b/post-config': mock.postConfig,
  'GET /b/post-review': mock.postAudit,
  'GET /b/post-review/*': mock.auditDetail,
  'GET /b/post/*': mock.postDetail,
  // 话题管理
  'GET /b/topic': mock.topicList,
  'GET /b/topic-config': mock.topicConfig,
  // 用户管理
  'GET /b/user': mock.userList,
  'GET /b/level': mock.levelList,
  'GET /b/identity': mock.userIdentity,
  // 社区收入
  'GET /b/co-wallet': mock.walletList,
  'GET /b/co-wallet/create': mock.income,
  // 签到管理
  'GET /b/sign-user-data': mock.signList,
  // 公告管理
  'GET /b/notice': mock.noticeList,
  'GET /b/notice/*': mock.noticeInfo,
  // 马甲管理
  'GET /b/vest': mock.vestList,
  'GET /b/vest/*': mock.vestInfo,
  // 基础设置
  'GET /b/forum': mock.forumInfo,
  // 首页设置
  'GET /b/cover': mock.coverSetting,
  // 付费内容
  'GET /b/forum-pay': mock.forumPay,
  'GET /b/forum-pay-list': mock.forumPayList,
  'GET /b/forum-pay-share': mock.forumPayShare,
  // 积分设置
  'GET /b/user-behavior-rules': mock.behaviorRules,
  // 数据分析
  'GET /b/stats/forum': mock.forumBasic,
  // 模板消息
  'GET /b/message/0': mock.messageConfig,
  'GET /b/message': mock.messageList,
};

export default noProxy ? {} : delay(proxy, 1000);
