module.exports.login = require('../login.json');
module.exports.forumList = require('../forum-list.json');
module.exports.forumItem = require('../forum-item.json');
module.exports.baseData = require('../base-data.json');
module.exports.uploadImg = require('../upload-image.json');
// 粉丝圈概况
module.exports.forumIndex = require('../forum-index.json');
// 微帖管理
module.exports.postList = require('../post-list.json');
module.exports.postConfig = require('../post-config.json');
module.exports.postAudit = require('../post-audit.json');
module.exports.postDetail = require('../post-detail.json');
module.exports.auditDetail = require('../audit-detail.json');
module.exports.postComments = require('../post-comments.json');
// 话题管理
module.exports.topicList = require('../topic-list.json');
module.exports.topicConfig = require('../topic-config.json');
// 用户管理
module.exports.userList = require('../user-list.json');
module.exports.levelList = require('../level-list.json');
module.exports.userIdentity = require('../user-identity.json');
// 社区收入
module.exports.walletList = require('../co-wallet.json');
module.exports.income = require('../income.json');
// 签到管理
module.exports.signList = require('../sign-list.json');
// 公告管理
module.exports.noticeList = require('../notice-list.json');
module.exports.noticeInfo = require('../notice-info.json');
// 马甲管理
module.exports.vestList = require('../vest-list.json');
module.exports.vestInfo = require('../vest-info.json');
// 基础设置
module.exports.forumInfo = require('../forum-info.json');
// 首页设置
module.exports.coverSetting = require('../cover.json');
// 付费内容
module.exports.forumPay = require('../forum-pay.json');
module.exports.forumPayList = require('../forum-pay-list.json');
module.exports.forumPayShare = require('../forum-pay-share.json');
// 积分设置
module.exports.behaviorRules = require('../behavior-rules.json');
// 数据分析
module.exports.forumBasic = require('../forum-basic.json');
// 模板消息
module.exports.messageConfig = require('../message-config.json');
module.exports.messageList = require('../message-list.json');
